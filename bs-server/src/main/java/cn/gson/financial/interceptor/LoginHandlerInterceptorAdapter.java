package cn.gson.financial.interceptor;

import cn.gson.financial.annotation.IgnoresLogin;
import cn.gson.financial.kernel.model.vo.UserVo;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@NoArgsConstructor
@AllArgsConstructor
public class LoginHandlerInterceptorAdapter extends HandlerInterceptorAdapter {

    private String[] ignores;
    private AntPathMatcher matcher = new AntPathMatcher();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("登录拦截器1: " + request.getRequestURL());
        HttpSession session = request.getSession();
        HandlerMethod hm = (HandlerMethod) handler;
        UserVo user = (UserVo) session.getAttribute("user");
        if (user == null) {
            if (hm.hasMethodAnnotation(IgnoresLogin.class)) {
                return true;
            }

            String servletPath = request.getServletPath();
            if (this.ignores != null && this.ignores.length > 0) {
                for (String path : this.ignores) {
                    if (matcher.match(path, servletPath)) {
                        return true;
                    }
                }
            }

            response.setStatus(HttpStatus.FORBIDDEN.value());
            return false;
        }

        return true;
    }
}
