package cn.gson.financial.interceptor;

import cn.gson.financial.annotation.IgnoresLogin;
import cn.gson.financial.annotation.Permissions;
import cn.gson.financial.kernel.common.Roles;
import cn.gson.financial.kernel.model.vo.UserVo;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PermissionsHandlerInterceptorAdapter extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("Permission拦截器2: " + request.getRequestURL());
        HttpSession session = request.getSession();
        HandlerMethod hm = (HandlerMethod) handler;
        UserVo userVo = (UserVo) session.getAttribute("user");
        if (userVo == null || hm.hasMethodAnnotation(IgnoresLogin.class)) {
            return true;
        }

        if (!hm.hasMethodAnnotation(Permissions.class)) {
            return true;
        }

        if (userVo.getRole() == null) {
            return false;
        }

        Roles role = Roles.valueOf(userVo.getRole());
        if (role.equals(Roles.Manager)) {
            return true;
        }

        //没有注解，就表示不需控制权限
        Permissions mainPermissions = hm.getBeanType().getAnnotation(Permissions.class);
        Permissions permissions = hm.getMethodAnnotation(Permissions.class);
        if (ArrayUtils.contains(permissions.value(), role)) {
            return true;
        }

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        return false;
    }
}
