package cn.gson.financial.controller;

import cn.gson.financial.base.BaseController;
import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.model.entity.ReportBottomTableBean;
import cn.gson.financial.kernel.model.entity.Voucher;
import cn.gson.financial.kernel.model.vo.IndexTargetVo;
import cn.gson.financial.kernel.model.vo.VoucherSelectVo;
import cn.gson.financial.kernel.service.AllSelectService;
import cn.gson.financial.kernel.service.ReportBottomTableService;
import cn.gson.financial.kernel.service.ReportTemplateService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.controller
 * @Author: 龙泽霖
 * @CreateTime: 2023-04-03  09:42
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/select")
@AllArgsConstructor
@Slf4j
public class AllSelectController extends BaseController {

    @Autowired
    private AllSelectService allSelectService;
    @Resource
    private ReportBottomTableService reportBottomTableService;
    @Autowired
    private ReportTemplateService reportTemplateService;

    @PostMapping(value = "/voucherSelect")
    public List<Voucher> voucherSelect(VoucherSelectVo vo){
        return allSelectService.voucherSelect(vo,this.accountSetsId);
    }
    @GetMapping(value = "/indexTargetMonth")
    public JsonResult indexTargetMonth(@RequestParam String timeStart, @RequestParam String timeEnd){
        return JsonResult.successful(reportTemplateService.IndexTarget(this.accountSetsId,timeStart,timeEnd));
    }
    @GetMapping(value = "/indexTargetYear")
    public IndexTargetVo indexTargetYear(String date){
        System.out.println(date);
        return allSelectService.IndexTargetYear(date,this.accountSetsId);
    }
    //底表生成、每次修改
    @PostMapping(value = "/bottom")
    public JsonResult bottomTable(@RequestBody List<ReportBottomTableBean> list){
        System.out.println(list);
        reportBottomTableService.insertBottomTable(list);
        return JsonResult.successful();
    }

    @PostMapping(value = "/delbottom")
    public JsonResult bottomTable(String id){
         reportBottomTableService.delectBottomTable(id);
        return JsonResult.successful();
    }
    //设置--凭证字查询
    @GetMapping(value = "/selectWord")
    public JsonResult selectWord(){
        return JsonResult.successful(allSelectService.selectWord(this.accountSetsId));
    }

}
