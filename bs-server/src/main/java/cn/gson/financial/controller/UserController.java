package cn.gson.financial.controller;

import cn.gson.financial.base.BaseCrudController;
import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.model.entity.User;
import cn.gson.financial.kernel.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequestMapping("/user")
public class UserController extends BaseCrudController<UserService, User> {

    @Override
    public JsonResult list(@RequestParam Map<String, String> params) {
        return JsonResult.successful(service.listByAccountSetId(this.accountSetsId));
    }
}
