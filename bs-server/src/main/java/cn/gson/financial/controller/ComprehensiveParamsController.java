package cn.gson.financial.controller;

import cn.gson.financial.kernel.constans.ComprehensiveParamesConstans;
import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.service.ComprehensiveParamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/5/4 11:12
 * @Description：
 */
@RestController
@RequestMapping("/comprehensiveParams")
public class ComprehensiveParamsController {

    @Autowired
    private ComprehensiveParamsService comprehensiveParamsService;

    @GetMapping("/selectSoftName")
    public JsonResult findContent(){
        String content = comprehensiveParamsService.queryDefaultContent(ComprehensiveParamesConstans.SOFT_WARE_NAME_LIST);
        String[] split = content.split(",");
        List<HashMap<String, String>> list = new ArrayList<>();
        for (String softName : split) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("name",softName);
            list.add(hashMap);
        }
        return JsonResult.successful(list);
    }
}
