package cn.gson.financial.controller;

import cn.gson.financial.base.BaseCrudController;
import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.model.entity.ReportTemplateItems;
import cn.gson.financial.kernel.model.entity.ReportTemplateItemsFormula;
import cn.gson.financial.kernel.service.ReportTemplateItemsService;
import lombok.Data;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/report/template/items")
public class ReportTemplateItemsController extends BaseCrudController<ReportTemplateItemsService, ReportTemplateItems> {


    @PostMapping("formula")
    public JsonResult saveFormula(@RequestBody FormulaForm formulaForm) {
        service.saveFormula(formulaForm.getTemplateItemsId(), formulaForm.getFormulas(), this.accountSetsId);
        return JsonResult.successful();
    }

    @Data
    static class FormulaForm {
        List<ReportTemplateItemsFormula> formulas;
        Integer templateItemsId;
    }
}
