package cn.gson.financial.controller;

import cn.gson.financial.annotation.IgnoresLogin;
import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.dao.ComprehensiveParamsDao;

import cn.gson.financial.kernel.model.entity.FileBean;
import cn.gson.financial.kernel.model.mapper.FileMapper;
import cn.gson.financial.kernel.service.OssService;
import cn.gson.financial.kernel.service.impl.FileManageService;
import cn.gson.financial.kernel.utils.JsonStringUtil;
import cn.gson.financial.kernel.utils.JsonUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/4/11 10:59
 * @Description：
 */
@RestController
@RequestMapping("/file")
public class FileManageController {

    @Autowired
    private OssService ossService;
    @Autowired
    private ComprehensiveParamsDao comprehensiveParamsDao;
    @Autowired
    private FileMapper fileMapper;
    @Autowired
    private FileManageService fileManageService;

    @ApiOperation("文件上传-单张")
    @PostMapping(value = "/uploadFile")
    public JsonResult uploadFile(@RequestParam("file") MultipartFile file){
        return JsonResult.successful(ossService.uploadFile(file));
    }

    @ApiOperation("文件下载")
    @GetMapping(value = "/downloadFile")
    public ResponseEntity<byte[]> downloadFile(String fileId, HttpServletRequest request) throws UnsupportedEncodingException {
        byte[] bytes = ossService.downLoadFile(fileId);
        String fileType = fileId.substring(fileId.lastIndexOf("."));
        String fileName = "fxy" + System.currentTimeMillis() +  fileType;
        String downFileName = new String(fileName.getBytes("UTF-8"),"ISO-8859-1");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("attachment", downFileName);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        ResponseEntity responseEntity = new ResponseEntity<>(bytes, headers, HttpStatus.OK);
        return responseEntity;
    }

    @ApiOperation("文件附件上传")
    @PostMapping(value = "/uploadAppendix")
    public JsonResult uploadAppendix(FileBean fileBean){

        fileBean.setCreateTime(new Date());
        int i = 0;
        for (MultipartFile multipartFile : fileBean.getFileList()) {
            String fileId = ossService.uploadFile(multipartFile);
            String fileName = multipartFile.getOriginalFilename();
            fileBean.setFileId(fileId);
            fileBean.setFileName(fileName);

            System.out.println(fileBean);
            System.out.println("fileBean111="+fileBean);

            int insert = fileMapper.insert(fileBean);
            System.out.println("fileBean222="+fileBean);
            i += insert;
        }


        if (i == fileBean.getFileList().size()){
            return JsonResult.successful("文件上传成功");
        }
        return JsonResult.failure("文件附件上传失败");

    }

    @ApiOperation("文件附件下载")
    @GetMapping(value = "/downloadAppendix")
    public ResponseEntity<byte[]> downloadAppendix(String fileId, HttpServletRequest request) throws UnsupportedEncodingException {
        byte[] bytes = ossService.downLoadFile(fileId);

        String fileType = fileId.substring(fileId.lastIndexOf("."));
        String fileName = "fxy" + System.currentTimeMillis() +  fileType;
        String downFileName = new String(fileName.getBytes("UTF-8"),"ISO-8859-1");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("attachment", downFileName);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        ResponseEntity responseEntity = new ResponseEntity<>(bytes, headers, HttpStatus.OK);
        return responseEntity;
    }

    @ApiOperation("文件删除")
    @GetMapping(value = "/delFile")
    public JsonResult delFile(@RequestParam("fileId") String fileId) {
        boolean result = ossService.deleteFile(fileId);
        if (result){
            return JsonResult.successful("全部删除成功！");
        }
        return JsonResult.failure("删除失败");
    }

    @ApiOperation("文件展示")
    @GetMapping(value = "/showFile")
    public JsonResult showFile(String docId,HttpServletRequest request) {
        byte[] bytes = ossService.downLoadFile(docId);
        byte[] bytesBase = Base64.encodeBase64(bytes);
        String encodedString = new String(bytesBase);
        String fileType = docId.substring(docId.lastIndexOf(".") +1);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("fileType",fileType);
        hashMap.put("base64",encodedString);
        return JsonResult.successful(hashMap);
    }

    @ApiOperation("文件下载并修改文件名")
    @GetMapping(value = "/downloadReName")
    public ResponseEntity<byte[]> downloadReName(String fileId,String reName) throws UnsupportedEncodingException {
        byte[]  bytes = ossService.downLoadFile(fileId);

        String fileType = fileId.substring(fileId.lastIndexOf("."));
        String fileName = reName +  fileType;
        String downFileName = new String(fileName.getBytes("UTF-8"),"ISO-8859-1");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("attachment", downFileName);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        ResponseEntity responseEntity = new ResponseEntity<byte[]>(bytes,headers, HttpStatus.OK);
        return responseEntity;
    }

    @ApiOperation("下载导入模板")
    @GetMapping(value = "/downloadImportMould")
    public ResponseEntity<byte[]> downloadImportMould(@RequestParam("code") String code) throws UnsupportedEncodingException {
        String content = comprehensiveParamsDao.queryDefaultContent(code);
        String fileName = JsonStringUtil.JsonStringGetValueByKey(content, "fileName");
        String fileId = JsonStringUtil.JsonStringGetValueByKey(content, "fileId");
        byte[]  bytes = ossService.downLoadFile(fileId);
        String downFileName = new String(fileName.getBytes("UTF-8"),"ISO-8859-1");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("attachment", downFileName);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        ResponseEntity responseEntity = new ResponseEntity<byte[]>(bytes,headers, HttpStatus.OK);
        return responseEntity;
    }

    @ApiOperation("查看附件信息")
    @GetMapping(value = "/findFileBean")
    public JsonResult findFileBean(@RequestParam("accountSetsId") int accountSetsId,@RequestParam("voucherId") int voucherId){
        return JsonResult.successful(fileManageService.findFileBean(accountSetsId,voucherId));
    }
}


