package cn.gson.financial.controller;

import cn.gson.financial.base.BaseCrudController;
import cn.gson.financial.kernel.model.entity.VoucherTemplate;
import cn.gson.financial.kernel.service.VoucherTemplateService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/voucher-template")
public class VoucherTemplateController extends BaseCrudController<VoucherTemplateService, VoucherTemplate> {

}
