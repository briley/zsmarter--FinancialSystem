package cn.gson.financial.controller;

import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.exception.ResultCode;
import cn.gson.financial.kernel.model.entity.UserRoundupsBean;
import cn.gson.financial.kernel.model.vo.AddUserRoleVo;
import cn.gson.financial.kernel.model.vo.BangdPhoneVo;
import cn.gson.financial.kernel.model.vo.UpdatePhoneVo;
import cn.gson.financial.kernel.service.SmsBaoService;
import cn.gson.financial.kernel.service.UserRoundupsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.controller
 * @Author: 龙泽霖
 * @CreateTime: 2023-03-27  15:50
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/userRoundups")
@AllArgsConstructor
@Slf4j
public class UserRoundupsController {
    @Autowired
    private UserRoundupsService userRoundupsService;
    @Resource
    private SmsBaoService smsService;
    //摘要接口
    @GetMapping("/select")
    public JsonResult selectRoundupsByUserId(String userId) {
        return JsonResult.successful(userRoundupsService.selectRoundupByUserId(userId));
    }
    @PostMapping("/insert")
    public JsonResult insertRoundupsByUserId(@RequestBody UserRoundupsBean bean) {
        return JsonResult.successful(userRoundupsService.insertRoundupByUserId(bean));
    }
    //用户手机号相关接口
    //获取绑定手机验证码
    @GetMapping("/3")
    public JsonResult getSmsCode(String phone){
        String res =  smsService.brdingSendMessage(phone);
        if(res.equals("发送成功")){
            return JsonResult.successful(res);
        }else if(res.equals("一分钟后再尝试")){
            return JsonResult.failure(res);
        }
        return JsonResult.failure(res);
    }
    //绑定手机号
    @PostMapping("/2")
    public void brdingPhone(String id, String phone, String code){
        userRoundupsService.bridingPhone(phone,id,code);
    }

    @PostMapping("/bangdPhone")
    public JsonResult bangdPhone(@RequestBody BangdPhoneVo vo, HttpSession session){
       String res =  userRoundupsService.updateUserPhoneHx(
                vo.getWxUserId(),
                vo.getOpenId(),
                vo.getPhoneNum(),
                vo.getCode(),session);
       if(res.contains("失败")){
           return JsonResult.failure(res);
       }else if(res.contains("错误")){
           return JsonResult.failure(ResultCode.USER_SMS_CODE_ERROR);
       }
        return JsonResult.successful();
    }


    //修改手机号
    @PostMapping("/updatePhone")
    public JsonResult updatePhone(@RequestBody UpdatePhoneVo vo){
        System.out.println(vo);
        String id = vo.getId();
                String phone =vo.getPhone();
                String code =vo.getCode();
        String res = userRoundupsService.updateUserPhone(phone,id,code);
        if (res.equals("手机号已注册")){
            return JsonResult.failure(res);
        }else if(res.equals("验证码错误")){
            return JsonResult.failure(res);
        }
        return JsonResult.successful(res);
    }
    //权限获取手机验证码
    @GetMapping("/4")
    public JsonResult getPowerCode(String phone,Integer accountSetsId){
        String res =  smsService.powerSendMessage(phone,accountSetsId);
        if(res.equals("发送成功")){
            return JsonResult.successful(res);
        }else if(res.equals("一分钟后再尝试")){
            return JsonResult.failure(res);
        }
        return JsonResult.failure(res);
    }

    //删除账套
    @GetMapping("/deleteAccount")
    public JsonResult deleteAccount(String phone){
        String res =  smsService.deleteAccountSendMessage(phone);
        if(res.equals("发送成功")){
            return JsonResult.successful(res);
        }else if(res.equals("一分钟后再尝试")){
            return JsonResult.failure(res);
        }
        return JsonResult.failure(res);
    }

    //增加权限
    @PostMapping("/addUserRole")
    public JsonResult addUserRole(@RequestBody AddUserRoleVo vo){
        System.out.println(vo);
        String phone = vo.getPhone();
        String role =vo.getRole();
        Integer accountSetsId =vo.getAccountSetsId();
        String code =vo.getCode();
        String res = userRoundupsService.addUserRole(phone,role,accountSetsId,code);
        if (res.equals("添加成功")){
            return JsonResult.successful(res);
        }else {
            return JsonResult.failure(res);
        }
    }
    //查询权限人数
    @GetMapping("/selectAccountRole")
    public JsonResult selectAccountRole(Integer accountSetsId){
        return JsonResult.successful(userRoundupsService.selectAccountRole(accountSetsId));
    }




}
