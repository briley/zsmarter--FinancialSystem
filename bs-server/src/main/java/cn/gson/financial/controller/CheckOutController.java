package cn.gson.financial.controller;

import cn.gson.financial.base.BaseCrudController;
import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.model.entity.Checkout;
import cn.gson.financial.kernel.model.vo.UserVo;
import cn.gson.financial.kernel.service.CheckoutService;
import cn.gson.financial.kernel.service.UserService;
import cn.gson.financial.kernel.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;


@RestController
@RequestMapping("/checkout")
public class CheckOutController extends BaseCrudController<CheckoutService, Checkout> {
    DateFormat df = new SimpleDateFormat("yyyyMM");
    @Autowired
    private UserService userService;
    @Autowired
    private RedisUtil redisUtil;
    /**
     * 期初检查
     *
     * @param year
     * @param month
     * @return
     */
    @GetMapping("initialCheck")
    public JsonResult initialCheck(@RequestParam Integer year, @RequestParam Integer month) {
        //判断是否是结转第一期，如果是第一期，则需要检查期初平衡
        if (!df.format(this.currentUser.getAccountSets().getEnableDate()).equals(year + "" + month)) {
            return JsonResult.instance(this.service.initialCheck(this.accountSetsId));
        }
        return JsonResult.successful();
    }

    /**
     * 期末检查
     *
     * @param year
     * @param month
     * @return
     */
    @GetMapping("finalCheck")
    public JsonResult finalCheck(@RequestParam Integer year, @RequestParam Integer month) {
        return JsonResult.instance(this.service.finalCheck(this.accountSetsId, year, month));
    }

    /**
     * @description:结转科目税率设置
     * @author: 龙泽霖
     * @date:  14:46
     * @param:
     * @return:
     **/
    @GetMapping("checkoutSet")
    public JsonResult checkoutSet(@RequestParam Integer year, @RequestParam Integer month){
        return JsonResult.successful(this.service.checkoutSet(this.accountSetsId, year, month));
    }

    /**
     * 报表检查
     *
     * @param year
     * @param month
     * @return
     */
    @GetMapping("reportCheck")
    public JsonResult reportCheck(@RequestParam Integer year, @RequestParam Integer month) {
        return JsonResult.successful(this.service.reportCheckFy(this.accountSetsId, year, month));
    }

    /**
     * 断号检查（新增凭证审核检查）
     * ①“断号检查”变为“凭证检查”
     * ②不需要审核凭证的账套，只检查是否断号，若存在断号，提示“凭证号不连续”
     * ③需要审核凭证的账套，需要检查断号和审核状态，若仅存在断号，提示“凭证号不连续”；若仅存在未审核，提示“存在未审核凭证”；若断号、未审核都存在，提示“凭证号不连续；存在未审核凭证”
     * @param year
     * @param month
     * @return
     */
    @GetMapping("brokenCheck")
    public JsonResult brokenCheck(@RequestParam Integer year, @RequestParam Integer month) {
        Boolean falg1 = this.service.checkVoucher(this.accountSetsId,year,month);
        Boolean falg2 = this.service.brokenCheck(this.accountSetsId, year, month);
        if (!falg1 && !falg2){
            return JsonResult.successful("凭证号不连续；存在未审核凭证");
        }else if (falg1 && !falg2){
            return JsonResult.successful("凭证号不连续");
        }else if (!falg1 && falg2){
            return JsonResult.successful("存在未审核凭证");
        }else{

            return JsonResult.successful();
        }
    }

    /**
     * 结账
     *
     * @param year
     * @param month
     * @return
     */
    @GetMapping("invoicing")
    public JsonResult invoicing(@RequestParam Integer year, @RequestParam Integer month) {
        if (!this.service.invoicing(this.currentUser, year, month)) {
            return JsonResult.failure();
        }
        //更新 Session 中的用户信息
        UserVo userVo = this.userService.getUserVo(this.currentUser.getId());
        redisUtil.setAttribute(userVo,session);

        return JsonResult.successful();
    }

    /**
     * 反结账
     *
     * @param year
     * @param month
     * @return
     */
    @GetMapping("unCheck")
    public JsonResult unCheck(@RequestParam Integer year, @RequestParam Integer month) {
        if (!this.service.unCheck(this.currentUser, year, month)) {
            return JsonResult.failure();
        }
        //更新 Session 中的用户信息
        UserVo userVo = this.userService.getUserVo(this.currentUser.getId());
        redisUtil.setAttribute(userVo,session);
        return JsonResult.successful();
    }

}
