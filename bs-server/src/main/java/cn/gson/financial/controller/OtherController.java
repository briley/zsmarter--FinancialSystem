package cn.gson.financial.controller;

import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.service.ExportFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：ywh
 * @Date：2023/5/5 16:54
 * @Description：
 */
@RestController
@RequestMapping("/other")
public class OtherController {

    @Autowired
    private ExportFileService exportFileService;

    @PostMapping("/findState")
    public JsonResult findState(@RequestParam("mobile") String mobile) {
        return JsonResult.successful(exportFileService.findState(mobile));
    }
}
