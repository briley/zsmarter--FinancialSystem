package cn.gson.financial.controller;

import cn.gson.financial.annotation.IgnoresLogin;
import cn.gson.financial.base.BaseCrudController;
import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.exception.ServiceException;
import cn.gson.financial.kernel.model.entity.AccountingCategory;
import cn.gson.financial.kernel.model.entity.AccountingCategoryDetails;
import cn.gson.financial.kernel.service.AccountingCategoryDetailsService;
import cn.gson.financial.kernel.service.AccountingCategoryService;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itextpdf.text.log.SysoCounter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@RestController
@RequestMapping("/accounting-category-details")
@Slf4j
public class AccountingCategoryDetailsController extends BaseCrudController<AccountingCategoryDetailsService, AccountingCategoryDetails> {
    @Autowired
    private AccountingCategoryDetailsService accountingCategoryDetailsService;
    @Autowired
    private AccountingCategoryService categoryService;
    @Override
    public JsonResult list(@RequestParam Map<String, String> params) {
        LambdaQueryWrapper<AccountingCategoryDetails> qw = Wrappers.lambdaQuery();
        qw.eq(AccountingCategoryDetails::getAccountingCategoryId, params.get("accounting_category_id"));
        Page<AccountingCategoryDetails> pageable = new Page<>(Integer.parseInt(params.get("page")), 10);
        String keyword = params.get("keyword");
        if (StringUtils.isNotEmpty(keyword)) {
            qw.and(dqw -> dqw.like(AccountingCategoryDetails::getCode, keyword).or().like(AccountingCategoryDetails::getName, keyword));
        }
        qw.orderByAsc(AccountingCategoryDetails::getCode);
        return JsonResult.successful(service.page(pageable, qw));
    }

    /**
     * 检测 辅助是否能 编辑
     * @param categoryId
     * @param categoryDetailsId
     * @return
     */
    @IgnoresLogin
    @GetMapping("/checkEdit")
    public JsonResult checkIsEdit(Integer categoryId,Integer categoryDetailsId) {
        int x =  accountingCategoryDetailsService.findData(categoryId, categoryDetailsId);
        if(x > 0){
            return JsonResult.successful(false);
        }

        return JsonResult.successful(true);
    }

    /**
     * 删除元素
     *
     * @param
     * @return
     */
    @IgnoresLogin
    @GetMapping("/d")
    public JsonResult delete(Integer categoryId,Integer categoryDetailsId) {
      int x =  accountingCategoryDetailsService.findData(categoryId, categoryDetailsId);
      if(x > 0){
          return JsonResult.failure("该辅助已做账,无法删除");
      }else {
          if(categoryId == null){
              LambdaQueryWrapper<AccountingCategoryDetails> qw = new LambdaQueryWrapper<>();
              qw.eq(AccountingCategoryDetails::getId, categoryDetailsId);
              boolean f = service.remove(qw);
              if(f){
                  return JsonResult.successful(true);
              }
              return JsonResult.failure("辅助删除异常");
          }else if(categoryDetailsId == null){
              LambdaQueryWrapper<AccountingCategory> qw = new LambdaQueryWrapper<>();
              qw.eq(AccountingCategory::getId, categoryId);
             boolean f = categoryService.remove(qw);
              if(f){
                  return JsonResult.successful(true);
              }
              return JsonResult.failure("辅助删除异常");
          }

      }
      return JsonResult.failure("辅助删除异常");
    }


    /**
     * 清空元素
     *
     * @param id
     * @return
     */
    @DeleteMapping("/clear/{id:\\d+}")
    public JsonResult clear(@PathVariable Integer id) {
        boolean f =  service.clear(id, this.accountSetsId);
       if(f){
           return JsonResult.successful("清空完成");
       }
     return JsonResult.failure("亲，您不能做清空操作！辅助可能已做账");

    }

    /**
     * 加载核算项目
     *
     * @param categories
     * @return
     */
    @PostMapping("loadAuxiliaryAccountingData")
    public JsonResult loadAuxiliaryAccountingData(@RequestBody List<AccountingCategory> categories) {
        Map<Integer, List> listMap = new HashMap<>(categories.size());
        categories.stream().forEach(category -> {
            LambdaQueryWrapper<AccountingCategoryDetails> qw = Wrappers.lambdaQuery();
            qw.eq(AccountingCategoryDetails::getAccountingCategoryId, category.getId());
            qw.eq(AccountingCategoryDetails::getEnable, true);
            listMap.put(category.getId(), service.list(qw));
        });

        return JsonResult.successful(listMap);
    }
}
