package cn.gson.financial.controller;

import cn.gson.financial.annotation.IgnoresLogin;
import cn.gson.financial.kernel.constans.ComprehensiveParamesConstans;
import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.dao.ComprehensiveParamsDao;
import cn.gson.financial.kernel.model.entity.UserAccountSets;
import cn.gson.financial.kernel.model.entity.UserManage;
import cn.gson.financial.kernel.model.mapper.UserAccountSetsMapper;
import cn.gson.financial.kernel.model.vo.UserVo;
import cn.gson.financial.kernel.service.AdminService;
import cn.gson.financial.kernel.service.ExportFileService;
import cn.gson.financial.kernel.utils.*;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/7/24 15:08
 * @Description：
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private ComprehensiveParamsDao comprehensiveParamsDao;
    @Autowired
    private UserAccountSetsMapper userAccountSetsMapper;
    @Autowired
    private ExportFileService exportFileService;

    private Boolean isAdmin(HttpServletRequest request){
        UserVo user =(UserVo) request.getSession().getAttribute("user");
        LambdaQueryWrapper<UserAccountSets> uaqw = Wrappers.lambdaQuery();
        uaqw.eq(UserAccountSets::getUserId,user.getId());
        uaqw.eq(UserAccountSets::getRoleType, "admin");
        uaqw.eq(UserAccountSets::getAccountSetsId, 0);
        Integer integer = userAccountSetsMapper.selectCount(uaqw);
        if (integer>0){
            return true;
        }
        return false;
    }

    @PostMapping("/findUserManage")
    public JsonResult findUserManage(@RequestBody UserManage userManage,HttpServletRequest request) {
        Boolean admin = this.isAdmin(request);
        if (admin){
            System.out.println("userManage===" + userManage);
            List<UserManage> userManageList = adminService.findUserManage(userManage);
            System.out.println(userManageList);
            return JsonResult.successful(userManageList);
        }
        return JsonResult.failure("权限不足！");
    }

    @PostMapping("/findUserManageDownLoad")
    public void findUserManageDownLoad(@RequestBody UserManage userManage, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Boolean admin = this.isAdmin(request);
        if (admin){
            List<UserManage> userManageList = adminService.findUserManage(userManage);
            String[] title = {"用户ID","手机号","扫码时间","绑定时间","账套数","最近登陆时间",""};
            String content = comprehensiveParamsDao.queryDefaultContent(ComprehensiveParamesConstans.EXPORT_USER_MANAGE_DOWN_LOAD);
            String fileName = JsonStringUtil.JsonStringGetValueByKey(content, "fileName");
            String sheetName = JsonStringUtil.JsonStringGetValueByKey(content, "sheetName");
            List<List<Object>> list = adminService.findUserManageDownLoad(title,userManageList);
            System.out.println("list==="+ list);
            ExcelUtils.export(response,fileName,sheetName,list);
        }
    }

    @PostMapping("/findUserSource")
    public JsonResult findUserSource(@RequestBody UserManage userManage,HttpServletRequest request) {
        Boolean admin = this.isAdmin(request);
        System.out.println("userManage===" + userManage);
        if (admin){
            List<UserManage> userManageList = adminService.findUserSource(userManage);
            return JsonResult.successful(userManageList);
        }
        return JsonResult.failure("权限不足！");
    }

    @PostMapping("/findUserSourceDownLoad")
    public void findUserSourceDownLoad(@RequestBody UserManage userManage,HttpServletRequest request,HttpServletResponse response) {
        Boolean admin = this.isAdmin(request);
        System.out.println("userManage===" + userManage);
        if (admin){
            List<UserManage> userManageList = adminService.findUserSource(userManage);
            String[] title = {"用户ID","注册时间","渠道来源"};
            String content = comprehensiveParamsDao.queryDefaultContent(ComprehensiveParamesConstans.EXPORT_USER_SOURCE_DOWN_LOAD);
            String fileName = JsonStringUtil.JsonStringGetValueByKey(content, "fileName");
            String sheetName = JsonStringUtil.JsonStringGetValueByKey(content, "sheetName");
            List<List<Object>> list = adminService.findUserSourceDownLoad(title,userManageList);
            ExcelUtils.export(response,fileName,sheetName,list);
        }
    }

    @Scheduled(cron = "0 0 8,10,12,14,16,18 * * ?")//每天xx点执行一次
    public void updateInvoiceCheck(){
        //同步真票靓查验的用户数据
        adminService.updateInvoiceCheck();
    }
}


