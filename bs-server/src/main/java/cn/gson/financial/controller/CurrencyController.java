package cn.gson.financial.controller;

import cn.gson.financial.base.BaseCrudController;
import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.exception.ServiceException;
import cn.gson.financial.kernel.model.entity.Currency;
import cn.gson.financial.kernel.service.CurrencyService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/currency")
public class CurrencyController extends BaseCrudController<CurrencyService, Currency> {

    @Autowired
    private CurrencyService currencyService;

    @GetMapping("/list")
    public JsonResult currencyList() {
        return JsonResult.successful(currencyService.currencyList(this.accountSetsId));
    }

    @GetMapping("/initCurrencyList")
    public JsonResult initCurrencyList(@RequestParam(value="type") String type) {
        return JsonResult.successful(currencyService.initCurrencyList(this.accountSetsId,type));
    }

    @PostMapping("/save")
    public JsonResult saveCurrency(@RequestBody Currency entity) {
        entity.setAccountSetsId(this.accountSetsId);
        if (currencyService.saveCurrency(entity)==0){
            return JsonResult.successful();
        }else{
            return JsonResult.failure();
        }
    }

    @PostMapping("/update")
    public JsonResult updateCurrency(@RequestBody Currency entity) {
        if (currencyService.updateCurrency(entity)==0){
            return JsonResult.successful();
        }else{
            return JsonResult.failure();
        }
    }

    @PostMapping("/delete/{id:\\d+}")
    public JsonResult deleteCurrency(@PathVariable Integer id) {
        if (currencyService.deleteCurrency(id)==0){
            return JsonResult.successful();
        }else{
            return JsonResult.failure("该币也被使用");
        }
    }
}
