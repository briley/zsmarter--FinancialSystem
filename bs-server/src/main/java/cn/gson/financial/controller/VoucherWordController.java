package cn.gson.financial.controller;

import cn.gson.financial.base.BaseCrudController;
import cn.gson.financial.kernel.model.entity.VoucherWord;
import cn.gson.financial.kernel.service.VoucherWordService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/voucher-word")
public class VoucherWordController extends BaseCrudController<VoucherWordService, VoucherWord> {

}
