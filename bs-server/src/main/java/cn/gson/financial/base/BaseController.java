package cn.gson.financial.base;

import cn.gson.financial.kernel.exception.ServiceException;
import cn.gson.financial.kernel.model.vo.UserVo;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public abstract class BaseController {

    protected UserVo currentUser;

    protected Integer accountSetsId;

    protected HttpSession session;

    @ModelAttribute
    public void common(HttpServletRequest request, HttpSession session) {
        this.currentUser = (UserVo) request.getSession().getAttribute("user");
        if (this.currentUser != null) {
            this.accountSetsId = this.currentUser.getAccountSetsId();
        }
        this.session = session;
    }
}
