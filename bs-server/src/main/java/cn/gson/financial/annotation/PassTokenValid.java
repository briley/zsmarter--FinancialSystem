package cn.gson.financial.annotation;


import java.lang.annotation.*;

/**
 * 因为需要核对 请求是否 合法, 需要再次解析 token中的 信息. 但是有些请求 不需要核对 用户信息 ,因此需要跳过 二次验证
 * 用户 跳过 token 信息二次验证
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
@Documented
public @interface PassTokenValid {
}
