package cn.gson.financial.annotation;

import java.lang.annotation.*;

/**
 * @Author：ywh
 * @Date：2022/10/11 16:22
 * @Description：
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
@Documented
public @interface PassSessionCheckValid {
}
