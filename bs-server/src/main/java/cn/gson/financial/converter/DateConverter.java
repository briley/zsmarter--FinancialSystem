package cn.gson.financial.converter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.util.Date;
@Slf4j
public class DateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        if (StringUtils.isEmpty(source)) {
            return null;
        }
        try {
            return DateUtils.parseDate(source, "yyyy-MM-dd", "yyyy-MM", "yyyy-MM-dd HH:mm:ss");
        } catch (ParseException e) {
            log.error("日期格式错误:{}", source);
        }
        return null;
    }
}
