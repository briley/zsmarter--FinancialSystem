package cn.gson.financial.kernel.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "fxy_financial_subject_supplier")
public class SubjectSupplier implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "subject_id")
    private Integer subjectId;

    @TableField(value = "supplier_id")
    private Integer supplierId;

    @TableField(value = "account_sets_id")
    private Integer accountSetsId;

}