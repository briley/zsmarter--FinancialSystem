package cn.gson.financial.kernel.model.vo;

import lombok.Data;
import org.json.JSONArray;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-04-11  11:39
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class AccountBookVo {
    private Integer accountSetsId;
    private Integer subjectId;
    private List<String> timeList;
    private String subjectCode;
    private Boolean showNumPrice;
    private String companyName;
    private String subjectName;
    private Integer currencyId;
}
