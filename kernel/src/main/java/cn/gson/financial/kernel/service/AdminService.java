package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.UserManage;

import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/7/24 15:16
 * @Description：
 */
public interface AdminService {
    List<UserManage> findUserManage(UserManage userManage);

    List<UserManage> findUserSource(UserManage userManage);

    void updateInvoiceCheck();

    List<List<Object>> findUserManageDownLoad(String[] title, List<UserManage> userManageList);

    List<List<Object>> findUserSourceDownLoad(String[] title, List<UserManage> userManageList);
}
