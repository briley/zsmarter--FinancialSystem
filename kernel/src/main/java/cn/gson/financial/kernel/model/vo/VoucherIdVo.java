package cn.gson.financial.kernel.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-05-05  10:42
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class VoucherIdVo {
    private List<Integer> voucherId;
}
