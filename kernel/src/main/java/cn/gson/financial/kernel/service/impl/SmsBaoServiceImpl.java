package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.model.mapper.AllSelectMapper;
import cn.gson.financial.kernel.model.mapper.UserRoundupsMapper;
import cn.gson.financial.kernel.service.SendMesssageService;
import cn.gson.financial.kernel.service.SmsBaoService;

import cn.gson.financial.kernel.utils.RandomUtils;
import cn.gson.financial.kernel.utils.RedisCacheManager;
import cn.gson.financial.kernel.utils.ZaUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class SmsBaoServiceImpl implements SmsBaoService {

    @Resource
    RedisCacheManager redisCacheManager;
    @Resource
    private UserRoundupsMapper userRoundupsMapper;
    @Autowired
    private SendMesssageService sendMesssageService;

    @Override
    public String brdingSendMessage(String phone){
        String id = userRoundupsMapper.selectUserIdByphone(phone);
        if(id==null||id.equals("")) {
            if (redisCacheManager.get(phone + "bdtime") == null) {
                String code = RandomUtils.getSixBitRandom();

                Boolean result = sendMesssageService.sendMessage(phone,code);
//                String result = smsSendMessage(phone, code);
                if (result) {
                    redisCacheManager.set(phone + "binding", code, 5 * 60);
                    redisCacheManager.set(phone + "bdtime", "已经发送", 55);
                    return "发送成功";
                } else {
                    return "请检查手机号是否正确";
                }
            } else {
                return "一分钟后再尝试";
            }
        }
        else {
            return "该手机号已经存在";
        }
    }
    @Override
    public String powerSendMessage(String phone,Integer accountSetsId){
        Integer sum = userRoundupsMapper.selectAccount(phone);
        if (sum>=5){
            return "该用户账套数已达上限";
        }
        Integer num = userRoundupsMapper.selectUserAccount(phone,accountSetsId);
        if (num>0){
            return "您已为该用户添加权限";
        }
        if(redisCacheManager.get(phone+"ptime")==null){
        String code = RandomUtils.getSixBitRandom();
//        String result = smsSendMessage(phone,code);
            Boolean result = sendMesssageService.sendMessage(phone,code);
        if (result){
            redisCacheManager.set(phone+"power",code,5*60);
            redisCacheManager.set(phone+"ptime","已经发送",55);
            return "发送成功";
        }else {
            return "请检查手机号是否正确";
        }
    }else {
        return "一分钟后再尝试";
    }

    }

    @Override
    public String deleteAccountSendMessage(String phone){
        if(redisCacheManager.get(phone+"ptime")==null){
            String code = RandomUtils.getSixBitRandom();
//            String result = smsSendMessage(phone,code);
            Boolean result = sendMesssageService.sendMessage(phone,code);
            if (result){
                redisCacheManager.set(phone+"power",code,5*60);
                redisCacheManager.set(phone+"ptime","已经发送",55);
                return "发送成功";
            }else {
                return "请检查手机号是否正确";
            }
        }else {
            return "一分钟后再尝试";
        }

    }


}
