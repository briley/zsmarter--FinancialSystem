package cn.gson.financial.kernel.TimeScheduling;


import cn.gson.financial.kernel.model.entity.AccountSets;
import cn.gson.financial.kernel.model.mapper.SubjectMapper;
import cn.gson.financial.kernel.model.vo.BalanceVo;
import cn.gson.financial.kernel.model.vo.InitBlanceVo;
import cn.gson.financial.kernel.service.AccountSetsService;
import cn.gson.financial.kernel.service.LimitAccessUrlService;
import cn.gson.financial.kernel.service.SubjectService;
import cn.gson.financial.kernel.utils.DateUtil;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class Scheduling {
    @Autowired
    private LimitAccessUrlService accessUrlService;

    @Autowired
    private SubjectService service;
    @Autowired
    private SubjectMapper subjectMapper;

    @Autowired
    private AccountSetsService accountSetsService;

    private static final Logger logger = LoggerFactory.getLogger(Scheduling.class);

    @Scheduled(cron = "0 59 23 * * ?")
    public void cleanLimitAccessUrlToUser(){
        accessUrlService.cleanLimitAccessUrlTable();
        logger.info("23:59 清除接口访问限制");
    }

    @Scheduled(cron = "0 59 23 * * ?")
    public void updateInitBalanceVo(){
        Date now = new Date();
        int x = DateUtil.getDayOfMonth(now);
        String startTime=DateFormatUtils.format(now, "yyyy-MM")+"-01";
        String endTime=DateFormatUtils.format(now, "yyyy-MM")+"-"+x;
        String startSubject="";
        String endSubject="";
        Integer startLevel=1;
        Integer endLevel=4;
        boolean showNumPrice=false;
        boolean isOpen=false;

        List<AccountSets> list = accountSetsService.list();
        for (AccountSets accountSets : list) {
            List<BalanceVo> balanceVos = service.subjectBalanceByParam(startTime, endTime, startSubject,endSubject, startLevel, endLevel,accountSets.getId(), showNumPrice, isOpen,-1);
            balanceVos.forEach(b->{
                       if(b.getName().equals("合计")){

                       }else {
                           InitBlanceVo v = new InitBlanceVo();
                           v.setSubjectId(b.getSubjectId());
                           v.setSubjectName(b.getName());
                           v.setSubjectCode(b.getCode());
                           v.setAccountSetsId(accountSets.getId());
                           v.setBalanceDirection(b.getBalanceDirection());
                           v.setEndingDebitBalance(b.getEndingDebitBalance());
                           v.setEndingDebitBalanceNum(b.getEndingDebitBalanceNum());
                           v.setEndingCreditBalence(b.getEndingCreditBalance());
                           v.setEndingCreditBalenceNum(b.getEndingCreditBalanceNum());
                           v.setRemberDate(DateFormatUtils.format(now, "yyyy-MM"));
                           v.setParentId(b.getParentId());


                           v.setCurrencyEndingDebitBalance(b.getCurrencyEndingDebitBalance());//外币期末借方余额
                           v.setCurrencyEndingCreditBalance(b.getCurrencyEndingCreditBalance());//外币期末贷方余额
                           InitBlanceVo blanceVo =  subjectMapper.findBySubIdAndAccountSwtId(v.getSubjectId(),
                                   accountSets.getId(),
                                   DateFormatUtils.format(now, "yyyy-MM"),
                                   v.getSubjectCode());
                           if(blanceVo == null){
                               subjectMapper.addInitBalanceVo(v);
                           }else {
                                subjectMapper.updateInitBlanceVo(v);
                           }
                       }
           });
        }
    }
}
