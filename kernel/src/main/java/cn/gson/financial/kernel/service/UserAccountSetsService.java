package cn.gson.financial.kernel.service;

import java.util.List;
import cn.gson.financial.kernel.model.entity.UserAccountSets;
import com.baomidou.mybatisplus.extension.service.IService;

public interface UserAccountSetsService extends IService<UserAccountSets>{


    int batchInsert(List<UserAccountSets> list);

    void deletAllVoucher(Integer id);
}
