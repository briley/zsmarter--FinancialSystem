package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.AccountingCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


public interface AccountingCategoryService extends IService<AccountingCategory> {


    int batchInsert(List<AccountingCategory> list);

}


