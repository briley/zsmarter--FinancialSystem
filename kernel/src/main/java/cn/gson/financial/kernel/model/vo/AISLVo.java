package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * 核算项目明细账VO
 *
 */
@Data
public class AISLVo {

    //凭证日期
    private String voucherDate;
    //凭证字号
    private String voucherWord;
    //凭证字号编码
    private String voucherWordCode;
    //摘要
    private String summary;
    //借方金额
    private Double debitAmount;
    //贷方金额
    private Double creditAmount;
    //接待方向
    private String balanceDirection;
    //科目ID
    private Integer subjectId;
    //科目编码
    private String subjectCode;
    //科目名称
    private String subjectName;
    // 辅助核算-数量
    private Double num;
    // 辅助核算-数量-单价
    private Double price;
}
