package cn.gson.financial.kernel.model.mapper;


import cn.gson.financial.kernel.model.entity.ReportTemplate;
import cn.gson.financial.kernel.model.entity.ReportTemplateItems;
import cn.gson.financial.kernel.model.entity.User;
import cn.gson.financial.kernel.model.vo.AccountRoleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AllSelectMapper {

    Integer selectLirunReport(@Param("accountSetsId") Integer accountSetsId,@Param("name") String name);

    User selectUserByPhone(@Param("phone")String phone);

    String selectCreateMember(@Param("id") Integer id);

    List<ReportTemplateItems> selectReportTemplateItemsById(Integer lrId);

    List<String> selectWord(@Param("id") Integer accountSetsId);

    Integer selectAccountRole(@Param("id")Integer accountSetsId,@Param("role")String role);
}
