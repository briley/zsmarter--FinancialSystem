package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.model.entity.FileBean;

import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/5/6 10:50
 * @Description：
 */
public interface FileManageService {

    List<FileBean> findFileBean(int accountSetsId, int voucherId);
}
