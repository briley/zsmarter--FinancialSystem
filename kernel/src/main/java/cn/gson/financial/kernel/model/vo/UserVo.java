package cn.gson.financial.kernel.model.vo;

import cn.gson.financial.kernel.model.entity.AccountSets;
import cn.gson.financial.kernel.model.entity.Checkout;
import cn.gson.financial.kernel.model.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = false)
public class UserVo extends User {

    private AccountSets accountSets;

    private String role;

    private List<AccountSets> accountSetsList;

    private List<Checkout> checkoutList;

    private String uuid;
    private Boolean isAdmin;//是否管理员
}
