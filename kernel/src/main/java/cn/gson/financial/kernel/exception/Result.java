package cn.gson.financial.kernel.exception;

import lombok.Data;

import java.io.Serializable;

@Data
public class Result implements Serializable {
    private String code;
    private String msg;
    private Object body;
    public Result(ResultCode resultCode, Object data){
        this.code = resultCode.status();
        this.msg = resultCode.message();
        this.body = data;
    }
    public Result(ResultCode resultCode){
        this.code = resultCode.status();
        this.msg = resultCode.message();
    }

    // 返回成功
    public static Result success(){
        Result resultVO = new Result(ResultCode.SUCCESS);
        return resultVO;
    }
    // 返回成功
    public static Result success(Object data){
        Result resultVO = new Result(ResultCode.SUCCESS,data);
        return resultVO;
    }
    // 返回失败
    public static Result fail(ResultCode resultCode){
        Result resultVO = new Result(resultCode);
        return resultVO;
    }
    // 返回失败
    public static Result fail(ResultCode resultCode, Object data){
        Result resultVO = new Result(resultCode,data);
        return resultVO;
    }
}
