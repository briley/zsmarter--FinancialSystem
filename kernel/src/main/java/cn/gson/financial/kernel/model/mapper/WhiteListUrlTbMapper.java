package cn.gson.financial.kernel.model.mapper;


import cn.gson.financial.kernel.model.entity.WhiteListUrlTbBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 白名单
 */
@Mapper
@Repository
public interface WhiteListUrlTbMapper {

     WhiteListUrlTbBean findWhiteUrl(@Param("url") String url);
     
     List<String> findByType(@Param("type") String type);
}
