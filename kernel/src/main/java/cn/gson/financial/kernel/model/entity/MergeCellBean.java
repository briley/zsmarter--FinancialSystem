package cn.gson.financial.kernel.model.entity;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/17 10:55
 * @Description：
 */
@Data
public class MergeCellBean {
    private int firstRow;
    private int lastRow;
    private int firstCol;
    private int lastCol;
}
