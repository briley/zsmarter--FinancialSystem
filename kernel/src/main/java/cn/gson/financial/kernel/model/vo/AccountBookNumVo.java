package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/7/7 19:01
 * @Description：
 */
@Data
public class AccountBookNumVo {
    private Integer subjectId;
    private String unit;
    private String ffsSubjectCode;
    private String ffsSubjectName;
    private String balanceDirection;
    private String currencyName;
    private String exchangeRate;
    private String voucherDate;
    private Double debitAmount; //借方金额
    private Double debitNum; // 借方数量
    private Double creditAmount; //贷方金额
    private Double creditNum; //贷方金额
    private Double currencyDebit; //外币借方金额
    private Double currencyCredit; //外币贷方金额
    private Double balance; //余额
    private Double balanceNum; //余额数量
    private Double currencyBalance; //外币余额

    private String currencyAccounting;
}
