package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.OldAccountImportBean;
import cn.gson.financial.kernel.model.entity.User;
import cn.gson.financial.kernel.model.vo.AuxiliaryVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author：ywh
 * @Date：2023/4/17 20:17
 * @Description：
 */

@Mapper
@Repository
public interface ExportFileMapper {

    int oldAccountImport(OldAccountImportBean oldAccountImportBean);


    List<User> findRealName(@Param("list") List<String> list);

//    @MapKey("voucherDetailsId")
   List<AuxiliaryVo> findAuxiliary(@Param("voucherDetailsIdList") List<Integer> voucherDetailsIdList);

    int findMaxOldAccountTaskId(int accountSetsId);

    int updateOldAccountState(@Param("id") int id,@Param("state") int state);

    OldAccountImportBean findState(@Param("mobile") String mobile);
//    List<AuxiliaryVo> findAuxiliary(@Param("voucherDetailsIdList") List<Integer> voucherDetailsIdList);
}
