package cn.gson.financial.kernel.dao;

import cn.gson.financial.kernel.model.mapper.VoucherDetailsMapper;
import cn.gson.financial.kernel.model.vo.ChronologicalAccountVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ChronologicalAccountDao {

    @Autowired
    private VoucherDetailsMapper detailsMapper;


    public List<ChronologicalAccountVo> selectChronologicalAccountByParam(
            Integer accsetsId,
            String sTime,
            String eTime,
            String sWordCode,
            String eWordCode,
            String word,
            String summary,
            String createMemberName,
            String sSubjectCode,
            String eSubjectCode,
            String sMoney,
            String eMoney,
            Integer sort,
            List<String> wordCodeList,
            List<String> subCodeList
    ){
        List<ChronologicalAccountVo> list = detailsMapper.selectChronologicalAccount(
                accsetsId,
                sTime, eTime,
                sWordCode,
                eWordCode,
                word,
                summary,
                createMemberName,
                sSubjectCode,
                eSubjectCode,
                sMoney,
                eMoney,
                sort,
                wordCodeList,
                subCodeList);
        for (ChronologicalAccountVo vo:list) {
            vo.setVoucherWord(vo.getVoucherWord()+"-"+vo.getVoucherWordNum());
        }
        return  list;
    }
    public List<ChronologicalAccountVo> selectChronologicalAccountByParamBaseCurrency(
            Integer accsetsId,
            String sTime,
            String eTime,
            String sWordCode,
            String eWordCode,
            String word,
            String summary,
            String createMemberName,
            String sSubjectCode,
            String eSubjectCode,
            String sMoney,
            String eMoney,
            Integer sort,
            List<String> wordCodeList,
            List<String> subCodeList
    ){
        List<ChronologicalAccountVo> list = detailsMapper.selectChronologicalAccountBaseCurrency(
                accsetsId,
                sTime, eTime,
                sWordCode,
                eWordCode,
                word,
                summary,
                createMemberName,
                sSubjectCode,
                eSubjectCode,
                sMoney,
                eMoney,
                sort,
                wordCodeList,
                subCodeList
                );
        for (ChronologicalAccountVo vo:list) {
            vo.setVoucherWord(vo.getVoucherWord()+"-"+vo.getVoucherWordNum());
        }
        return  list;
    }

    public List<ChronologicalAccountVo> selectChronologicalAccountByCurrencyParam(
            Integer accsetsId,
            String sTime,
            String eTime,
            String sWordCode,
            String eWordCode,
            String word,
            String summary,
            String createMemberName,
            String sSubjectCode,
            String eSubjectCode,
            String sMoney,
            String eMoney,
            Integer sort,
            List<String> wordCodeList,
            List<String> subCodeList,
            Integer currencyId
    ){
        List<ChronologicalAccountVo> list = detailsMapper.selectChronologicalAccountCurrency(
                accsetsId,
                sTime, eTime,
                sWordCode,
                eWordCode,
                word,
                summary,
                createMemberName,
                sSubjectCode,
                eSubjectCode,
                sMoney,
                eMoney,
                sort,
                wordCodeList,
                subCodeList,
                currencyId);
        for (ChronologicalAccountVo vo:list) {
            vo.setVoucherWord(vo.getVoucherWord()+"-"+vo.getVoucherWordNum());
        }
        return  list;
    }

}
