package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.common.DateUtil;
import cn.gson.financial.kernel.common.DoubleValueUtil;
import cn.gson.financial.kernel.model.entity.*;
import cn.gson.financial.kernel.model.mapper.AllSelectMapper;
import cn.gson.financial.kernel.model.mapper.SubjectMapper;
import cn.gson.financial.kernel.model.mapper.VoucherMapper;
import cn.gson.financial.kernel.model.vo.*;
import cn.gson.financial.kernel.service.AllSelectService;
import cn.gson.financial.kernel.service.ReportTemplateService;
import cn.gson.financial.kernel.service.SubjectService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.service.impl
 * @Author: 龙泽霖
 * @CreateTime: 2023-04-03  09:43
 * @Description: TODO
 * @Version: 1.0
 */
@Service
@Slf4j
public class AllSelectServiceimpl extends ServiceImpl<VoucherMapper, Voucher> implements AllSelectService {

    @Autowired
    private SubjectService subjectService;
    @Resource
    private SubjectMapper subjectMapper;
    @Resource
    private VoucherMapper voucherMapper;
    @Resource
    private AllSelectMapper allSelectMapper;
    @Autowired
    private ReportTemplateService reportTemplateService;


    @Override
    public List<VoucherCreateVo> selectCreateMember(List<VoucherCreateVo> list){
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId()!=null&&!list.get(i).getId().equals("")){
                list.get(i).setMember(allSelectMapper.selectCreateMember(list.get(i).getId()));
            }
        }
        return list;
    }

    @Override
    public List<String> selectWord(Integer accountSetsId) {
        List<String> list = allSelectMapper.selectWord(accountSetsId);
        List<String> listSup = new ArrayList<>();
        if (list.size()==0){
            return list;
        }
        for (int i = 0; i <list.size() ; i++) {
            if (listSup.size()==0){
                listSup.add(list.get(i));
            }
            for (int j = 0; j <listSup.size() ; j++) {
                if (!list.get(i).equals(listSup.get(j))){
                    listSup.add(list.get(i));
                    break;
                }
            }
        }
        return listSup;
    }


    @Override
    public List<Voucher> voucherSelect(VoucherSelectVo vo,Integer id) {
        Boolean flag = vo.getVoucherAccount().matches("(.*)-(.*)");
        List<String> accountList = Arrays.asList(vo.getVoucherAccount().split("-"));
        List<Voucher> list;
        if (flag) {
            list = voucherMapper.selectVoucher2(vo.getVoucherWord(), vo.getTimeStart(), vo.getTimeEnd(), vo.getVoucherState(), vo.getVoucherSummary(), id, accountList.get(0), accountList.get(1));
//                list = voucherMapper.selectVoucher2(vo,id,accountList.get(0),accountList.get(1));
        } else {
            list = voucherMapper.selectVoucher1(vo, id);
        }
        for (Voucher voucher : list) {
            log.info("直接在数据库查询的凭证：" + voucher);
        }
        return this.dealWithVoucherSelect(flag, vo, list);
    }


    //处理查询出来的数据
    public List dealWithVoucherSelect(Boolean flag, VoucherSelectVo vo,List<Voucher> list){
        System.out.println(vo);
        List<Voucher> oneList = new ArrayList<>();
        List<Voucher> twoList = new ArrayList<>();
        List<Voucher> threeList = new ArrayList<>();
        List<Voucher> fourList = new ArrayList<>();

        List<Voucher> intersection0 = new ArrayList<>(list);
        List<Voucher> intersection1 = new ArrayList<>(list);
        List<Voucher> intersection2 = new ArrayList<>(list);
        List<Voucher> intersection3 = new ArrayList<>(list);
        System.out.println(list);
        if(!vo.getVoucherState().equals("")){
            for (int i = 0; i < list.size(); i++) {
                    if (!list.get(i).getState().equals(vo.getVoucherState())){
                        list.remove(i);
                        i--;
                    }
            }
        }
        if(!vo.getVoucherWord().equals("")){
            for (int i = 0; i < list.size(); i++) {
                    if (!list.get(i).getWord().equals(vo.getVoucherWord())){
                        list.remove(i);
                        i--;
                    }
            }
        }
        if(!vo.getVoucherSummary().equals("")){

            for (int i = 0; i < list.size(); i++) {
                for (int j = 0; j <list.get(i).getDetails().size() ; j++) {
                    String str = list.get(i).getDetails().get(j).getSummary();
                    Boolean res =  str.contains(vo.getVoucherSummary());
                    if (res==true){
                        threeList.add(list.get(i));
                    }
                }
            }
            intersection0.retainAll(threeList);
            list=intersection0;
        }
        if(!vo.getMoneyStart().equals("")||!vo.getMoneyEnd().equals("")){
            if (!vo.getMoneyStart().equals("")&&vo.getMoneyEnd().equals("")){
                for (int i = 0; i < list.size(); i++) {
                    for (int j = 0; j <list.get(i).getDetails().size() ; j++) {
                        double detailsMoney = 0d;
                        if (list.get(i).getDetails().get(j).getCreditAmount()==null){
                            detailsMoney=list.get(i).getDetails().get(j).getDebitAmount();
                        }else {
                            detailsMoney=list.get(i).getDetails().get(j).getCreditAmount();
                        }

                        if (detailsMoney>= Integer.parseInt(vo.getMoneyStart())){
                            fourList.add(list.get(i));
                        }
                    }
                    if (list.get(i).getCreditAmount() >= Integer.parseInt(vo.getMoneyStart())) {
                        fourList.add(list.get(i));
                    }
                }
                for (int i = 0; i < list.size() ; i++) {

                }
                intersection3.retainAll(fourList);
                list=intersection3;
            }
            else if (vo.getMoneyStart().equals("")&&!vo.getMoneyEnd().equals("")){
                for (int i = 0; i < list.size(); i++) {
                    for (int j = 0; j <list.get(i).getDetails().size() ; j++) {
                        double detailsMoney = 0d;
                        if (list.get(i).getDetails().get(j).getCreditAmount()==null){
                            detailsMoney=list.get(i).getDetails().get(j).getDebitAmount();
                        }else {
                            detailsMoney=list.get(i).getDetails().get(j).getCreditAmount();
                        }
                        if (detailsMoney <= Integer.parseInt(vo.getMoneyEnd())){
                            fourList.add(list.get(i));
                        }
                    }
                    if (list.get(i).getCreditAmount() <= Integer.parseInt(vo.getMoneyEnd())) {
                        fourList.add(list.get(i));
                    }
                }

                intersection3.retainAll(fourList);
                list=intersection3;
            }else if (!vo.getMoneyStart().equals("")&&!vo.getMoneyEnd().equals("")){
                for (int i = 0; i < list.size(); i++) {
                    for (int j = 0; j <list.get(i).getDetails().size() ; j++) {
                        double detailsMoney = 0d;
                        if (list.get(i).getDetails().get(j).getCreditAmount()==null){
                            detailsMoney=list.get(i).getDetails().get(j).getDebitAmount();
                        }else {
                            detailsMoney=list.get(i).getDetails().get(j).getCreditAmount();
                        }
                        if (detailsMoney >= Integer.parseInt(vo.getMoneyStart())
                                && detailsMoney <= Integer.parseInt(vo.getMoneyEnd())){
                            fourList.add(list.get(i));
                        }
                    }
                    if (list.get(i).getCreditAmount() >= Integer.parseInt(vo.getMoneyStart())
                    &&list.get(i).getCreditAmount() <= Integer.parseInt(vo.getMoneyEnd())) {
                        fourList.add(list.get(i));
                    }
                }
                intersection3.retainAll(fourList);
                list=intersection3;
            }
        }
        if(!vo.getVoucherAccount().equals("")){
//            Boolean flag1 = vo.getVoucherAccount().matches("(.*)-(.*)");
            Boolean flagD1 = vo.getVoucherAccount().matches("(.*)，(.*)");

            if (flagD1){
                vo.setVoucherAccount(vo.getVoucherAccount().replace("，",","));
            }
            Boolean flag2 = vo.getVoucherAccount().matches("(.*),(.*)");
             if (flag.equals(false)&&flag2.equals(true)){
                List<String> accountList2 = Arrays.asList(vo.getVoucherAccount().split(","));
                for (int x = 0; x <accountList2.size() ; x++) {
                    for (int i = 0; i < list.size(); i++) {
                        for (int j = 0; j <list.get(i).getDetails().size() ; j++) {
                            if (list.get(i).getDetails().get(j).getSubjectCode().contains(accountList2.get(x))){
//                                list.remove(i);
//                                i--;
                                oneList.add(list.get(i));
                            }
                        }
                    }
                }

                intersection1.retainAll(oneList);
                list=intersection1;

            }
            else if (flag.equals(false)&&flag2.equals(false)){

                for (int i = 0; i < list.size(); i++) {
                    for (int j = 0; j <list.get(i).getDetails().size() ; j++) {
                        if (list.get(i).getDetails().get(j).getSubjectCode().contains(vo.getVoucherAccount())){
                            oneList.add(list.get(i));
//
                        }
                    }
                }
                intersection1.retainAll(oneList);
                list=intersection1;
            }
        }
        if(!vo.getVoucherNo(). equals("")){
            Boolean flag1 = vo.getVoucherNo().matches("(.*)-(.*)");
            Boolean flagD2 = vo.getVoucherNo().matches("(.*)，(.*)");
            if (flagD2){
                vo.setVoucherNo(vo.getVoucherNo().replace("，",","));
            }
            Boolean flag2 = vo.getVoucherNo().matches("(.*),(.*)");
            if (flag1.equals(true)&&flag2.equals(false)){

                List<String> noList1 = Arrays.asList(vo.getVoucherNo().split("-"));
                System.out.println(noList1);
                for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getCode()< Integer.parseInt(noList1.get(0))
                                ||list.get(i).getCode()> Integer.parseInt(noList1.get(1))){
                            list.remove(i);
                            i--;
//                            sa.add(list.get(i));
                      }
                }
            }
            else if (flag1.equals(false)&&flag2.equals(true)){
                List<String> noList2 = Arrays.asList(vo.getVoucherNo().split(","));
                System.out.println(noList2);
                for (int x = 0; x <noList2.size() ; x++) {
                    for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getCode().equals(Integer.parseInt(noList2.get(x)))){
//                                list.remove(i);
//                                i--;
                                twoList.add(list.get(i));
                            }
                    }
                }

                intersection2.retainAll(twoList);
                list=intersection2;

            }
            else if (flag1.equals(false)&&flag2.equals(false)){
                for (int i = 0; i < list.size(); i++) {
                        if (!list.get(i).getCode().equals(Integer.parseInt(vo.getVoucherNo()))){
                            list.remove(i);
                            i--;
//                            sa.add(list.get(i));
                        }
                }
            }
        }
        return list;
    }

    @Override
    public List<IndexTargetVo> IndexTarget(Integer accountSetsId,String timeStart,String timeEnd) {

        Integer lrId = allSelectMapper.selectLirunReport(accountSetsId,"利润表");
        Integer zcId = allSelectMapper.selectLirunReport(accountSetsId,"资产负债表");

        List<ReportTemplateItems> lrlist = allSelectMapper.selectReportTemplateItemsById(lrId);
        List<ReportTemplateItems> zclist = allSelectMapper.selectReportTemplateItemsById(zcId);
        Long lrIdLong = new Long((long)lrId);
        Map<Integer, ReportDataVo> lrMap = reportTemplateService.viewReport(accountSetsId,lrIdLong,timeStart,timeEnd);
        Long zcIdLong = new Long((long)zcId);
        Map<Integer, ReportDataVo> zcMap = reportTemplateService.viewReport(accountSetsId,zcIdLong,timeStart,timeEnd);
        Integer a1=0;
        Integer a2=0;
        Integer a3=0;
        Integer a4=0;
        Integer a5=0;
        Integer a6=0;
        Integer a7=0;
        Integer a8=0;
        for (int i = 0; i < zclist.size(); i++) {
            if (zclist.get(i).getTitle().equals("货币资金")){
                a1 = zclist.get(i).getId();
            }
            if (zclist.get(i).getTitle().equals("应收账款")){
                a2 = zclist.get(i).getId();
            }
            if (zclist.get(i).getTitle().equals("存货")){
                a3 = zclist.get(i).getId();
            }
            if (zclist.get(i).getTitle().equals("固定资产")||zclist.get(i).getTitle().equals("固定资产账面价值")){
                a4 = zclist.get(i).getId();
            }
            if (zclist.get(i).getTitle().equals("应付账款")){
                a5 = zclist.get(i).getId();
            }
            if (zclist.get(i).getTitle().equals("应交税费")){
                a6 = zclist.get(i).getId();
            }
        }
        for (int i = 0; i < lrlist.size(); i++) {
            if (lrlist.get(i).getTitle().equals("一、营业收入")){
                a7 = lrlist.get(i).getId();
            }
            if (lrlist.get(i).getTitle().equals("三、利润总额（亏损失以“-”号填列）")){
                a8 = lrlist.get(i).getId();
            }
        }
        List<IndexTargetVo> list = new ArrayList<>();
        if (zcMap.get(a1).getCurrentPeriodAmount()!=null){
            IndexTargetVo vo1= new IndexTargetVo();
            vo1.setTarget(zcMap.get(a1).getCurrentPeriodAmount());
            vo1.setName("货币资金");
            list.add(vo1);
        }
        if (zcMap.get(a2).getCurrentPeriodAmount()!=null){
            IndexTargetVo vo2= new IndexTargetVo();
            vo2.setTarget(zcMap.get(a2).getCurrentPeriodAmount());
            vo2.setName("应收账款");
            list.add(vo2);
        }
        if (zcMap.get(a3).getCurrentPeriodAmount()!=null){
            IndexTargetVo vo3= new IndexTargetVo();
            vo3.setTarget(zcMap.get(a3).getCurrentPeriodAmount());
            vo3.setName("存货");
            list.add(vo3);
        }
        if (zcMap.get(a4).getCurrentPeriodAmount()!=null){
            IndexTargetVo vo4= new IndexTargetVo();
            vo4.setTarget(zcMap.get(a4).getCurrentPeriodAmount());
            vo4.setName("固定资产");
            list.add(vo4);
        }
        if (zcMap.get(a5).getCurrentPeriodAmount()!=null){
            IndexTargetVo vo5= new IndexTargetVo();
            vo5.setTarget(zcMap.get(a5).getCurrentPeriodAmount());
            vo5.setName("应付账款");
            list.add(vo5);
        }
        if (zcMap.get(a6).getCurrentPeriodAmount()!=null){
            IndexTargetVo vo6= new IndexTargetVo();
            vo6.setTarget(zcMap.get(a6).getCurrentPeriodAmount());
            vo6.setName("应交税费");
            list.add(vo6);
        }
        if (lrMap.get(a7).getCurrentPeriodAmount()!=null){
            IndexTargetVo vo7= new IndexTargetVo();
            vo7.setTarget(lrMap.get(a7).getCurrentPeriodAmount());
            vo7.setName("营业收入");
            list.add(vo7);
        }
        if (lrMap.get(a8).getCurrentPeriodAmount()!=null){
            IndexTargetVo vo8= new IndexTargetVo();
            vo8.setTarget(lrMap.get(a8).getCurrentPeriodAmount());
            vo8.setName("利润总额");
            list.add(vo8);
        }
        return list;
    }
    @Override
    public IndexTargetVo IndexTargetYear(String date,Integer accountSetsId) {
        IndexTargetVo vo= new IndexTargetVo();


        return vo;
    }

}
