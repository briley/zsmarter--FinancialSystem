package cn.gson.financial.kernel.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import cn.gson.financial.kernel.model.entity.UserAccountSets;
import cn.gson.financial.kernel.model.mapper.UserAccountSetsMapper;
import cn.gson.financial.kernel.service.UserAccountSetsService;

@Service
public class UserAccountSetsServiceImpl extends ServiceImpl<UserAccountSetsMapper, UserAccountSets> implements UserAccountSetsService{

    @Override
    public int batchInsert(List<UserAccountSets> list) {
        return baseMapper.batchInsert(list);
    }

    @Override
    public void deletAllVoucher(Integer id) {
        baseMapper.deletAllVoucher(id);
    }
}
