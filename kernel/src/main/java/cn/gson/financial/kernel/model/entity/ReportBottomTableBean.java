package cn.gson.financial.kernel.model.entity;

import lombok.Data;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.entity
 * @Author: 龙泽霖
 * @CreateTime: 2023-04-20  17:46
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class ReportBottomTableBean {
    private String tableId;
    private String templateId;
    private String amount;
    private String createDate;
    private String accountSetsId;
    private String note;
}
