package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.AccountSets;
import cn.gson.financial.kernel.model.entity.Currency;
import cn.gson.financial.kernel.model.entity.Voucher;
import cn.gson.financial.kernel.model.entity.VoucherDetails;
import cn.gson.financial.kernel.model.vo.VoucherDetailCurrencyVo;
import cn.gson.financial.kernel.model.vo.VoucherDetailVo;
import cn.gson.financial.kernel.model.vo.VoucherSelectVo;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.*;

@Mapper
public interface VoucherMapper extends BaseMapper<Voucher> {
    int batchInsert(@Param("list") List<Voucher> list);

    Integer selectMaxCode(@Param("accountSetsId") Integer accountSetsId, @Param("word") String word, @Param("year") int year, @Param("month") int month);

    List<Voucher> selectVoucher(@Param(Constants.WRAPPER) Wrapper wrapper);

    List<Voucher> selectVoucher1(@Param("vo") VoucherSelectVo vo ,@Param("id") Integer id);

    List<Voucher> selectVoucher2(@Param("voucherWord") String voucherWord,
                                 @Param("timeStart") String timeStart,
                                 @Param("timeEnd") String timeEnd,
                                 @Param("voucherState") String voucherState,
                                 @Param("voucherSummary") String voucherSummary,
                                 @Param("id") Integer id,@Param("ids") String ids,@Param("ide") String ide);

    IPage<Voucher> selectVoucher(IPage<Voucher> page, @Param(Constants.WRAPPER) Wrapper wrapper);

    List<VoucherDetailVo> selectAccountBookDetailsSip(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds, @Param("accountDate") Date accountDate, @Param("cId") Integer cId);

    List<VoucherDetailVo> selectAccountBookDetails(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds, @Param("accountDate") Date accountDate);
    List<VoucherDetailCurrencyVo> selectCurrAccountBookDetails(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds, @Param("accountDate") Date accountDate ,@Param("id")Integer id);

    List<VoucherDetailVo> selectAccountBookDetailsByParam(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds, @Param("sTime") String sTime,@Param("eTime") String eTime);


    List<VoucherDetailVo> selectAccountBookDetails1(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds, @Param("accountDate1") Date accountDate1, @Param("accountDate2") Date accountDate2);

    List<VoucherDetailVo> selectAccountBookStatistical(@Param("accountSetsId") Integer accountSetsId, @Param("subjectId") Integer subjectId, @Param("subjectIds") List<Integer> subjectIds,@Param("accountDate") Date accountDate);

    List<VoucherDetailVo> selectAccountBookStatisticalSup(@Param("accountSetsId") Integer accountSetsId, @Param("subjectId") Integer subjectId, @Param("subjectIds") List<Integer> subjectIds,@Param("accountDate") Date accountDate);
    List<VoucherDetailCurrencyVo> selectAccountBookStatisticalCurrency(@Param("accountSetsId") Integer accountSetsId, @Param("subjectId") Integer subjectId, @Param("subjectIds") List<Integer> subjectIds, @Param("accountDate") Date accountDate,@Param("cId")Integer cId);

    List<VoucherDetailVo> selectAccountBookStatisticalByParam(@Param("accountSetsId") Integer accountSetsId, @Param("subjectId") Integer subjectId, @Param("subjectIds") List<Integer> subjectIds,@Param("sTime") String sTime,@Param("eTime") String eTime);

    List<VoucherDetailVo> selectAccountBookInitialBalance(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds, @Param("accountDate") Date accountDate);
    List<VoucherDetailCurrencyVo> selectAccountBookInitialBalance1(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds, @Param("accountDate") Date accountDate,@Param("code") String code,@Param("cId")Integer cId);

    List<VoucherDetailVo> selectAccountBookInitialBalanceSup(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds, @Param("accountDate") Date accountDate ,@Param("code") String code,@Param("cId") Integer cId);

    List<VoucherDetailVo> selectAccountBookInitialBalanceSupByRMB(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds, @Param("accountDate") Date accountDate ,@Param("code") String code);

    //    List<VoucherDetailVo> selectAccountBookInitialBalanceByParam(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds,,@Param("sTime")String sTime,@Param("eTime")String eTime);//刘哥的
    List<VoucherDetailVo> selectAccountBookInitialBalanceByParam(@Param("accountSetsId") Integer accountSetsId, @Param("subjectIds") List<Integer> subjectIds,@Param("eTime")String eTime);//ywh修改的

    List<VoucherDetailVo> selectAccountBookInitialBalanceByParam1(@Param("accountSetsId") Integer accountSetsId, @Param("eTime")String eTime);//ywh修改的

    List<Map<String, Object>> selectBrokenData(@Param("accountSetsId") Integer accountSetsId, @Param("year") Integer year, @Param("month") Integer month);

    List<VoucherDetailVo> selectReportStatistical(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("codes") Collection<String> codes,
            @Param("accountDate") Date accountDate);

    /**
     * 各科目的期年统计数据 (三大报表 动态)
     * @param accountSetsId
     * @param codes
     * @param
     * @return
     */
    List<VoucherDetailVo> selectReportStatisticalByParam(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("codes") Collection<String> codes,
            @Param("sTime") String sTime,
            @Param("eTime") String eTime);

    List<Map<String, Object>> selectHomeReport(@Param("accountSetsId") Integer accountSetsId, @Param("voucherYear") Integer year);

    List<Map<String, Object>> selectHomeCostReport(@Param("accountSetsId") Integer accountSetsId, @Param("voucherYear") Integer year, @Param("voucherMonth") Integer month);

    List<Map<String, Object>> selectHomeCashReport(@Param("accountSetsId") Integer accountSetsId, @Param("voucherYear") Integer year, @Param("voucherMonth") Integer month);

    Integer selectBeforeId(@Param("accountSetsId") Integer accountSetsId, @Param("currentId") Integer currentId);

    Integer selectNextId(@Param("accountSetsId") Integer accountSetsId, @Param("currentId") Integer currentId);

    List<VoucherDetailVo> selectSubjectDetail(
            @Param("subjectIds") Set<Integer> subjectIds,
            @Param("accountSetsId") Integer accountSetsId,
            @Param("monthBegin") Date monthBegin,
            @Param("monthEnd") Date monthEnd,
            @Param("showNumPrice") boolean showNumPrice);

    List<VoucherDetailCurrencyVo> selectSubjectDetailByCurrency(
            @Param("subjectIds") Set<Integer> subjectIds,
            @Param("accountSetsId") Integer accountSetsId,
            @Param("monthBegin") Date monthBegin,
            @Param("monthEnd") Date monthEnd,
            @Param("showNumPrice") boolean showNumPrice);

    List<VoucherDetailVo> selectSubjectDetailByParam(
            @Param("subjectIds") Set<String> subjectIds,
            @Param("accountSetsId") Integer accountSetsId,
            @Param("monthBegin") String monthBegin,
            @Param("monthEnd") String monthEnd,
            @Param("showNumPrice") boolean showNumPrice);

    List<VoucherDetailVo> selectSubjectDetailByParam1(
            @Param("subjectIds") List<String> subjectIds,
            @Param("accountSetsId") Integer accountSetsId,
            @Param("monthBegin") String monthBegin,
            @Param("monthEnd") String monthEnd,
            @Param("cId") Integer cId);

    Integer updateAudit(@Param(Constants.WRAPPER) Wrapper wrapper);

    List<VoucherDetailVo> selectReportBalanceStatistical(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("codes") List<String> codes,
            @Param("accountDate") Date accountDate);

    /**
     * 三大报表-改版 动态 - 资产负债查余额
     */
    List<VoucherDetailVo> selectReportBalanceStatisticalByParam(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("codes") List<String> codes,
            @Param("sTime") String sTime,
            @Param("eTime") String eTime);

    List<VoucherDetailVo> selectReportInitBalance(@Param("accountSetsId") Integer accountSetsId, @Param("codes") List<String> codes);

    Date selectMaxVoucherDate(@Param("accountSetsId") Integer accountSetsId);


    /**
     * 查询 部门 / 项目 各科目的期年统计数据
     * @return
     */
    List<VoucherDetailVo> selectReportStatistical2(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("codes") Collection<String> codes,
            @Param("sTime")String sTime,
            @Param("eTime")String etime,
            @Param("ids")Integer ids
    );

    VoucherDetailVo accumulatedThisYear(@Param("accSetsId") Integer accSetsId,@Param("subjectId") Integer subjectId,@Param("endTime") String endTime);

    Currency selectCurrency(@Param("id")Integer currencyId);

    List<VoucherDetailVo> selectyearAmount(@Param("accountSetsId") Integer accountSetsId, @Param("codes") List<String> codes);

    VoucherDetailVo selectyearAmountByOne(@Param("accountSetsId") Integer accountSetsId, @Param("code") String codes, @Param("currencyId")Integer currencyId);

    VoucherDetailVo selectyearAmountByOneByCurrency(@Param("accountSetsId") Integer accountSetsId, @Param("code") String codes,@Param("cId") Integer cId);

    String selectBalanceDirection(@Param("subjectCode")String subjectCode, @Param("accountSetsId")Integer accountSetsId);

    Double selectInitYearAmount( @Param("accountSetsId")Integer accountSetsId,@Param("subjectCode") String subjectCode);

    int checkAccount(@Param("accountSetsId")Integer accountSetsId);

    List<Voucher> checkVoucher(@Param("accountSetsId")Integer accountSetsId,@Param("year") Integer year,@Param("month") Integer month);

    VoucherDetailVo findStartDetail(@Param("accountSets") Integer accountSets,@Param("subjectId") Integer subjectId);
}