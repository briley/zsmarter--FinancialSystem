package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * 核算项目明细账 最终展示VO
 */
@Data
public class AISLFinalVo {

    //凭证日期
    private String voucherDate;
    //凭证字号
    //private String voucherWord;
    //凭证字号+编码
    private String voucherWordCode;
    //摘要
    private String summary;
    //借方金额
    private Double debitAmount;
    //贷方金额
    private Double creditAmount;
    //接待方向
    private String balanceDirection;
    // 科目 = 科目编码+科目名称
    private String subject;
    //科目余额
    private Double subjectBlance;
    // 辅助核算 - 数量
    private Double num;
    // 辅助核算 -数量 -单价
    private Double price;
}
