package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.CheckOutSetBean;
import cn.gson.financial.kernel.model.entity.Checkout;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;


@Mapper
public interface CheckoutMapper extends BaseMapper<Checkout> {
    int batchInsert(@Param("list") List<Checkout> list);

    int updateCheck(@Param("status")int status, @Param("checkDate")Date checkDate, @Param("accountSetsId")Integer accountSetsId, @Param("monthEnd")Date monthEnd);

    CheckOutSetBean selectCheckoutSet( @Param("accountSetsId")Integer accountSetsId, @Param("year") Integer year,  @Param("month")Integer month);
}