package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.VoucherTemplate;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;

public interface VoucherTemplateService extends IService<VoucherTemplate>{


    int batchInsert(List<VoucherTemplate> list);

}
