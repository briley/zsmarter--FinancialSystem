package cn.gson.financial.kernel.model.vo;

import lombok.Data;

import java.util.List;

/***
 * 查询 部门 项目 利润参数VO
 */
@Data
public class ReportBMXMVo {

    /**
     * 账套Id
     */
    private Integer accountSetsId;
    /**
     * 要查询的 报表ID
     */
    private Long reportTemlateId;

    /** 如果 是月报 则只需要判断 开始时间
     * 如果是 季报  则需要判断 开始 结束两个时间
     *
     * 开始时间
     */
    private String sTime;
    /**
     * 结束时间
     */
    private String eTime;
    /**
     * 部门 - 项目 互相切换
     * true为 部门
     * false为 项目
     */
    private Boolean isChange;

    /**
     * 具体的 部门或项目 id集合
     */
    private List<Integer> ids;

    private String companyName;//账套名
}
