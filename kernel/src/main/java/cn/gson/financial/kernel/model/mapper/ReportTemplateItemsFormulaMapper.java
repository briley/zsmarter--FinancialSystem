package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.ReportTemplateItemsFormula;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface ReportTemplateItemsFormulaMapper extends BaseMapper<ReportTemplateItemsFormula> {
    int batchInsert(@Param("list") List<ReportTemplateItemsFormula> list);
}