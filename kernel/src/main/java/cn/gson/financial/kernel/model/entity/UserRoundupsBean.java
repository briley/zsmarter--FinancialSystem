package cn.gson.financial.kernel.model.entity;

import lombok.Data;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.entity
 * @Author: 龙泽霖
 * @CreateTime: 2023-03-27  15:50
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class UserRoundupsBean {
    private String id;
    private String roundup;
    private String roundupTime;
    private String roundupNum;
}
