package cn.gson.financial.kernel.service.impl;



import cn.gson.financial.kernel.dao.LimitAccessUrlDao;
import cn.gson.financial.kernel.model.entity.LimitAccessUrltbBean;
import cn.gson.financial.kernel.service.LimitAccessUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Throwable.class)
public class LimitAccessUrlServiceImpl implements LimitAccessUrlService {
    @Autowired
    private LimitAccessUrlDao dao;

    @Override
    public LimitAccessUrltbBean findByIdUrl(String idUrl) {
        return dao.findByIdUrl(idUrl);
    }

    @Override
    public void cleanLimitAccessUrlTable() {
        dao.cleanLimitAccessUrlTable();
    }

    @Override
    public void addLimitAccessUrlBean(LimitAccessUrltbBean bean) {
        dao.addLimitAccessUrl(bean);
    }

    @Override
    public void updateAccessNum(String idUrl, String num) {
        dao.updateAccessNum(idUrl, num);
    }

}
