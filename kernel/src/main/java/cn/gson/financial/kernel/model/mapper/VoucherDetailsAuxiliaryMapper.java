package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.AccountingCategoryDetails;
import cn.gson.financial.kernel.model.entity.VoucherDetailsAuxiliary;
import cn.gson.financial.kernel.model.vo.AISLVo;
import cn.gson.financial.kernel.model.vo.AccountingCategoryDetailsVo;
import cn.gson.financial.kernel.model.vo.VoucherDetailVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Mapper
public interface VoucherDetailsAuxiliaryMapper extends BaseMapper<VoucherDetailsAuxiliary> {
    int batchInsert(@Param("list") List<VoucherDetailsAuxiliary> list);

    List<AccountingCategoryDetails> selectByAccountBlock(@Param("accountSetsId") Integer accountSetsId, @Param("auxiliaryId") Integer auxiliaryId);

    List<AccountingCategoryDetailsVo> selectByAccountBlock1(@Param("accountSetsId") Integer accountSetsId, @Param("auxiliaryId") Integer auxiliaryId);

    List<AccountingCategoryDetailsVo> selectByAccountBlock2(@Param("accountSetsId") Integer accountSetsId, @Param("auxiliaryId") Integer auxiliaryId);

    //所有辅助项目 -动态
    List<AccountingCategoryDetails> selectByAccountBlockByParam(@Param("accountSetsId") Integer accountSetsId,
                                                                @Param("auxiliaryId") Integer auxiliaryId,
                                                                @Param("detailsAuxiliaryId")String detailsAuxiliaryId
                                                                );

    //所有辅助项目 -动态
    List<AccountingCategoryDetails> selectByAccountBlockByParam2(@Param("accountSetsId") Integer accountSetsId,
                                                                @Param("auxiliaryId") Integer auxiliaryId,
                                                                @Param("detailsAuxiliaryId")String detailsAuxiliaryId
    );

    List<VoucherDetailVo> selectAccountBookDetails(@Param("accountSetsId") Integer accountSetsId, @Param("auxiliaryId") Integer auxiliaryId, @Param("accountDate") Date accountDate, @Param("auxiliaryItemId") Integer auxiliaryItemId);
    List<VoucherDetailVo> selectAccountBookDetailsByParams(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("auxiliaryId") Integer auxiliaryId,
            @Param("startDate") Date startDate,
            @Param("endDate") Date endDate,
            @Param("auxiliaryItemId") Integer auxiliaryItemId,
            @Param("sSubcode") String sSubjectCode,
            @Param("eSubjectCode") String eSubjectCode,
            @Param("showNumPrice")boolean showNumPrice,
            @Param("subCodeList") List<String> subCodeLidt);

    List<VoucherDetailVo> selectAccountBookStatistical(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("auxiliaryId") Integer auxiliaryId,
            @Param("startDate") Date startDate,
            @Param("endDate") Date endDate,
            @Param("auxiliaryItemId") Integer auxiliaryItemId);

    List<VoucherDetailVo> selectAccountBookStatisticalByParams(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("auxiliaryId") Integer auxiliaryId,
            @Param("startDate") Date startDate,
            @Param("endDate") Date endDate,
            @Param("auxiliaryItemId") Integer auxiliaryItemId,
            @Param("sSubcode") String sSubjectCode,
            @Param("eSubjectCode") String eSubjectCode,
            @Param("showNumPrice")boolean showNumPrice,
            @Param("subCodeList") List<String> subCodeLidt);

    /**
     * 核算项目明细 纯期初 数据
     * @param accountSetsId
     * @param auxiliaryId
     * @param startDate
     * @param endDate
     * @param auxiliaryItemId
     * @param sSubjectCode
     * @param eSubjectCode
     * @param showNumPrice
     * @param subCodeList
     * @return
     */
    List<VoucherDetailVo> selectAccountBookStatisticalByParams2(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("auxiliaryId") Integer auxiliaryId,
            @Param("startDate") Date startDate,
            @Param("endDate") Date endDate,
            @Param("auxiliaryItemId") Integer auxiliaryItemId,
            @Param("sSubcode") String sSubjectCode,
            @Param("eSubjectCode") String eSubjectCode,
            @Param("showNumPrice")boolean showNumPrice,
            @Param("subCodeList") List<String> subCodeList);



    /**
     *
     * @param accountSetsId
     * @param endDate
     * @param auxiliaryItemId
     * @param sSubjectCode
     * @param eSubjectCode
     * @param showNumPrice
     * @param subCodeLidt
     * @return
     */
    List<VoucherDetailVo> selectAccountBookStatisticalByParams3(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("endDate") Date endDate,
            @Param("auxiliaryItemId") Integer auxiliaryItemId,
            @Param("sSubcode") String sSubjectCode,
            @Param("eSubjectCode") String eSubjectCode,
            @Param("showNumPrice")boolean showNumPrice,
            @Param("subCodeList") List<String> subCodeLidt);

    // 动态查询 凭证明细
    List<VoucherDetailVo> selectAccountBookStatisticalByParam(@Param("accountSetsId") Integer accountSetsId,
                                                              @Param("auxiliaryId") Integer auxiliaryId,
                                                              @Param("startDate") String startDate,
                                                              @Param("endDate") String endDate,
                                                              @Param("sSubjectCode") String sSubjectCode,
                                                              @Param("eSubjectCode") String eSubjectCode,
                                                              @Param("auxiliaryItemId") Integer auxiliaryItemId,
                                                              @Param("subList") List<String> subList,
                                                              @Param("showNumPrice") boolean showNumPrice
        );

    // 核算科目余额 纯期初查询
    List<VoucherDetailVo> selectAccountBookStatisticalByParam2(@Param("accountSetsId") Integer accountSetsId,
                                                              @Param("auxiliaryId") Integer auxiliaryId,
                                                              @Param("showNumPrice") boolean showNumPrice,
                                                              @Param("eTime") String eTime
    );


    //核算项目明细动态查询
    List<AISLVo> selectAISLByParams(@Param("accSetsId") Integer accSetsId,
                                    @Param("sSubjectCode") String sSubjectCode,
                                    @Param("eSubjectCode") String eSubjectCode,
                                    @Param("sTime")String sTime,
                                    @Param("eTime")String eTime,
                                    @Param("categoryId")Integer categoryId,
                                    @Param("categoryCode")String categoryCode);


    Integer findData(@Param("categoryId") Integer categoryId,@Param("categoryDetailsId") Integer categoryDetailsId);


    List<VoucherDetailVo> selectAccountBookStatisticalByParamsYwh1(@Param("accountSetsId") Integer accountSetsId,
                                                                   @Param("accountingCategoryId") Integer accountingCategoryId,
                                                                   @Param("accountingCategoryDetailsId") Integer accountingCategoryDetailsId,
                                                                   @Param("showNumPrice") Boolean showNumPrice,
                                                                   @Param("subCodeList") List<String> subCodeList);

    List<VoucherDetailVo> selectAccountBookStatisticalByParamsYwh2(@Param("accountSetsId") Integer accountSetsId,
                                                                   @Param("subjectId") Integer subjectId,
                                                                   @Param("accountingCategoryId") Integer accountingCategoryId,
                                                                   @Param("accountingCategoryDetailsId") Integer accountingCategoryDetailsId,
                                                                   @Param("sTime") String sTime,
                                                                   @Param("eTime") String eTime,
                                                                   @Param("showNumPrice") Boolean showNumPrice);

    HashMap<String,Double> selectBalance(@Param("accountSetsId") Integer accountSetsId,
                          @Param("subjectId") Integer subjectId,
                          @Param("accountingCategoryId") Integer accountingCategoryId,
                          @Param("accountingCategoryDetailsId") Integer accountingCategoryDetailsId,
                          @Param("sTime") String sTime,
                          @Param("showNumPrice") Boolean showNumPrice,
                          @Param("subCodeList") List<String> subCodeList,
                          @Param("detailSubjectCode")String detailSubjectCode);

    List<VoucherDetailVo> findVoucherDetails(@Param("accountSetsId") Integer accountSetsId,
                            @Param("subjectId") Integer subjectId,
                            @Param("accountingCategoryId") Integer accountingCategoryId,
                            @Param("accountingCategoryDetailsId") Integer accountingCategoryDetailsId,
                            @Param("sTime") String sTime,
                            @Param("eTime") String eTime,
                            @Param("showNumPrice") Boolean showNumPrice);

    List<String> selectUnit(@Param("id") Integer id,
                            @Param("sTime") String sTime,
                            @Param("eTime") String eTime);
}
