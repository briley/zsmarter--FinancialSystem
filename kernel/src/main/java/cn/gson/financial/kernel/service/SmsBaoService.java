package cn.gson.financial.kernel.service;


public interface SmsBaoService {

//    String smsSendMessage(String phone,String code);

    String brdingSendMessage(String phone);

    String powerSendMessage(String phone,Integer accountSetsId);

    String deleteAccountSendMessage(String phone);
}
