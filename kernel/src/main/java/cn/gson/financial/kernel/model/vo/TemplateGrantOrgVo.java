package cn.gson.financial.kernel.model.vo;

import cn.gson.financial.kernel.model.entity.ReportTemplate;
import lombok.Data;

import java.util.List;

@Data
public class TemplateGrantOrgVo {

    private ReportTemplate template;

    private List<Integer> orgIds;
}
