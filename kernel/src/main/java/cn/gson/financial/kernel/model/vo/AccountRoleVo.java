package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-08-02  17:49
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class AccountRoleVo {

    private Integer mangerNum;
    private Integer viewNum;
    private Integer makingNum;

}
