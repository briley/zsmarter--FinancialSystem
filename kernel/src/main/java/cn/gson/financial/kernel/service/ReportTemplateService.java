package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.ReportTemplate;
import cn.gson.financial.kernel.model.vo.IndexTargetVo;
import cn.gson.financial.kernel.model.vo.ReportBMXMVo;
import cn.gson.financial.kernel.model.vo.ReportDataVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public interface ReportTemplateService extends IService<ReportTemplate> {


    int batchInsert(List<ReportTemplate> list);

    /**
     * 三大报表 原版
     * @param accountSetsId
     * @param id
     * @param accountDate
     * @return
     */
    Map<Integer, ReportDataVo> view(Integer accountSetsId, Long id, Date accountDate);

    /**
     * 三大报表改版 -动态
     * @param accountSetsId
     * @param id
     * @param
     * @return
     */
    Map<Integer, ReportDataVo> viewReport(Integer accountSetsId, Long id, String sTime,String eTime);

    List<IndexTargetVo> IndexTarget(Integer accountSetsId, String timeStart, String timeEnd);

    Map<Integer, ReportDataVo> view2(
            Integer accountSetsId,
            Long reportTemlateId,
            String sTime,
            String eTime,
            Integer bmxmId);
}
