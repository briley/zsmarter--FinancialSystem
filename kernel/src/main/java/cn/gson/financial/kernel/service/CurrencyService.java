package cn.gson.financial.kernel.service;

import java.util.List;
import cn.gson.financial.kernel.model.entity.Currency;
import cn.gson.financial.kernel.model.vo.CurrencyVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2019 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : ${PACKAGE_NAME}</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2019年07月30日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public interface CurrencyService extends IService<Currency> {


    int batchInsert(List<Currency> list);

    int saveCurrency(Currency entity);

    int updateCurrency(Currency entity);

    int deleteCurrency(Integer id);

    List<CurrencyVo> currencyList(Integer accountSetsId);

    List<Currency> initCurrencyList(Integer accountSetsId, String type);
}


