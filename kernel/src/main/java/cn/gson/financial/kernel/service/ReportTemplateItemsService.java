package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.ReportTemplateItems;
import cn.gson.financial.kernel.model.entity.ReportTemplateItemsFormula;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


public interface ReportTemplateItemsService extends IService<ReportTemplateItems> {


    int batchInsert(List<ReportTemplateItems> list);

    /**
     * 保存公式信息
     *
     * @param formulas
     * @param accountSetsId
     * @param templateItemsId
     */
    void saveFormula(Integer templateItemsId, List<ReportTemplateItemsFormula> formulas, Integer accountSetsId);

}




