package cn.gson.financial.kernel.model.vo;

import lombok.Data;

import java.util.Map;

@Data
public class DepartmentOrProjectVO {

   private String departentProjectName;

   private Map<Integer, ReportDataVo> map;
}
