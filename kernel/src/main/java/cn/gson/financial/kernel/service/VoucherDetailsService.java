package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.VoucherDetails;
import cn.gson.financial.kernel.model.vo.CategoryVo;
import cn.gson.financial.kernel.model.vo.ChronologicalAccountVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.*;

public interface VoucherDetailsService extends IService<VoucherDetails> {


    int batchInsert(List<VoucherDetails> list);

    /**
     * 期初数据
     *
     * @param accountSetsId
     * @param type
     * @return
     */
    List<VoucherDetails> balanceList(Integer accountSetsId, String type,Integer currencyId);

    /**
     * 期初试算平衡
     *
     * @param accountSetsId
     * @return
     */
    Map<String, Map<String, Double>> trialBalance(Integer accountSetsId);

    void saveAuxiliary(Integer accountSetsId, HashMap<String, Object> entity);

    List<VoucherDetails> auxiliaryList(Integer accountSetsId, String type, Integer currencyId);

    /**
     * 科目会计期间的累计金额
     *
     * @param accountSetsId
     * @param codeList
     * @param currentAccountDate
     * @return
     */
    Map<String, VoucherDetails> getAggregateAmount(Integer accountSetsId, Set<String> codeList, Date currentAccountDate);



    List<ChronologicalAccountVo> selectChronologicalAccountByParam(
            Integer accsetsId,
            String sTime,
            String eTime,
            String wordCode,
            String word,
            String summary,
            String createMemberName,
            String subjectCode,
            String sMoney,
            String eMoney,
            Integer sort
    );

    List<ChronologicalAccountVo> selectChronologicalAccountByCurrencyParam(
            Integer accsetsId,
            String sTime,
            String eTime,
            String wordCode,
            String word,
            String summary,
            String createMemberName,
            String subjectCode,
            String sMoney,
            String eMoney,
            Integer sort,
            Integer currencyId
    );

    int updateQiChu(VoucherDetails voucherDetails);

    //期初使用
    boolean removeAuxiliary(Integer subjectId,String name);

    Boolean checkIsInsertQiChu(Integer accountSetsId, Integer subjectId, List<CategoryVo> categoryVoList);

    void delQIChu(Integer accountSetsId, Integer subjectId, String subjectCode);
}











