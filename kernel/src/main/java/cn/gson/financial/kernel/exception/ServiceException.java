package cn.gson.financial.kernel.exception;

public class ServiceException extends RuntimeException {

    private Integer code = 500;

    public ServiceException(String msg) {
        super(msg);
    }

    public ServiceException(String msg, Integer code) {
        super(msg);
        this.code = code;
    }

    public ServiceException(String msg, Object... objects) {
        super(String.format(msg, objects));
    }

    public Integer getCode() {
        return code;
    }
}
