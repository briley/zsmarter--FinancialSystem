package cn.gson.financial.kernel.model.vo;

import cn.gson.financial.kernel.model.entity.Currency;
import cn.gson.financial.kernel.model.entity.Subject;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SubjectVo extends Subject {

    private String subjectFullName;

    private String parentCode;

    private Boolean isLeaf;

    @JSONField(name = "subjectFullName")
    public String getSubjectFullName() {
        return this.getCode() + "-" + this.getName();
    }

    private List<SubjectVo> children = new ArrayList<>();

}
