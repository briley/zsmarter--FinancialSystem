package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * checkLogin()的参数
 */
@Data
public class CheckLoginVo {
   private String ticket;
}
