package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-04-12  17:20
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class IndexTargetVo {
    private String name;
    private Double target;

}
