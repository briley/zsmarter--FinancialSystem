package cn.gson.financial.kernel.dao;


import cn.gson.financial.kernel.model.entity.WhiteListUrlTbBean;
import cn.gson.financial.kernel.model.mapper.WhiteListUrlTbMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WhiteListUrlTbDao {
    @Autowired
    private WhiteListUrlTbMapper mapper;

    public WhiteListUrlTbBean findByUrl(String url){
        return mapper.findWhiteUrl(url);
    }

    public List<String> findByType(String type){
        return mapper.findByType(type);
    }
}
