package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.AccountSets;
import cn.gson.financial.kernel.model.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


public interface AccountSetsService extends IService<AccountSets> {


    int batchInsert(List<AccountSets> list);

    List<AccountSets> myAccountSets(Integer uid);

    String addUser(Integer accountSetsId, String mobile, String role);

    User addNewUser(Integer accountSetsId, String mobile, String role);

    void removeUser(Integer accountSetsId, Integer uid);

    void updateUserRole(Integer accountSetsId, Integer uid, String role);

    /**
     * 账套移交
     *
     * @param accountSetsId
     * @param currentUserId
     * @param mobile
     */
    void handOver(Integer accountSetsId, Integer currentUserId, String mobile);

    void updateEncode(Integer accountSetsId, String encoding, String newEncoding);

}




