package cn.gson.financial.kernel.service;

import java.util.List;

/**
 * @Author：ywh
 * @Date：2022/9/16 15:37
 * @Description：
 */
public interface ComprehensiveParamsService {

    String queryDefaultContent(String code);

    List<String> findByType(String type);
}
