package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.SubjectMateriel;
import cn.gson.financial.kernel.model.entity.SubjectSupplier;
import cn.gson.financial.kernel.model.vo.SubjectMaterielVo;

import java.util.List;


public interface SubjectMaterielService {

    void saveAll(List<SubjectMateriel> entity, Integer accountSetsId);

    List<SubjectMaterielVo> materielSubject(Integer accountSetsId);

}
