package cn.gson.financial.kernel.excelexport;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/21 14:37
 * @Description：
 */

@Data
public class ProfitExport {
    //利润表导出
    private String project;//项目
    private String lineNo;//行次
    private String accumulatedAmountThisYear;//本年累计金额
    private String currentAmount;//本期金额

}
