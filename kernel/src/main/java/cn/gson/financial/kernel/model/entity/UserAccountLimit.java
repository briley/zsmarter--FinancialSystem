package cn.gson.financial.kernel.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户账套 数量限制
 */
@Data
@TableName(value = "fxy_financial_user_account_limit")
public class UserAccountLimit implements Serializable {
    @TableField(value = "user_openid")
    private String userOpenId;
    @TableField(value = "account_limit_num")
    private Integer accountLimitNum=3;

    private static final long serialVersionUID = 1L;

    public static final String COL_USER_OPENID = "user_openid";

    public static final String COL_ACCOUNT_LIMIT_NUM = "account_limit_num";
}
