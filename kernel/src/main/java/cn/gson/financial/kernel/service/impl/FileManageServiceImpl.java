package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.model.entity.FileBean;
import cn.gson.financial.kernel.model.mapper.FileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/5/6 10:50
 * @Description：
 */
@Service
public class FileManageServiceImpl implements FileManageService {


    @Autowired
    private FileMapper fileMapper;
    @Override
    public List<FileBean> findFileBean(int accountSetsId, int voucherId) {
        return fileMapper.findFileBean(accountSetsId, voucherId);
    }
}
