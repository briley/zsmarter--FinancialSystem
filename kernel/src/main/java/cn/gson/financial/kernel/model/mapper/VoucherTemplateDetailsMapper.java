package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.VoucherTemplateDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface VoucherTemplateDetailsMapper extends BaseMapper<VoucherTemplateDetails> {
    int batchInsert(@Param("list") List<VoucherTemplateDetails> list);
}