package cn.gson.financial.kernel.constans;

/**
 * 易变参数 匹配数据库TYPE
 */
public class ComprehensiveParamesConstans {


    /*
     * 白名单 跳过session验证的类型码
     * */
    public static final String WHITE_LIST_URL_IS_SESSION_CHECK = "0001";

    /**
     * 借
     */
    public static final String JIE = "001";

    /**
     * 贷
     */
    public static final String DAI = "002";

    /*
     * 总账导出code,接口名字:/export/downloadLedgerToExcel
     * */
    public static final String EXPORT_ALL_ACCOUNT_EXPORT = "004";

    /*
     * 明细账导出code,接口名字:/export/downloadSubsidiaryToExcel
     * */
    public static final String EXPORT_SUB_LEDGER_EXPORT = "005";

    /*
     * 科目余额导出code,接口名字:/export/downloadAccountBalanceToExcel
     * */
    public static final String EXPORT_ACCOUNT_BALANCE_EXPORT = "006";

    /*
     * 序时账导出code,接口名字:/export/downloadChronologicalAccountToExcel
     * */
    public static final String EXPORT_CHRONOLOGICAL_ACCOUNT_EXPORT = "007";

    /*
     * 凭证导出code,接口名字:/export/voucherExport
     * */
    public static final String EXPORT_VOUCHER_EXPORT = "008";

    /*
     * 现金流量表导出code,接口名字:/export/downloadCashFlowToExcel
     * */
    public static final String EXPORT_CASH_FLOW_EXPORT = "009";

    /*
     * 利润表导出code,接口名字:/export/downloadProfitToExcel
     * */
    public static final String EXPORT_PROFIT_EXPORT = "010";

    /*
     * 资产负债表表导出code,接口名字:/export/downloadAssetsLiabilitiesToExcel
     * */
    public static final String EXPORT_ASSETS_LIABILITIES_EXPORT = "011";

    /*
     * 旧账导入时选择的软件名列表
     * */
    public static final String SOFT_WARE_NAME_LIST = "012";

    /*
     * 资旧账导入的下载excel模板表头
     * */
    public static final String OLD_ACCOUNT_EXCEL_HEAD = "013";

    /*
     * 旧账导入的期初下载模板
     * */
    public static final String DOWN_LOAD_OLD_ACCOUNT_MODEL = "014";

//    /**
//     * 合计
//     */
//    public static final String PING = "007";
//
//    /**
//     * 合计
//     */
//    public static final String HEJI = "008";

    /*
     * 用户信息表code,接口名字:/admin/findUserManageDownLoad
     * */
    public static final String EXPORT_USER_MANAGE_DOWN_LOAD = "016";

    /*
     * 用户来源表code,接口名字:/admin/findUserSourceDownLoad
     * */
    public static final String EXPORT_USER_SOURCE_DOWN_LOAD = "017";

    public static final String WHITE_NAME_LIST = "018";

}
