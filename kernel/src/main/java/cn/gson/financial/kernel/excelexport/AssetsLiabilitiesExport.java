package cn.gson.financial.kernel.excelexport;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/21 14:41
 * @Description：
 */
@Data
public class AssetsLiabilitiesExport {
    //资产负债表导出
    private String asset;//资产
    private String aLineNo;//行次
    private String aEndingBalance;//期末余额
    private String aBeginningBalance;//年初余额
    private String liabilitiesEquity;//负债和所有者权益
    private String lLineNo;//行次
    private String lEndingBalance;//期末余额
    private String lBeginningBalance;//年初余额
}
