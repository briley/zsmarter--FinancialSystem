package cn.gson.financial.kernel.utils;


import cn.gson.financial.kernel.exception.GlobalHandleException;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseUtil {

    public static String checkValueIsNull(String value){
        if (value == null){
            return "";
        }
        return value;
    }
    public static Double checkDoubleIsNull(Double value){
        if (value == null){
            return 0.0;
        }
        return value;
    }

    public static String checkValueIsSpace(String value){
        if (value == null || value.equals("")){
            return " ";
        }
        return value;
    }

    public static String getTokenUserId(HttpServletRequest request){
        String token = request.getHeader("token");// 从 http 请求头中取出 token
        String userId = "";
        try {
             userId = JWTUtil.getAudience(token);
        } catch (GlobalHandleException e) {
            e.printStackTrace();
        }
        //处理id和ip
        String[] ary = userId.split("\\*");
        userId = ary[0];
        return userId;
    }

    public static Date strToDate(String time,String pattern){
        Date date = new Date();
        try {
            date = new SimpleDateFormat(pattern).parse(time);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static boolean isNumber(String str) {
        Pattern pattern = Pattern.compile("-?[0-9]+(\\\\.[0-9]+)?");
        Matcher m = pattern.matcher(str);
        return m.matches();
    }
}
