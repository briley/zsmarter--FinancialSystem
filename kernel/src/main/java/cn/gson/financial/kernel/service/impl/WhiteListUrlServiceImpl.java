package cn.gson.financial.kernel.service.impl;


import cn.gson.financial.kernel.dao.WhiteListUrlTbDao;
import cn.gson.financial.kernel.model.entity.WhiteListUrlTbBean;
import cn.gson.financial.kernel.service.WhiteListUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional(rollbackFor = Throwable.class)
public class WhiteListUrlServiceImpl implements WhiteListUrlService {
    @Autowired
    private WhiteListUrlTbDao dao;

    @Override
    public WhiteListUrlTbBean findByUrl(String url) {
        return dao.findByUrl(url);
    }

    public List<String> findByType(String type){
        return dao.findByType(type);
    }

}
