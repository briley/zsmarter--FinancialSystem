package cn.gson.financial.kernel.model.vo;

import cn.gson.financial.kernel.model.entity.Subject;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 设置 - 期初VO
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class InitialBalanceVo extends Subject {
    //科目id
    private Integer subjectId;
    //余额id
    private Integer balanceId;
    //期初余额
    private Double beginBalance;
    //借方累计
    private Double debitAccumulation;
    //贷方累计
    private Double creditAccumulation;
    //初始余额
    private Double initialBalance;
    //数量
    private Double num;
    /**
     * 期初累计借方
     */
    private Double cumulativeDebit;

    /**
     * 期初累计贷方
     */
    private Double cumulativeCredit;
    //累计借方数量
    private Double cumulativeDebitNum;
    //累计贷方数量
    private Double cumulativeCreditNum;

    private boolean leaf;

    private List<InitialBalanceVo> children;
    //是否可以删除该辅助科目
    private boolean ynDel;

    private Double currencyCumulativeDebit;
    private Double currencyCumulativeCredit;

    private Double currencyDebit;//原币借方金额

    private Double currencyCredit;//原币贷方金额
}
