package cn.gson.financial.kernel.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-06-12  10:34
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class CurrencyVo {
    private Integer id;
    private String code;
    private String name;
    private Double exchangeRate;
    private Boolean localCurrency;
    private Integer accountSetsId;
    private Boolean flag;
}
