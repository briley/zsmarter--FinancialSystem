package cn.gson.financial.kernel.utils;

import org.apache.commons.lang3.StringUtils;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * ID生成工具
 */
public class IdCreateUtil {


    private static final long EPOCH = 1479533469598L; //开始时间,固定一个小于当前时间的毫秒数
    private static final int max12bit = 4095;
    private static final long max41bit= 1099511627775L;
    private static String machineId = "10100110101" ; // 机器ID

    /**
     * 创建ID
     * @return
     */
    public static String createInvitationCode(){

        long time = System.currentTimeMillis() - EPOCH  + max41bit;
        // 二进制的 毫秒级时间戳
        String base = Long.toBinaryString(time);
        // 序列数
        String randomStr = StringUtils.leftPad(Integer.toBinaryString(new Random().nextInt(max12bit)),12,'0');
        if(StringUtils.isNotEmpty(machineId)){
                machineId = StringUtils.leftPad(machineId, 10, '0');
        }
        //拼接
        String appendStr = base + machineId + randomStr;
        // 转化为十进制 返回
        BigInteger bi = new BigInteger(appendStr, 2);
        String id20 = bi.toString();
        return  getId(id20);
    }

    public static String getId(String id){
        String[] ary = {"a","b","c","d","e","f","g","h","i","j","k","m","l",
                "n","o","p","q","r","s","t","u","v","w","x","y","z","A",
                "B","C","D","E","F","G","H","I","J","K","M","L","I","O",
                "P","Q","R","S","T","U","V","W","X","Y","Z","-","_","!","#","@","$","^"};
        Random random = new Random();
        int[] indexAry = new int[10];
        for(int i = 0; i < 10; i++){
            int y = random.nextInt(19);
            indexAry[i] = y;
        }
        String newId = "";
        for(int i = 0; i < indexAry.length; i++){
            newId += id.charAt(indexAry[i]);
        }
        //分段 截取 和 拼接
        String str1 = "",str2 = "";
        for(int i = 0; i < 6; i++){
            int index = random.nextInt(newId.length() - 1);
            int codeIndex = random.nextInt(58);
            str1 = newId.substring(0, index);
            str2 = newId.substring(index, newId.length());
            newId = str1 + ary[codeIndex] + str2;
        }
        int[] idIndex = new int[5];
        for(int i = 0; i < 5; i++){
            idIndex[i] = random.nextInt(16);
        }
        String idx = "";
        for(int i = 0; i < idIndex.length; i++){
//            System.out.println(idIndex[i]);
           idx += newId.charAt(idIndex[i]);
        }
        return idx;
    }

    //任务流水生成工具--时间戳版
    public static String createTaskId(){
        return String.valueOf(System.currentTimeMillis());
    }

    //日期生成工具
    public static String createDate(){
        return new SimpleDateFormat("yyyyMMdd24HHmmss").format(new Date());
    }

    //日期生成工具
    public static String createDate(Date date){
        return new SimpleDateFormat("yyyyMMdd24HHmmss").format(date);
    }

    //用户编号生成工具
    public static  String createUserId(){
        return "zst" + System.currentTimeMillis();
    }

    //用户编号生成工具
    public static  String createMsgId(){
        return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
    }

    //用户编号生成工具
    public static  String createMsgId(Date date){
        return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(date);
    }

    //模拟生成sessionId
    public static String createSessionId(){
        char cha[]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};
        String sessionId = "";
        for(int i=0;i<30;i++){
            int index=(int)(Math.random()*(cha.length));
            sessionId += cha[index];
        }
        return sessionId;
    }

    //模拟生成docId
    public static String createFileId(){
        char cha[]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                    'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                    '0','1','2','3','4','5','6','7','8','9'};
        String docId = "";
        for(int i=0;i<15;i++){
            int index=(int)(Math.random()*(cha.length));
            docId += cha[index];
        }
        return docId;
    }
}
