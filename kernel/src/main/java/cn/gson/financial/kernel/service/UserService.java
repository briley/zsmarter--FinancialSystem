package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.User;
import cn.gson.financial.kernel.model.vo.UserVo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


public interface UserService extends IService<User> {


    int batchInsert(List<User> list);

    UserVo login(String mobile, String password);

    UserVo selectUser(String userOpenId);

    UserVo getUserVo(Integer userId);

    List<UserVo> listByAccountSetId(Integer accountSetsId);

    boolean updateUserPhoneByOpenId(String phone,String openId);

    Integer updateAccessAccountSetsRoleToUser(
            Integer userIdOld,
           Integer userIdNew);

    User selectUserByMobileOrOpenId(String key);

}


