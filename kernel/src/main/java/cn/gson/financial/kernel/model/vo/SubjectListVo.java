package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/5/19 16:23
 * @Description：
 */
@Data
public class SubjectListVo {
    private SubjectVo subjectVo;
    private boolean qiChuHaveData;//期初是否有数据
}
