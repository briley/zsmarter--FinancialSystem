package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-05-10  17:17
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class BottomTableVo {
    private String templateId;
    private String notes;
    private String amountMonth;
    private String amountYear;
}
