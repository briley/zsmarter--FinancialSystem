package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.model.entity.ReportTemplateItemsFormula;
import cn.gson.financial.kernel.model.mapper.ReportTemplateItemsFormulaMapper;
import cn.gson.financial.kernel.service.ReportTemplateItemsFormulaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ReportTemplateItemsFormulaServiceImpl extends ServiceImpl<ReportTemplateItemsFormulaMapper, ReportTemplateItemsFormula> implements ReportTemplateItemsFormulaService {

    @Override
    public int batchInsert(List<ReportTemplateItemsFormula> list) {
        return baseMapper.batchInsert(list);
    }
}
