package cn.gson.financial.kernel.model.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class SubjectMaterielVo  implements Serializable {

    private Integer subjectId;

    /**
     * 科目类型
     */
    private Object type;

    /**
     * 科目编码
     */
    private String code;

    /**
     * 科目名称
     */
    private String name;


    private Integer materielId;

    @JSONField(name = "subjectFullName")
    public String getSubjectFullName() {
        return this.getCode() + "-" + this.getName();
    }
}
