package cn.gson.financial.kernel.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//时间工具
public class TimeUtil {
    public static String getYearMonthDay(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        return df.format(new Date());
    }

    public static String get24Time(){
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd24HHmmss");
        return df.format(new Date());
    }

    public static String getyyyy_MM_dd_HH_mm_ss_Time(String get24Time){
        String year = get24Time.substring(0, 4);
        String month = get24Time.substring(4, 6);
        String day = get24Time.substring(6, 8);
        String hour = get24Time.substring(10,12);
        String minute = get24Time.substring(12, 14);
        String second = get24Time.substring(14, 16);
        String data = year + "-" + month + "-" + day +" " + hour + ":" + minute + ":" +second;
        return data;

    }
    public static String getNianYueRi(String get24Time){
        String year = get24Time.substring(0, 4);
        String month = get24Time.substring(4, 6);
        String day = get24Time.substring(6, 8);
        String data = year +"年"+ month + "月" + day + "日";
        return data;
    }
    public static String getYearMonthDayHourMinSecTime(String get24Time){
        String year = get24Time.substring(0, 4);
        String month = get24Time.substring(4, 6);
        String day = get24Time.substring(6, 8);
        String hour = get24Time.substring(10,12);
        String minute = get24Time.substring(12, 14);
        String second = get24Time.substring(14, 16);
        String data = year + "年" + month + "月" + day +"日 " + hour + ":" + minute + ":" +second;
        return data;
    }
    public static String getDislodge(String param){
        String[] arr1 = param.split("-"); // 不限制元素个数
        String date = "";
        for (int i = 0; i < arr1.length; i++) {
            date=date+arr1[i];
        }
        return date;
    }
    public static String getYearMonthDay(String get24Time){
        String year = get24Time.substring(0, 4);
        String month = get24Time.substring(4, 6);
        String day = get24Time.substring(6, 8);
        String data = year + "-" + month + "-" + day;
        return data;
    }

    public static String getYearMonthDayIsNum(String get24Time){
        String year = get24Time.substring(0, 4);
        String month = get24Time.substring(4, 6);
        String day = get24Time.substring(6, 8);
        String data = year + month + day;
        return data;
    }

    public static String getHourMinSec(String get24Time){
        String hour = get24Time.substring(10,12);
        String minute = get24Time.substring(12, 14);
        String second = get24Time.substring(14, 16);
        String data = hour + ":" + minute + ":" +second;
        return data;
    }

    public static String timeStampOfTime(String time){
        String res = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(Long.parseLong(time)));
        return res;
    }

    public static int getSeason(Date date) {

        int season = 0;

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int month = c.get(Calendar.MONTH);
        switch (month) {
            case Calendar.JANUARY:
            case Calendar.FEBRUARY:
            case Calendar.MARCH:
                season = 1;
                break;
            case Calendar.APRIL:
            case Calendar.MAY:
            case Calendar.JUNE:
                season = 2;
                break;
            case Calendar.JULY:
            case Calendar.AUGUST:
            case Calendar.SEPTEMBER:
                season = 3;
                break;
            case Calendar.OCTOBER:
            case Calendar.NOVEMBER:
            case Calendar.DECEMBER:
                season = 4;
                break;
            default:
                break;
        }
        return season;
    }

    public static void main(String[] args) {
        int season = TimeUtil.getSeason(new Date());
        System.out.println(season);

    }
}
