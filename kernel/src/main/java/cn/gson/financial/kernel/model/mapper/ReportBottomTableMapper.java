package cn.gson.financial.kernel.model.mapper;


import cn.gson.financial.kernel.model.entity.ReportBottomTableBean;
import cn.gson.financial.kernel.model.vo.BottomTableVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Mapper
@Repository
public interface ReportBottomTableMapper {

    int insertBottomTable(@Param("vo")ReportBottomTableBean bean);

    int updateBottomTable(@Param("bean")ReportBottomTableBean bean);

    List<ReportBottomTableBean> selectBottomTable(@Param("aid") String accountSetsId,@Param("date") String accountDate);

    int delectBottomTable(@Param("id")String id);

    List<ReportBottomTableBean> bottomView(@Param("id")Long id,@Param("sTime")String sTime,@Param("eTime")String eTime,@Param("aid") Integer accountSetsId);

    List<BottomTableVo> bottomYearView(@Param("id")Long id, @Param("sTime")String sTime, @Param("eTime")String eTime);

    List<BottomTableVo> bottomYearView1(@Param("id")Long id, @Param("sTime")String sTime, @Param("eTime")String eTime);

    List<String> selectTemple(@Param("id")String tableId);
}
