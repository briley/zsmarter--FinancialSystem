package cn.gson.financial.kernel.model.entity;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/4/17 19:45
 * @Description：
 */
@Data
public class OldAccountImportBean {
    public static final Integer EXPORTING = 1;//导入中
    public static final Integer EXPORT_SUCCESS = 2;//已成功
    public static final Integer EXPORT_FAIL = 3;//已失败
    public static final Integer EXPORTED = 4;//已完成

    private int id;//主键id
    private String softwareName;//使用的软件名
    private String fileIdName;//期初文件id和名称
    private MultipartFile openingFile;//期初文件
    private MultipartFile voucherFile;//凭证文件
    private Date createTime;//创建时间
    private String isNewAccount;//是否新账套
    private int accountSetsId;//账套id
    private String companyName;//账套名称
    private Date enableDate;//账套启动年月
    private String accountingStandards;//会计准则：0: 小企业会计准则, 1: 企业会计准则
    private String vatType;//增值税种类;0: 小规模纳税人, 1: 一般纳税人
    private int state;//1:导入中2:已成功3:已失败4:已完成
    private String mobile;

    private String voucherReviewed;//是否需要审核

}
