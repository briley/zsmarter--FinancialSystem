package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.ReportTemplateItems;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface ReportTemplateItemsMapper extends BaseMapper<ReportTemplateItems> {
    int batchInsert(@Param("list") List<ReportTemplateItems> list);
}