package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.excelexport.*;
import cn.gson.financial.kernel.model.entity.OldAccountImportBean;
import cn.gson.financial.kernel.model.entity.ReportTemplate;
import cn.gson.financial.kernel.model.entity.User;
import cn.gson.financial.kernel.model.vo.*;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author：ywh
 * @Date：2023/4/17 11:12
 * @Description：
 */
public interface ExportFileService {

    //旧账导入
    String oldAccountImport(OldAccountImportBean oldAccountImportBean);

    //总账导出
    List<AllAccountExport> downloadLedgerToExcel(Integer accSetsId,String startTime, String endTime ,String startSubject,String endSubject,Integer startLevel,Integer endLevel,boolean isOpen,boolean showNumPrice) throws ParseException;
    //总账的sheet处理
    Workbook allAccountExport(InputStream inputStream, int length,String companyName,String sheetName,String date) throws IOException;

    //明细表导出
    List<SubLedger> downloadSubsidiaryToExcel(AccountBookVo vo);
    //明细的sheet处理
    Workbook subLedgerExport(String subjectName,InputStream inputStream, int length, String companyName, String sheetName, String date) throws IOException;

    //科目余额导出
    List<AccountBalanceExport> downloadAccountBalanceToExcel(String companyName, String startTime, String endTime, String startSubject, String endSubject, int startLevel, int endLevel, int accountSetsId, Boolean showNumPrice, Boolean isOpen);
    //科目余额的sheet处理
    Workbook accountBalanceExport(InputStream inputStream, int length, String companyName, String sheetName, String date,int writeTitleIndex) throws IOException;

    //时序账导出
    List<List<Object>> downloadChronologicalAccountToExcel(Integer accsetsId, String sTime, String eTime, String sWordCode, String eWordCode, String word, String summary, String sSubjectCode, String eSubjectCode, String sMoney, String eMoney, Integer sort,String columnIndex);

    //凭证导出
    List<List<Object>>voucherExport(VoucherSelectVo vo,String columnIndex,Integer id);

    List<User> findRealName(List<String> list);

    //现金流量表的sheet处理
    Workbook cashFlowExport(InputStream inputStream, int length, String companyName, String sheetName, String format) throws IOException;
    //现金流量导出
    List<CashFlowExport> downloadCashFlowToExcel(ReportTemplate allData, Map<String, BottomTableVo> bottomTableVoMap);

    //资产负债表的sheet处理
    Workbook assetsLiabilitiesExporrt(InputStream inputStream, int length, String companyName, String sheetName, String format) throws IOException;
    //资产负债导出
    List<AssetsLiabilitiesExport> downloadAssetsLiabilitiesToExcel(ReportTemplate allData, Map<Integer, ReportDataVo> integerReportDataVoMap);

    //项目/部门利润表导出
    List<ProfitExport> downloadProjectProfitToExcel(ReportTemplate allData, List<DepartmentOrProjectVO> views);

    //利润表导出
    List<ProfitExport> downloadProfitToExcel(ReportTemplate allData,Map<Integer, ReportDataVo> integerReportDataVoMap);


    int findMaxOldAccountTaskId(int accountSetsId);

    Object findState(String mobile);
}
