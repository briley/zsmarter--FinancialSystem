package cn.gson.financial.kernel.dao;


import cn.gson.financial.kernel.model.mapper.ComprehensiveParamsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * @Author：ywh
 * @Date：2022/9/16 15:34
 * @Description：
 */
@Component
public class ComprehensiveParamsDao {

    @Autowired
    private ComprehensiveParamsMapper comprehensiveParamsMapper;

    public String queryDefaultContent(String code){
        return comprehensiveParamsMapper.queryDefaultContent(code);
    }

    public List<String> findByType(String type) {
        return comprehensiveParamsMapper.findByType(type);
    }
}
