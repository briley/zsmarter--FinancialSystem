package cn.gson.financial.kernel.model.vo;

import lombok.Data;


@Data
public class ReportDataVo {

    /**
     * 模板项 ID
     */
    private Integer itemId;

    /**
     * 季度累计金额
     */
    private Double currentYearAmount;

    /**
     * 本期金额
     */
    private Double currentPeriodAmount;
}
