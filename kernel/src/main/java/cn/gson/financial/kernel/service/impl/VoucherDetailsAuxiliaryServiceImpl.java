package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.dao.AISLDao;
import cn.gson.financial.kernel.model.entity.VoucherDetailsAuxiliary;
import cn.gson.financial.kernel.model.mapper.VoucherDetailsAuxiliaryMapper;
import cn.gson.financial.kernel.model.vo.AISLFinalVo;
import cn.gson.financial.kernel.model.vo.AISLVo;
import cn.gson.financial.kernel.service.SubjectService;
import cn.gson.financial.kernel.service.VoucherDetailsAuxiliaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class VoucherDetailsAuxiliaryServiceImpl extends ServiceImpl<VoucherDetailsAuxiliaryMapper, VoucherDetailsAuxiliary> implements VoucherDetailsAuxiliaryService {
    @Autowired
    private AISLDao aislDao;
    @Autowired
    private SubjectService subjectService;

    @Override
    public int batchInsert(List<VoucherDetailsAuxiliary> list) {
        return baseMapper.batchInsert(list);
    }

    @Override
    public List<AISLFinalVo> selectAISLByParams(Integer accSetsId,
                                                String sSubjectCode,
                                                String eSubjectCode,
                                                String sTime,
                                                String eTime,
                                                Integer categoryId,
                                                String categoryCode) {
        List<AISLVo> aislVoList = aislDao.selectAISLByParams(accSetsId, sSubjectCode, eSubjectCode,sTime,eTime,categoryId,categoryCode);
        List<AISLFinalVo> aislFinalVoList = new ArrayList<>();
        aislVoList.forEach(aislVo -> {
            AISLFinalVo aislFinalVo = getAISLFinalVo(aislVo);
            aislFinalVo.setSubjectBlance(subjectService.balance(accSetsId, aislVo.getSubjectId(), null, null));
            aislFinalVoList.add(aislFinalVo);
        });
        return aislFinalVoList;
    }

    private AISLFinalVo getAISLFinalVo(AISLVo aislVo){
        AISLFinalVo aislFinalVo = new AISLFinalVo();
        aislFinalVo.setBalanceDirection(aislVo.getBalanceDirection());
        aislFinalVo.setCreditAmount(aislVo.getCreditAmount());
        aislFinalVo.setDebitAmount(aislVo.getDebitAmount());
        aislFinalVo.setSubjectBlance(null);
        aislFinalVo.setSummary(aislVo.getSummary());
        aislFinalVo.setVoucherDate(aislVo.getVoucherDate());
//        aislFinalVo.setVoucherWord(aislVo.getVoucherWord());
        aislFinalVo.setVoucherWordCode(aislVo.getVoucherWord()+"-"+aislVo.getVoucherWordCode());
        aislFinalVo.setNum(aislVo.getNum());
        aislFinalVo.setPrice(aislVo.getPrice());
        aislFinalVo.setSubject(aislVo.getSubjectCode()+"-"+aislVo.getSubjectName());
        return aislFinalVo;
    }
}
