package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-05-08  10:34
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class AddUserRoleVo {
    private String phone;
    private String role;
    private Integer accountSetsId;
    private String code;
}
