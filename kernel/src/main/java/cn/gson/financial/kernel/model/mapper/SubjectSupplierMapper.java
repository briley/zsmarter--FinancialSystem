package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.SubjectSupplier;
import cn.gson.financial.kernel.model.vo.SubjectSupplierlVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface SubjectSupplierMapper extends BaseMapper<SubjectSupplier> {

    List<SubjectSupplierlVo> selectSubjectSupplierlVo(@Param("accountSetsId") Integer accountSetsId);
}
