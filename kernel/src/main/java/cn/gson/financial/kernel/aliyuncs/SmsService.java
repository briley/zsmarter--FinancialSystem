package cn.gson.financial.kernel.aliyuncs;

import lombok.Data;
import lombok.NonNull;

import java.util.Map;


public interface SmsService {

    String PRODUCT = "Dysmsapi";

    String DOMAIN = "dysmsapi.aliyuncs.com";

    /**
     * 发送短信
     *
     * @param smsBody
     */
    void send(SmsBody smsBody);

    @Data
    class SmsBody {
        @NonNull
        private String phoneNumbers;
        @NonNull
        private String signName;
        @NonNull
        private String templateCode;

        private Map<String, String> templateParam;
        private String outId;
        private String smsUpExtendCode;
    }

}
