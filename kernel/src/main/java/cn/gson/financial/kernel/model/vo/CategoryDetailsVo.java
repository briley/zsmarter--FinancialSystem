package cn.gson.financial.kernel.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/20 16:55
 * @Description：
 */
@Data
public class CategoryDetailsVo {

    private Integer id;
    private String code;
    private String name;
    private String acName;
    private String remark;
    private Boolean enable;
    private Integer accountingCategoryId;
    private String cusColumn0;
    private String cusColumn1;
    private String cusColumn2;
    private String cusColumn3;
    private String cusColumn4;
    private String cusColumn5;
    private String cusColumn6;
    private String cusColumn7;
    private String cusColumn8;
    private String cusColumn9;
    private String cusColumn10;
    private String cusColumn11;
    private String cusColumn12;
    private String cusColumn13;
    private String cusColumn14;
    private String cusColumn15;

}
