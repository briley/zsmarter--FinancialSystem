package cn.gson.financial.kernel.model.mapper;


import cn.gson.financial.kernel.model.entity.UserRoundupsBean;
import cn.gson.financial.kernel.model.vo.AuditVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface UserRoundupsMapper {

    List<String> selectRoundupByUserId(String userId);

    int insertRoundupByUserId(@Param("bean") UserRoundupsBean bean);

    String selectPhoneByUserId(String userId);

    String selectUserIdByphone(String phone);

    int bridingPhone(@Param("phone")String phone,@Param("userId") String userId,@Param("date")Date date);

    int updateUserPhone(@Param("phone")String phone,@Param("userId") String userId);

    int updateUserPhoneHx(@Param("phone") String phone,@Param("openId") String userId,@Param("date")Date date);

    String selectAllByRoundup(@Param("roundup")String roundup,@Param("id")String id);

    int updateRoundupNum(@Param("num")String num ,@Param("id") String id ,@Param("roundup")String roundup,@Param("date") String date);

    int updateAudit(@Param("vid") Integer vid,@Param("uid") Integer uid,@Param("name") String name,@Param("date") Date date);

    int cancelAudit(@Param("vid") Integer voucherId);

    void deleteVoucher(@Param("vid") Integer voucherId);

    void deleteVoucherD(@Param("vid") Integer voucherId);

    List<Integer> selectCheckoutVoucherId(@Param("id") Integer accountSetsId);

    Integer selectAccount(@Param("phone")String phone);

    Integer selectUserAccount(@Param("phone")String phone, @Param("aId")Integer accountSetsId);

    Integer judgingNum(@Param("role")String role, @Param("aId")Integer accountSetsId);
}
