package cn.gson.financial.kernel.service.impl;


import cn.gson.financial.kernel.dao.UserAccountLimitDao;
import cn.gson.financial.kernel.model.entity.SubjectSupplier;
import cn.gson.financial.kernel.model.entity.UserAccountLimit;
import cn.gson.financial.kernel.model.mapper.SubjectSupplierMapper;
import cn.gson.financial.kernel.model.mapper.UserAccountLimitMapper;
import cn.gson.financial.kernel.model.mapper.UserAccountSetsMapper;
import cn.gson.financial.kernel.service.UserAccountLimitService;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.additional.update.impl.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.additional.update.impl.UpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Slf4j
@Service
@AllArgsConstructor
public class UserAccountLimitServiceImpl extends ServiceImpl<UserAccountLimitMapper,UserAccountLimit> implements UserAccountLimitService{

    @Autowired
    private UserAccountLimitDao userAccountLimitDao;

    @Override
    public void saveBean(UserAccountLimit userAccountLimit) {
        this.userAccountLimitDao.save(userAccountLimit);
    }

    @Override
    public int selectUserAccountLimitNum(String userOpenId) {
        return this.userAccountLimitDao.selectAccountLimitNum(userOpenId);
    }
}
