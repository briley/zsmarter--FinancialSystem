package cn.gson.financial.kernel.excelexport;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/17 21:41
 * @Description：
 */
@Data
public class ChronologicalAccountExport {

    //时序账导出
    private String voucherData;//凭证日期
    private String voucherName;//凭证字号
    private String summary;//摘要
    private String subjectCode;//科目编码
    private String subjectName;//科目名称
    private String currencyCode;//外币代码
    private String cumulativeDebitNum;//借方数量
    private String foreignDebitAmount;//借方外币，只有勾选外币核算才有的
    private String debitAmount;//借方本币，也就是借方金额
    private String cumulativeCreditNum;//贷方数量
    private String foreignCreditAmount ;//贷方外币，只有勾选外币核算才有的
    private String creditAmount;//贷方本币，也就是贷方金额
    private String customerCode;//客户编号
    private String customerName;//客户名称
    private String supplierCode;//供应商编码
    private String supplierName;//供应商名称
    private String employeeCode;//职员编码
    private String employeeName;//职员名称
    private String departmentCode;//部门编码
    private String departmentName;//部门名称
    private String projectCode;//项目编码
    private String projectName;//项目名称
    private String createMember;//制单人
    private String auditMemberName;//审核人
    private String receiptNum;//附件数
    private String remark;//备注

}
