package cn.gson.financial.kernel.model.vo;

import lombok.Data;

import java.util.Date;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-04-12  10:49
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class SubjectSelectVo {
    private Integer accountSetsId;
    private String timeStart;
    private String timeEnd;
    private String subjectIdStart;
    private String subjectIdEnd;
    private Short levelStart;
    private Short levelEnd;
    private Integer currencyId;
}
