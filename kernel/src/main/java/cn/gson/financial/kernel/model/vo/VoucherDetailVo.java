package cn.gson.financial.kernel.model.vo;

import lombok.Data;

import java.util.Date;


@Data
public class VoucherDetailVo {

    private Date voucherDate;
    private Integer voucherId;
    private Integer detailsId;
    private String word;
    private Integer code;
    private String summary;
    private String subjectName;
    private String voucherWordCode;
    private Integer subjectId;
    private String subjectCode;
    private String detailSubjectCode;
    private Double debitAmount;
    private Double creditAmount;
    private String balanceDirection;
    private Double balance;
    private Double num;
    private Double numBalance;
    private Double price;
    private String unit;
    private String auxiliaryTitle;
    private Date minVoucherDate;
    private Double currencyDebit;
    private Double currencyCredit;
    private Double currencyBalance;
    private Double cumulativeCredit;
    private Double cumulativeDebit;

    private Double cumulativeCreditNum;
    private Double cumulativeDebitNum;

    private Double currentDebitAmountNum;
    private Double currentCreditAmountNum;//本期贷方数量;

    private Double currencyCumulativeCredit;//原币累计贷方
    private Double currencyCumulativeDebit;//原币累计借方

    private String currencyAccounting;



    public boolean isNull() {
        return this.debitAmount == null && this.creditAmount == null;
    }
}
