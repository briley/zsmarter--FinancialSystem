package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.Currency;
import cn.gson.financial.kernel.model.entity.Subject;
import cn.gson.financial.kernel.model.entity.VoucherDetails;
import cn.gson.financial.kernel.model.vo.BalanceVo;
import cn.gson.financial.kernel.model.vo.InitBlanceVo;
import cn.gson.financial.kernel.model.vo.SubjectSelectVo;
import cn.gson.financial.kernel.model.vo.SubjectVo;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.Date;
import java.util.List;


@Mapper
public interface SubjectMapper extends BaseMapper<Subject> {
    int batchInsert(@Param("list") List<Subject> list);

    List<Subject> selectNoChildrenSubject(@Param(Constants.WRAPPER) Wrapper wrapper);

    List<Subject> selectAccountBookList(@Param("accountDate") String accountDate, @Param("accountSetsId") Integer accountSetsId, @Param("showNumPrice") boolean showNumPrice);
    List<Subject> selectAccountBookSubjectSummaryList(@Param("startTime") Date startTime,@Param("endTime") Date endTime, @Param("accountSetsId") Integer accountSetsId, @Param("showNumPrice") boolean showNumPrice);

    List<Subject> selectAccountBookListBaseCurrency(@Param("startTime") Date startTime,@Param("endTime") Date endTime, @Param("accountSetsId") Integer accountSetsId, @Param("showNumPrice") boolean showNumPrice);

    List<Subject> selectAccountBookListCurrency(@Param("startTime") Date startTime,@Param("endTime") Date endTime, @Param("accountSetsId") Integer accountSetsId, @Param("showNumPrice") boolean showNumPrice, @Param("currencyId") Integer currencyId);

    //总账的 动态查询
    List<Subject> selectAccountBookListByParam(@Param("sTime")String startTime,
                                               @Param("eTime")String endTime ,
                                               @Param("sSub")String startSubject,
                                               @Param("eSub")String endSubject,
                                               @Param("sLevel")Integer startLevel,
                                               @Param("eLevel")Integer endLevel,
                                               @Param("asId")Integer accountSetsId,
                                               @Param("showNumPrice") boolean showNumPrice,
                                               @Param("currencyId")Integer currencyId
    );

    //科目余额的 科目查询
    List<Subject> selectAccountSubjectListByParam(@Param("sTime")String startTime,
                                               @Param("eTime")String endTime ,
                                               @Param("sSub")String startSubject,
                                               @Param("eSub")String endSubject,
                                               @Param("sLevel")Integer startLevel,
                                               @Param("eLevel")Integer endLevel,
                                               @Param("asId")Integer accountSetsId,
                                               @Param("showNumPrice") boolean showNumPrice,
                                               @Param("currencyId") Integer currencyId);

    List<Subject> selectAccountBookList1(@Param("accountDate1")Date date1,@Param("accountDate2")Date date2,@Param("id1")String subjectIdStart,@Param("id2")String subjectIdEnd,@Param("level1")Short levelStart, @Param("level2")Short levelEnd, @Param("accountSetsId") Integer accountSetsId, @Param("showNumPrice") boolean showNumPrice, @Param("cId") Integer id);
    List<Currency> accountBookCurrencyList(@Param("accountSetsId") Integer accountSetsId);

    List<Subject> selectAccountBookList2(@Param("accountDate1")Date date1,@Param("accountDate2")Date date2,@Param("id1")String subjectIdStart,@Param("id2")String subjectIdEnd,@Param("level1")Short levelStart, @Param("level2")Short levelEnd, @Param("accountSetsId") Integer accountSetsId, @Param("showNumPrice") boolean showNumPrice);

    List<SubjectVo> selectSubjectVo(@Param(Constants.WRAPPER) Wrapper queryWrapper);

    List<Integer> selectLeaf(@Param("accountSetsId") Integer accountSetsId);

    List<Subject> selectBalanceSubjectList(@Param("accountDate") Date accountDate, @Param("accountSetsId") Integer accountSetsId, @Param("showNumPrice") boolean showNumPrice);

    InitBlanceVo findBySubIdAndAccountSwtId(
            @Param("subId") Integer subId,
            @Param("accSetsId") Integer accountSetId,
            @Param("remberDate") String remberDate,
            @Param("subjectCode") String subjectCode);

    Integer saveInitBlanceVo(@Param("vo") InitBlanceVo vo);

    Integer updateInitBlanceVo(@Param("vo") InitBlanceVo vo);

    List<VoucherDetails> findBySubId(@Param("subjectIdList") List<Integer> subjectIdList);

    List<Subject> selectQcSubject(@Param("accountSetsId") Integer accountSetsId);

    //记录上个月的 期末余额
    int addInitBalanceVo(@Param("vo") InitBlanceVo vo);
    //查询上个月的 期末余额
    InitBlanceVo selectInitBalanceVo(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("date") String date,
            @Param("code")String code,
            @Param("cId")Integer cId);

    InitBlanceVo selectEndingBalanceVo(
            @Param("accountSetsId") Integer accountSetsId,
            @Param("date") String date,
            @Param("code")String code,
            @Param("cId") Integer cId);

    List<Subject> findByTypeCurrency(@Param("accountSetsId") Integer accountSetsId, @Param("type") String type, @Param("currencyId") Integer currencyId);

    String selectCurrencyCode(@Param("id")Integer id);

    int updateSubject(@Param("subjectId") Integer subjectId);

    List<BalanceVo> selectInitAcombined(@Param("accountSetsId") Integer accountSetsId,@Param("date") String startTime,@Param("ids") List<Integer> subjectIds);

    List<BalanceVo> selectCurrcentAcombined(@Param("accountSetsId") Integer accountSetsId,@Param("date1") String startTime,@Param("date2") String endTime, @Param("ids") List<Integer> subjectIds);

    List<BalanceVo> selectEndAcombined(@Param("accountSetsId") Integer accountSetsId,@Param("date") String startTime, @Param("ids") List<Integer> subjectIds);
}