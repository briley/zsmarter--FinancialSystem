package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.SubjectMateriel;
import cn.gson.financial.kernel.model.entity.SubjectSupplier;
import cn.gson.financial.kernel.model.vo.SubjectMaterielVo;
import cn.gson.financial.kernel.model.vo.SubjectSupplierlVo;

import java.util.List;


public interface SubjectSupplierService {

    void saveAll(List<SubjectSupplier> entity, Integer accountSetsId);

    List<SubjectSupplierlVo> supplierSubject(Integer accountSetsId);
}
