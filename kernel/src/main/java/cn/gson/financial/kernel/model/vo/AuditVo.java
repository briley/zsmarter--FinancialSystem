package cn.gson.financial.kernel.model.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-05-04  15:48
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class AuditVo {
    private List<Integer> voucherId;
    private Integer auditMemberId;
    private String auditMemberName;
    private Date auditDate;
}
