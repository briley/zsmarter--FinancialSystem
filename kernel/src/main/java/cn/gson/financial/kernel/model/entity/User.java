package cn.gson.financial.kernel.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
@TableName(value = "fxy_financial_user")
public class User implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 电话
     */
    @TableField(value = "mobile")
    private String mobile;

    /**
     * 密码
     */
    @TableField(value = "password")
    private String password;

    /**
     * 微信 unionId
     */
    @TableField(value = "union_id")
    private String unionId;

    /**
     * 微信 openId
     */
    @TableField(value = "open_id")
    private String openId;

    /**
     * 昵称
     */
    @TableField(value = "nickname")
    private String nickname;

    /**
     * 头像
     */
    @TableField(value = "avatar_url")
    private String avatarUrl;

    /**
     * 真实姓名
     */
    @TableField(value = "real_name")
    private String realName;

    @TableField(value = "account_sets_id", updateStrategy = FieldStrategy.IGNORED)
    private Integer accountSetsId;

    /*
    * 第一次扫码注册时间
    * */
    @TableField(value = "create_date")
    private Date createDate;

    /**
     * 默认密码
     */
    @TableField(value = "init_password")
    private String initPassword;

    /**
     * 邮箱
     */
    @TableField(value = "email")
    private String email;

    /**
     * 登录时间
     */
    @TableField(value = "login_time")
    private Date loginTime;

    /**
     * 绑定手机号时间
     */
    @TableField(value = "bind_phone_time")
    private Date bindPhoneTime;

    /**
     * 渠道
     */
    @TableField(value = "channel")
    private String channel;




    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_MOBILE = "mobile";

    public static final String COL_PASSWORD = "password";

    public static final String COL_UNION_ID = "union_id";

    public static final String COL_OPEN_ID = "open_id";

    public static final String COL_NICKNAME = "nickname";

    public static final String COL_AVATAR_URL = "avatar_url";

    public static final String COL_REAL_NAME = "real_name";

    public static final String COL_ACCOUNT_SETS_ID = "account_sets_id";

    public static final String COL_CREATE_DATE = "create_date";

    public static final String COL_INIT_PASSWORD = "init_password";

    public static final String COL_EMAIL = "email";
}