package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * 豪侠修改版 绑定手机号
 */
@Data
public class BangdPhoneVo {
    //当前登录人员的用户ID
    private Integer wxUserId;
    //当前用户的 微信Id
    private String openId;
    //手机验证码
    private String code;
    //手机号码
    private String phoneNum;
}
