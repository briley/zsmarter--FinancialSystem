package cn.gson.financial.kernel.service;


import cn.gson.financial.kernel.model.entity.UserAccountLimit;
import com.baomidou.mybatisplus.extension.service.IService;

public interface UserAccountLimitService extends IService<UserAccountLimit> {


    void saveBean(UserAccountLimit userAccountLimit);

    int selectUserAccountLimitNum(String userOpenId);
}
