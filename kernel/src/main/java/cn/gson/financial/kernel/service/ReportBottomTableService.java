package cn.gson.financial.kernel.service;


import cn.gson.financial.kernel.model.entity.ReportBottomTableBean;
import cn.gson.financial.kernel.model.vo.BottomTableVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

public interface ReportBottomTableService {

    String insertBottomTable(List<ReportBottomTableBean> bean);

    int delectBottomTable(String id);

    List<BottomTableVo> viewG(Integer accountSetsId, Long id, String sTime, String eTime);
}
