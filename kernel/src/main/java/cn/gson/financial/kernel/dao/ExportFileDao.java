package cn.gson.financial.kernel.dao;

import cn.gson.financial.kernel.constans.ComprehensiveParamesConstans;
import cn.gson.financial.kernel.model.entity.AccountSets;
import cn.gson.financial.kernel.model.entity.OldAccountImportBean;
import cn.gson.financial.kernel.model.mapper.ExportFileMapper;
import cn.gson.financial.kernel.model.vo.AuxiliaryVo;
import cn.gson.financial.kernel.service.ExportFileService;
import cn.gson.financial.kernel.utils.ExcelProUtil;
import cn.gson.financial.kernel.utils.JsonStringUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author：ywh
 * @Date：2023/4/24 10:52
 * @Description：
 */
@Component
public class ExportFileDao {

    @Autowired
    private ExportFileMapper exportFileMapper;
    @Autowired
    private ComprehensiveParamsDao comprehensiveParamsDao;

    public HashMap<String, Object> createTitle(List<String> auxiliaryNameList, String sheetName, String columnIndex){

        List<String> list = new ArrayList<>();
        String[] title = {"日期","凭证字号","摘要","科目名称","外币代码","借方数量","借方外币","借方本币","贷方数量","贷方外币","贷方本币","客户编码","客户名称",
                "供应商编码","供应商名称","职员编码","职员名称","部门编码","部门名称","项目编码","项目名称","制单人","审核人","附件数","备注"};
        list.addAll(Arrays.asList(title));
        list.addAll(list.size()-Integer.parseInt(columnIndex),auxiliaryNameList);
        String[] newTitle = (String[]) list.toArray();

        ExcelProUtil excelProUtil = new ExcelProUtil();
        Workbook workbook= new XSSFWorkbook();
        workbook.createSheet(sheetName);
        Workbook setTitleWorkbook = excelProUtil.createRowTitle(workbook, newTitle, sheetName);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("setTitleWorkbook",setTitleWorkbook);
        hashMap.put("newTitle",newTitle);
        return hashMap;


    }

    public HashMap<String, Object> getCategoryList(List<Integer> voucherDetailsIdList,String[] titleHead,String columnIndex ) {
        List<Integer> collect = voucherDetailsIdList.stream().distinct().collect(Collectors.toList());
        //根据id查询辅助核算
        List<AuxiliaryVo> auxiliaryVoList = exportFileMapper.findAuxiliary(collect);

        List<String> auxiliaryNameList = new ArrayList<>();
        for (AuxiliaryVo auxiliaryVo : auxiliaryVoList) {
            System.out.println(auxiliaryVo);
            List<String> titleList = Arrays.asList(auxiliaryVo.getTitleList().split(","));
            auxiliaryNameList.addAll(titleList);
        }
        //去重后作为表头插入表头中
        List<String> filterTitle = auxiliaryNameList.stream().distinct().collect(Collectors.toList());

        List<String> titleHeadList = Arrays.asList(titleHead);
        List<Object> finTitleHeadList = new ArrayList<>();
        finTitleHeadList.addAll(titleHeadList);
        finTitleHeadList.addAll(finTitleHeadList.size()-Integer.parseInt(columnIndex),filterTitle);

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("finTitleHeadList",finTitleHeadList);
        hashMap.put("auxiliaryVoList",auxiliaryVoList);
        hashMap.put("filterTitle",filterTitle);
        return hashMap;
    }

    public List<Object> setAuxiliaryVo(List<Object> list, List<AuxiliaryVo> auxiliaryVoList, List<String> filterTitle,Integer id) {
        int size = list.size();
        //加入该账套的辅助核算字段
        for (AuxiliaryVo auxiliaryVo : auxiliaryVoList) {
            if (auxiliaryVo.getVoucherDetailsId().equals(String.valueOf(id))){
                for (String title : filterTitle) {
                    String value = JsonStringUtil.JsonStringGetValueByKey(auxiliaryVo.getContent(), title);
                    list.add(value+"");
                }
            }
        }
        //没有辅助核算的就写入空串
        if (size == list.size()){
            for (String title : filterTitle) {
                list.add("");
            }
        }
        return list;
    }

    public int findMaxOldAccountTaskId(int accountSetsId) {
        return exportFileMapper.findMaxOldAccountTaskId(accountSetsId);
    }

    @Async
    @Transactional(rollbackFor = Throwable.class)
    public void exportOldAccount(int id)  {
        System.out.println("异步执行excel导入旧账中.....");
        try {
            Thread.sleep(120000);
            System.out.println("异步执行excel导入旧账完成,准备修改状态");
            exportFileMapper.updateOldAccountState(id,OldAccountImportBean.EXPORT_SUCCESS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            exportFileMapper.updateOldAccountState(id,OldAccountImportBean.EXPORT_FAIL);
        }
    }

    public Object findState(String mobile) {
        OldAccountImportBean oldAccountImportBean = exportFileMapper.findState(mobile);
        if (oldAccountImportBean == null){
            OldAccountImportBean oldAccountImportBean1 = new OldAccountImportBean();
            oldAccountImportBean1.setState(OldAccountImportBean.EXPORTED);
            return oldAccountImportBean1;
        }
        if (oldAccountImportBean.getState() != OldAccountImportBean.EXPORTING){
            exportFileMapper.updateOldAccountState(oldAccountImportBean.getId(),OldAccountImportBean.EXPORTED);
        }
        return oldAccountImportBean;
    }

    public AccountSets createExportAccountSets(OldAccountImportBean oldAccountImportBean) {
        //创建账套
        AccountSets accountSets = new AccountSets();
        accountSets.setAccountingStandards(Short.valueOf(oldAccountImportBean.getAccountingStandards()));
        accountSets.setCashierModule(Byte.valueOf("0"));
        accountSets.setCompanyName(oldAccountImportBean.getCompanyName());
        accountSets.setCreditCode("");
        accountSets.setEnableDate(oldAccountImportBean.getEnableDate());
        accountSets.setFixedAssetModule(Byte.valueOf("0"));
        accountSets.setIndustry(Short.valueOf(""));
        accountSets.setVatType(Short.valueOf(oldAccountImportBean.getVatType()));
        accountSets.setVoucherReviewed(Byte.valueOf(oldAccountImportBean.getVoucherReviewed()));
        return accountSets;

    }
}
