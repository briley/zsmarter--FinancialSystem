package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.User;
import cn.gson.financial.kernel.model.entity.UserManage;
import cn.gson.financial.kernel.model.vo.UserVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface UserMapper extends BaseMapper<User> {
    int batchInsert(@Param("list") List<User> list);

    List<UserVo> selectByAccountSetId(@Param("accountSetsId") Integer accountSetsId);

    int updateUserPhoneByOpenId(@Param("phone") String userPhone,@Param("openId") String openId);

    Integer updateAccessAccountSetsRoleToUser(
            @Param("userIdOld") Integer userIdOld,
            @Param("userIdNew")Integer userIdNew);

    List<UserManage> findUserManage(@Param("userManage") UserManage userManage);

    void updateInvoiceCheck(@Param("list") List<String> list);

    List<UserManage> findUserSource(@Param("userManage") UserManage userManage);
}