package cn.gson.financial.kernel.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @Author：ywh
 * @Date：2023/4/11 11:10
 * @Description：
 */
public interface OssService {

    String uploadFile(MultipartFile multipartFile);

    byte[] downLoadFile(String docId);

    Boolean deleteFile(String fileId);
}
