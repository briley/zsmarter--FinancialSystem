package cn.gson.financial.kernel.excelexport;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/12 18:32
 * @Description：
 */
@Data
public class VoucherExport {

    //凭证导出
    private String voucherData;//凭证日期
    private String wordCode;//凭证号word+code
    private String rowNum;//行号
    private String summary;//摘要
    private String subjectCode;//科目编码
    private String subjectName;//科目名称
//    private String currencyCode;//外币代码
    private String debitNum;//借方数量
    private String debitPrice;//借方单价
//    private String foreignDebitAmount;//借方外币
//    private String ;//借方汇率
    private String debitAmount;//借方本币
    private String creditNum;//贷方数量
    private String creditPrice;//贷方单价
//    private String foreignCreditAmount;//贷方外币
//    private String exchangeRate;//贷方汇率
    private String creditAmount;//贷方本币
    private String customerCode;//客户编号
    private String customerName;//客户名称
    private String supplierCode;//供应商编码
    private String supplierName;//供应商名称
    private String employeeCode;//职员编码
    private String employeeName;//职员名称
    private String departmentCode;//部门编码
    private String departmentName;//部门名称
    private String projectCode;//项目编码
    private String projectName;//项目名称
    private String cashFlowCode;//现金流编码
    private String cashFlowName;//现金流名称
    private String createMember;//制单人
    private String auditMemberName;//审核人
    private String receiptNum;//附单据数
    private String remark;//备注


}
