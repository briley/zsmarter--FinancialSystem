package cn.gson.financial.kernel.model.vo;

import cn.gson.financial.kernel.model.entity.FileBean;
import cn.gson.financial.kernel.model.entity.Voucher;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/5/5 19:05
 * @Description：
 */
@Data
public class VoucherFileVo {
    private Voucher voucher;
    private List<MultipartFile> fileList;
}

