package cn.gson.financial.kernel.dao;

import cn.gson.financial.kernel.model.mapper.VoucherDetailsAuxiliaryMapper;
import cn.gson.financial.kernel.model.vo.AISLVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 核算项目明细dao
 */
@Component
public class AISLDao {
    @Autowired
    private VoucherDetailsAuxiliaryMapper voucherDetailsAuxiliaryMapper;


    public //核算项目明细动态查询
    List<AISLVo> selectAISLByParams(Integer accSetsId,
                                    String sSubjectCode,
                                    String eSubjectCode,
                                    String sTime,
                                    String eTime,
                                    Integer categoryId,
                                    String categoryCode){

        return voucherDetailsAuxiliaryMapper.selectAISLByParams(accSetsId, sSubjectCode, eSubjectCode,sTime,eTime,categoryId,categoryCode);
    }
}
