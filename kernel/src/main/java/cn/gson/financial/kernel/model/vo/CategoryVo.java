package cn.gson.financial.kernel.model.vo;

import lombok.Data;

@Data
public class CategoryVo {
    private Integer categoryId;
    private Integer categoryDetailsId;
}
