package cn.gson.financial.kernel.model.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * @Author：ywh
 * @Date：2022/9/16 15:30
 * @Description：
 */
@Repository
@Mapper
public interface ComprehensiveParamsMapper {

    String queryDefaultContent(@Param("code") String code);

    List<String> findByType(@Param("type") String type);

    int updateByCode(@Param("code") String code, @Param("content") String content);
}
