package cn.gson.financial.kernel.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.*;

/**
 * 根据key 获取 json 的value
 */
public class JsonStringUtil {

    public static String JsonStringGetValueByKey(String jsonString,String key){
        JSONObject jsonObject = JSONObject.parseObject(jsonString);
        String value = jsonObject.getString(key);
        return value;
    }

    public static List<Object> JsonListGetValueByKey(String jsonString, String key){
        JSONObject parseResult = (JSONObject) JSONObject.parse(jsonString);
        List<Object> valueList = (List) parseResult.get(key);
        return valueList;
    }

    public static String ObjectToJson(Object jsonObject){
        String jsonString = JSONObject.toJSONString(jsonObject);
        return jsonString;
    }
}
