package cn.gson.financial.kernel.model.vo;

import lombok.Data;

@Data
public class DelAuxiliaryVo {
    private Integer subjectId;
    private String name;
}
