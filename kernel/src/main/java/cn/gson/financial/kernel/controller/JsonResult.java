package cn.gson.financial.kernel.controller;

import cn.gson.financial.kernel.exception.ResultCode;
import lombok.Data;


@Data
public class JsonResult {

    private boolean success = true;

    private Integer code = 200;

    private String msg = "";

    private Object data;

    public static JsonResult instance(boolean success) {
        JsonResult result = new JsonResult();
        result.setSuccess(success);
        return result;
    }

    public static JsonResult successful() {
        return new JsonResult();
    }

    public static JsonResult successful(Object data) {
        JsonResult result = new JsonResult();
        result.setData(data);
        return result;
    }

    public static JsonResult successful(String msg, Integer code) {
        JsonResult result = new JsonResult();
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    public static JsonResult failure() {
        JsonResult result = new JsonResult();
        result.setSuccess(false);
        result.setCode(-1);
        return result;
    }

    public static JsonResult failure(String msg) {
        JsonResult result = new JsonResult();
        result.setSuccess(false);
        result.setMsg(msg);
        result.setCode(-1);
        return result;
    }

    public static JsonResult failure(Object data) {
        JsonResult result = new JsonResult();
        result.setSuccess(false);
        result.setMsg("失败");
        result.setCode(-1);
        result.setData(data);
        return result;
    }

    public static JsonResult failure(ResultCode resultCode) {
        JsonResult result = new JsonResult();
        result.setSuccess(false);
        result.setMsg(resultCode.message());
        result.setCode(Integer.parseInt(resultCode.status()));
        return result;
    }

    public static JsonResult failure(String msg, Integer code) {
        JsonResult result = new JsonResult();
        result.setSuccess(false);
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    public JsonResult setMsg(String msg) {
        this.msg = msg;
        return this;
    }
}
