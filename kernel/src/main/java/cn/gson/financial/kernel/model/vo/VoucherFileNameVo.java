package cn.gson.financial.kernel.model.vo;

import cn.gson.financial.kernel.model.entity.FileBean;
import cn.gson.financial.kernel.model.entity.Voucher;
import lombok.Data;

import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/5/6 14:20
 * @Description：
 */
@Data
public class VoucherFileNameVo {
    private Voucher voucher;
    private List<FileBean> fileBeanList;
}
