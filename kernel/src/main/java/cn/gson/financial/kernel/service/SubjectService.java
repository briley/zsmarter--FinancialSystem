package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.AccountSets;
import cn.gson.financial.kernel.model.entity.Currency;
import cn.gson.financial.kernel.model.entity.Subject;
import cn.gson.financial.kernel.model.entity.VoucherDetails;
import cn.gson.financial.kernel.model.vo.*;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;


public interface SubjectService extends IService<Subject> {

    int batchInsert(List<Subject> list);

    List<SubjectVo> selectData(Wrapper<Subject> queryWrapper, boolean showAll);

    /**
     * 凭证明细的科目菜单
     *
     * @param accountDate
     * @param accountSetsId
     * @return
     */
    List<Subject> accountBookList(Date accountDate, Integer accountSetsId, boolean showNumPrice);

    List<Subject> accountBookListCurrency(Date startTime,Date endTime, Integer accountSetsId, boolean showNumPrice, Integer id);


    List<Subject> accountBookList1(SubjectSelectVo vo, Integer accountSetsId, boolean showNumPrice);

    List<Currency> accountBookCurrencyList(Integer accountSetsId);

    /**
     * 凭证明细的科目菜单 总账动态查询
     *
     * @param
     * @param accountSetsId
     * @return
     */
    List<Subject> accountBookListByParam(
            String startTime,
            String endTime ,
            String startSubject,
            String endSubject,
            Integer startLevel,
            Integer endLevel,
            Integer accountSetsId,
            boolean isOpen,
            boolean showNumPrice,
            Integer currencyName
    );



    /**
     * 科目余额
     *
     * @param accountDate
     * @param accountSetsId
     * @return
     */
    List<BalanceVo> subjectBalance(Date accountDate, Integer accountSetsId, boolean showNumPrice);


    /**
     * 科目余额  改 动态查询
     *
     * @param
     * @param accountSetsId
     * @return
     */
    List<BalanceVo> subjectBalanceByParam(String startTime,String endTime ,String startSubject, String endSubject, Integer startLevel, Integer endLevel,Integer accountSetsId,boolean showNumPrice,boolean isOpen,Integer currencyId);

    /**
     * 科目汇总
     *
     * @param accountDate
     * @param accountSetsId
     * @return
     */
    List subjectSummary(Date startTime,Date endTime, Integer accountSetsId, boolean showNumPrice, Integer currencyId);

    /**
     * vo
     *
     * @param queryWrapper
     * @return
     */
    List<SubjectVo> listVo(Wrapper<Subject> queryWrapper);

    /**
     * 检查科目是否已经被使用
     *
     * @param id
     * @return
     */
    Boolean checkUse(Integer id);

    /**
     * 科目余额
     *
     * @param accountSetsId
     * @param subjectId
     * @param categoryId
     * @param categoryDetailsId
     * @return
     */
    Double balance(Integer accountSetsId, Integer subjectId, Integer categoryId, Integer categoryDetailsId);

    /**
     * 科目余额
     *
     * @param accountSetsId
     * @param subjectId
     * @return
     */
    Double balance2(Integer accountSetsId, Integer subjectId,String auxiliaryTitle,List<CategoryVo> categoryVoList);


    HashMap<String,String> subjectSave(Subject entity);
    /**
     * 过滤出所有-没有子节点的科目
     *
     * @param accountSetsId
     * @return
     */
    List<Integer> leafList(Integer accountSetsId);

    List<Subject> balanceSubjectList(Date accountDate, Integer accountSetsId, boolean b);

    void importVoucher(List<SubjectVo> voucherList, AccountSets accountSets);

    //查询 设置-期初 余额数量
    InitBlanceVo findBySubIdAndAccountSwtId(Integer subId,Integer accountSetId);
    //保存 设置 - 期初 余额数量
    boolean saveInitBlanceVo(InitBlanceVo vo);
    //修改 设置 - 期初 余额数量
    boolean updateInitBlanceVo(InitBlanceVo vo);

    List<VoucherDetails> findBySubId(List<Integer> subjectIdList);

    List<Subject> findByTypeCurrency(Integer accountSetsId, String type, Integer currencyId);

    int updateSubject(Integer subjectId);
}





