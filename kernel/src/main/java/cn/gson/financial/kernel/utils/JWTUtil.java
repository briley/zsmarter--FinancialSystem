package cn.gson.financial.kernel.utils;

import cn.gson.financial.kernel.exception.GlobalHandleException;
import cn.gson.financial.kernel.exception.ResultCode;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;


import java.util.Calendar;
import java.util.Date;

public class JWTUtil {
    /**
     签发对象：这个用户的id
     签发时间：现在
     有效时间：30分钟
     载荷内容：暂时设计为：这个人的名字，这个人的昵称
     加密密钥：这个人的id加上一串字符串
     */
    public static String createToken(String userId,String password,String path) {
        Calendar nowTime = Calendar.getInstance();
        if("PC".equals(path)){
            nowTime.add(Calendar.MINUTE,720);
        }else {
            nowTime.add(Calendar.MINUTE,180);
        }

        Date expiresDate = nowTime.getTime();
        return JWT.create().withAudience(userId)   //签发对象
                .withIssuedAt(new Date())    //发行时间
                .withExpiresAt(expiresDate)  //有效时间
                .sign(Algorithm.HMAC256(password));   //加密
    }

    /**
     * 检验合法性，其中secret参数就应该传入的是用户的id
     * @param token
     * @throws GlobalHandleException
     */
    public static void verifyToken(String token, String secret) throws GlobalHandleException {
        DecodedJWT jwt = null;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret)).build();
            jwt = verifier.verify(token);
        } catch (Exception e) {
            //效验失败
            //这里抛出的异常是我自定义的一个异常，你也可以写成别的
            throw new GlobalHandleException(ResultCode.USER_NOT_LOGGED_IN);
        }
    }

    /**
     * 获取签发对象，签发对象就是用户的ID
     */
    public static String getAudience(String token) throws GlobalHandleException {
        String audience = null;
        try {
            audience = JWT.decode(token).getAudience().get(0);
        } catch (JWTDecodeException j) {
            //这里是token解析失败
            throw new GlobalHandleException(ResultCode.USER_NOT_LOGGED_IN);
        }
        return audience;
    }

}
