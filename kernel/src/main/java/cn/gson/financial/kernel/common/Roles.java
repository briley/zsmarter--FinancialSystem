package cn.gson.financial.kernel.common;


public enum Roles {
    Manager("账套管理员"),
    Director("主管"),
    Making("制单人"),
    Cashier("出纳"),
    View("查看");

    public String display;

    Roles(String display) {
        this.display = display;
    }
}
