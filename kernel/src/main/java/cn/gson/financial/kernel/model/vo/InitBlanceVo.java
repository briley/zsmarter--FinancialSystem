package cn.gson.financial.kernel.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class InitBlanceVo {

    //科目ID
    private Integer subjectId;
    //科目名称
    private String subjectName;
    //科目编码
    private String subjectCode;
    //借贷方向
    private String balanceDirection;
    //账套Id
    private Integer accountSetsId;
    //期初余额
    private Double beginBalance;
    //期初数量
    private Double beginBalanceNum;
    //累计借方
    private Double cumulativeDebit;
    //累计贷方
    private Double cumulativeCredit;
    //累计借方数量
    private Double cumulativeDebitNum;
    //累计贷方数量
    private Double cumulativeCreditNum;
    //期末借方余额
    private Double endingDebitBalance;
    //期末借方数量余额
    private Double endingDebitBalanceNum;
    //期末贷方余额
    private Double endingCreditBalence;
    //期末贷方数量余额
    private Double endingCreditBalenceNum;
    //父节点id
    private Integer parentId;
    //截止时间
    private String remberDate;

    //外币期初余额
    private Double currencyBeginBalance;
    //外币期初累计借方
    private Double currencyCumulativeDebit;
    //外币期初累计贷方
    private Double currencyCumulativeCredit;
    //外币期末借方余额
    private Double currencyEndingDebitBalance;
    //外币期末贷方余额
    private Double currencyEndingCreditBalance;


}
