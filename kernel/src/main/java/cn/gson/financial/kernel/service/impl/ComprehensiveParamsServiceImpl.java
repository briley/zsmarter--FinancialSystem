package cn.gson.financial.kernel.service.impl;


import cn.gson.financial.kernel.dao.ComprehensiveParamsDao;
import cn.gson.financial.kernel.service.ComprehensiveParamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author：ywh
 * @Date：2022/9/16 15:37
 * @Description：
 */
@Service
public class ComprehensiveParamsServiceImpl implements ComprehensiveParamsService {

    @Autowired
    private ComprehensiveParamsDao comprehensiveParamsDao;

    @Override
    public String queryDefaultContent(String code) {
        return comprehensiveParamsDao.queryDefaultContent(code);
    }


    @Override
    public List<String> findByType(String type) {
        return comprehensiveParamsDao.findByType(type);
    }
}
