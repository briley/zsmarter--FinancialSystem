package cn.gson.financial.kernel.model.entity;

import lombok.Data;

/**
 * 白名单实体
 */
@Data
public class WhiteListUrlTbBean {
    private String url;
}
