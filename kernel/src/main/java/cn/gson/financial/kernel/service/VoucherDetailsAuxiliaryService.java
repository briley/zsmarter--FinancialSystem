package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.VoucherDetailsAuxiliary;
import cn.gson.financial.kernel.model.vo.AISLFinalVo;
import cn.gson.financial.kernel.model.vo.AISLVo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface VoucherDetailsAuxiliaryService extends IService<VoucherDetailsAuxiliary> {


    int batchInsert(List<VoucherDetailsAuxiliary> list);

    //核算项目明细动态查询
    List<AISLFinalVo> selectAISLByParams(Integer accSetsId,
                                         String sSubjectCode,
                                         String eSubjectCode,
                                         String sTime,
                                         String eTime,
                                         Integer categoryId,
                                         String categoryCode);

}
