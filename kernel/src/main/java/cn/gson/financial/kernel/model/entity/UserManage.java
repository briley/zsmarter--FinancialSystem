package cn.gson.financial.kernel.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author：ywh
 * @Date：2023/7/24 15:28
 * @Description：
 */
@Data
public class UserManage {
    private String openId;
    private String mobile;
    private String bindPhoneTime;//绑定手机号时间
    private Integer accountSetsNum;//账套数
    private String loginTime;//登录时间
    private String createDate;//第一次扫码关注时间
    private String isRegisterInvoiceCheck;//是否注册发票查验

    private String bindPhoneStartTime;//绑定手机号开始时间
    private String bindPhoneEndTime;//绑定手机号结束时间
    private String loginStartTime;//登录开始时间
    private String loginEndTime;//登录结束时间
    private Boolean isAscOrder;//是否升序
    private String channel;//渠道来源

}
