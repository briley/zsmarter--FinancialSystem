package cn.gson.financial.kernel.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 序时账 vo
 *
 */
@Data
public class ChronologicalAccountVo {

    //明细id ywh need
    private int voucherDetailsId;
    //凭证id
    private int voucherId;
    //凭证日期
    private String voucherData;
    //凭证字号
    private String voucherWord;
    //凭证字号 编号
    private Integer voucherWordNum;
    //摘要
    private String summary;
    //科目编码
    private String subjectCode;
    //科目名称
    private String subjectName;
    //借方数量
    private Double cumulativeDebitNum;
    //借方金额
    private Double debitAmount;
    //贷方数量
    private Double cumulativeCreditNum;
    //贷方金额
    private Double creditAmount;
    //客户id
    private Integer customerId;
    //客户编号
    private String customerCode;
    //客户名称
    private String customerName;
    //辅助菜单名
    private String ffacName;
    //制单人
    private String createMember;
    //审核人
    private String auditMemberName;
    //附件数量
    private Integer receiptNum;
    //备注
    private String remark;

    private Double num;

    private Double currencyDebit;
    private Double currencyCredit;
    private Double currencyCumulativeDebit;
    private Double currencyCumulativeCredit;

    //外币id
    private Integer currencyId;
    //外币编号
    private String currencyCode;
    //外币名称
    private String currencyName;

    //ywh：需要这几个字段
    private String  auxiliaryAccounting;
    private String  currencyAccounting;
    private String exchangeRate;

}
