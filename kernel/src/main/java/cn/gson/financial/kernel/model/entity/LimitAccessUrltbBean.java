package cn.gson.financial.kernel.model.entity;

import lombok.Data;

/**
 *  接口限量访问 实体
 */
@Data
public class LimitAccessUrltbBean {
    private String idUrl;
    private String accessNum;
    private String accessTime;
}
