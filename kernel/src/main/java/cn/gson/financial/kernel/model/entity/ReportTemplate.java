package cn.gson.financial.kernel.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
@TableName(value = "fxy_financial_report_template")
public class ReportTemplate implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "name")
    private String name;

    @TableField(value = "account_sets_id")
    private Integer accountSetsId;

    @TableField(value = "template_key")
    private String templateKey;

    /**
     * 报表类型：0普通报表，1资产报表
     */
    @TableField(value = "type")
    private Integer type;

    @TableField(exist = false)
    private List<ReportTemplateItems> items;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_NAME = "name";

    public static final String COL_ACCOUNT_SETS_ID = "account_sets_id";

    public static final String COL_TEMPLATE_KEY = "template_key";

    public static final String COL_TYPE = "type";
}