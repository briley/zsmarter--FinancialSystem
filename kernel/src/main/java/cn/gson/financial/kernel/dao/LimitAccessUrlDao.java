package cn.gson.financial.kernel.dao;


import cn.gson.financial.kernel.model.entity.LimitAccessUrltbBean;
import cn.gson.financial.kernel.model.mapper.LimitAccessUrlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LimitAccessUrlDao {
    @Autowired
    private LimitAccessUrlMapper mapper;

    public LimitAccessUrltbBean findByIdUrl(String idUrl){
        return mapper.findByIdUrl(idUrl);
    }

    public void cleanLimitAccessUrlTable(){
        mapper.cleanLimitAccessUrlTable();
    }

    public void addLimitAccessUrl(LimitAccessUrltbBean bean){
        mapper.addLimitAccessUrlBean(bean);
    }

    public void updateAccessNum(String idUrl,String num){
        mapper.updateLimitAccessUrlNumByIdUrl(idUrl, num);
    }
}
