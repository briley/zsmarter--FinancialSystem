package cn.gson.financial.kernel.model.vo;

import lombok.Data;


@Data
public class UpdatePhoneVo {
    private String id;
    private String phone;
    private String code;
}
