package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.constans.AliyunConstant;
import cn.gson.financial.kernel.service.SendMesssageService;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
//import com.swust.om.aliyunSMS.AliyunConstant;
//import com.swust.om.service.SendMesssageService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class SendMesssageServiceImpl implements SendMesssageService {


    @Override
    public Boolean sendMessage(String phone,String code) {

        Map<String, Object> map = new HashMap<>();
        map.put("code", code);
        if (phone.equals("")) {
            return null;
        }
        DefaultProfile profile =
                DefaultProfile.getProfile("cn-hangzhou", "","");
//        System.out.println(AliyunConstant.REGION_ID+"++"+AliyunConstant.ACCESS_KEY_ID+"++"+ AliyunConstant.SECRET);
        IAcsClient client = new DefaultAcsClient(profile);

        SendSmsRequest request = new SendSmsRequest();
        request.setSignName(""); // 短信签名
        request.setTemplateCode(""); // 短信模板code
        request.setPhoneNumbers(phone); // 测试号的号码（其它号码需要添加测试号先）
        request.setTemplateParam(JSONObject.toJSONString(map)); // 发送短信信息内容（因为这里是验证码，格式为：{"code":"4-6位数字"}）

        try {
            SendSmsResponse response = client.getAcsResponse(request);
//            log.info(JSON.toJSONString(response));
            return response.getMessage().equals("OK"); // 短信发送成功时Message信息为OK（默认的）
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
//            log.error("ErrCode:" + e.getErrCode());
//            log.error("ErrMsg:" + e.getErrMsg());
//            log.error("RequestId:" + e.getRequestId());
        }
        return false;
    }

}
