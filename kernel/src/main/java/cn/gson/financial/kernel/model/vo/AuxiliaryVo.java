package cn.gson.financial.kernel.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/4/24 10:20
 * @Description：
 */
@Data
public class AuxiliaryVo {
    private String voucherDetailsId;//凭证明细id
    private String content;//查询辅助核算的json内容
    private String titleList;//查询辅助核算excel表头

}
