package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.ReportTemplateItemsFormula;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface ReportTemplateItemsFormulaService extends IService<ReportTemplateItemsFormula> {


    int batchInsert(List<ReportTemplateItemsFormula> list);

}
