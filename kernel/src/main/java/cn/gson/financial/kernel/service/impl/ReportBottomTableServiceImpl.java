package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.model.entity.ReportBottomTableBean;
import cn.gson.financial.kernel.model.mapper.AllSelectMapper;
import cn.gson.financial.kernel.model.mapper.ReportBottomTableMapper;
import cn.gson.financial.kernel.model.vo.BottomTableVo;
import cn.gson.financial.kernel.service.ReportBottomTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReportBottomTableServiceImpl implements ReportBottomTableService {
    @Autowired
    private ReportBottomTableMapper reportBottomTableMapper;
    @Autowired
    private AllSelectMapper allSelectMapper;

    @Override
    public List<BottomTableVo> viewG(Integer accountSetsId, Long id, String sTime, String eTime){
        String year = eTime.substring(0,4);
        String yearsTime = year +"-01";

        if (sTime.equals(eTime)){
            List<BottomTableVo> listb = reportBottomTableMapper.bottomYearView(id, sTime, eTime);
            List<BottomTableVo> listYear = reportBottomTableMapper.bottomYearView(id, yearsTime, eTime);
            for (int i = 0; i < listb.size() ; i++) {
                for (int j = 0; j <listYear.size() ; j++) {
                    if (listb.get(i).getTemplateId().equals(listYear.get(j).getTemplateId())){
                        listYear.get(j).setAmountMonth(listb.get(i).getAmountYear());
                        break;
                    }
                }
            }
            return listYear;
        }else {
            List<BottomTableVo> listb = reportBottomTableMapper.bottomYearView(id, sTime, eTime);
            List<BottomTableVo> listYear = reportBottomTableMapper.bottomYearView(id, yearsTime, eTime);
            for (int i = 0; i < listb.size() ; i++) {
                for (int j = 0; j <listYear.size() ; j++) {
                    if (listb.get(i).getTemplateId().equals(listYear.get(j).getTemplateId())){
                        listYear.get(j).setAmountMonth(listb.get(i).getAmountYear());
                        break;
                    }
                }
            }
            return listYear;
        }
    }

    @Override
    public String insertBottomTable(List<ReportBottomTableBean> list) {
        List<ReportBottomTableBean> listSelect =  reportBottomTableMapper.selectBottomTable(list.get(0).getAccountSetsId(),list.get(0).getCreateDate());
        if (listSelect.size()==0){
//            Integer xlbId = allSelectMapper.selectLirunReport(Integer.parseInt(list.get(0).getAccountSetsId()),"现金流量表");
        List<String> tempList = reportBottomTableMapper.selectTemple(list.get(0).getTableId());
//        List<ReportBottomTableBean> finalList = new ArrayList<>();
            for (int i = 0; i <tempList.size() ; i++) {
                ReportBottomTableBean bean = new ReportBottomTableBean();
                bean.setTableId(list.get(0).getTableId());
                bean.setTemplateId(tempList.get(i));
                bean.setCreateDate(list.get(0).getCreateDate());
                bean.setAccountSetsId(list.get(0).getAccountSetsId());
                bean.setNote("");
                bean.setAmount("");
                for (int j = 0; j <list.size() ; j++) {
                    if (tempList.get(i).equals(list.get(j).getTemplateId())){
                        if (list.get(j).getAmount()!=null&&!list.get(j).getAmount().equals("")){
                            bean.setAmount(list.get(j).getAmount());
                        }
                        if (list.get(j).getNote()!=null&&!list.get(j).getNote().equals("")){
                            bean.setNote(list.get(j).getNote());
                        }

                    }
                }
//                finalList.add(bean);
                reportBottomTableMapper.insertBottomTable(bean);
            }
        }else {
            for (int i = 0; i < list.size(); i++) {
                reportBottomTableMapper.updateBottomTable(list.get(i));
            }
        }
        return "修改成功";
    }


    @Override
    public int delectBottomTable(String id) {
        return reportBottomTableMapper.delectBottomTable(id);
    }
}
