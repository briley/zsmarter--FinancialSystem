package cn.gson.financial.kernel.dao;

import cn.gson.financial.kernel.model.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDao {
    @Autowired
    private UserMapper mapper;

   public boolean updateUserPhoneByOpenId(String phone , String openId){
       int x = mapper.updateUserPhoneByOpenId(phone, openId);
       if(x > 0){
           return true;
       }
       return false;
    }

    public Integer updateAccessAccountSetsRoleToUser(
            Integer userIdOld,
            Integer userIdNew){
       return mapper.updateAccessAccountSetsRoleToUser(userIdOld, userIdNew);
    }
}
