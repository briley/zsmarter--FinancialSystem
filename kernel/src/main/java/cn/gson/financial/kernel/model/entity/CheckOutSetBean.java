package cn.gson.financial.kernel.model.entity;

import lombok.Data;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.entity
 * @Author: 龙泽霖
 * @CreateTime: 2023-07-07  15:52
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class CheckOutSetBean {

    private Integer accountSetsId;
    private Double taxRateOne;
    private Double taxRateTwo;
    private Double taxRateThree;
    private Double taxRateFour;
    private Integer year;
    private Integer month;

}
