package cn.gson.financial.kernel.model.vo;

import cn.gson.financial.kernel.common.DoubleValueUtil;
import lombok.Data;


@Data
public class BalanceVo {
    /**
     * 科目 id
     */
    private Integer subjectId;

    /**
     * 辅助项目 id
     */
    private Integer auxiliaryId;
    /**
     * 父 id
     */
    private Integer parentId;
    /**
     * 科目名称
     */
    private String name;
    /**
     * 科目编码
     */
    private String code;

    private String unit;
    /**
     * 余额方向
     */
    private String balanceDirection;
    /**
     * 层级
     */
    private Short level;
    /**
     * 期初借方余额
     */
    private Double beginningDebitBalance;

    /**
     * 期初借方发数量
     */
    private Double beginningDebitAmountNum;

    /**
     * 期初贷方发生数量
     */
    private Double beginningCreditAmountNum;

    /**
     * 期初贷方余额
     */
    private Double beginningCreditBalance;
    /**
     * 本期借方发生额
     */
    private Double currentDebitAmount;

    /**
     * 本期借方发数量
     */
    private Double currentDebitAmountNum;
    /**
     * 本期贷方发生额
     */
    private Double currentCreditAmount;
    /**
     * 本期贷方发生数量
     */
    private Double currentCreditAmountNum;
    /**
     * 期末借方余额
     */
    private Double endingDebitBalance;
    /**
     * 期末借方数量余额
     */
    private Double endingDebitBalanceNum;
    /**
     * 期末贷方余额
     */
    private Double endingCreditBalance;
    /**
     * 期末贷方数量余额
     */
    private Double endingCreditBalanceNum;
    /**
     * 本期原币借方发生额
     */
    private Double currencyDebitAmount;
    /**
     * 本期原币贷方发生额
     */
    private Double currencyCreditAmount;
    /**
     * 币别code
     */
    private String currencyCode;




    private String currencyAccounting;
    //外币期初余额
    private Double currencyBeginningBalance;
    //外币期初借方余额
    private Double currencyBeginningDebitBalance;
    //外币期初贷方余额
    private Double currencyBeginningCreditBalance;
    //外币本期贷方发生额
    private Double currencyCurrentCredit;
    //外币本期借方发生额
    private Double currencyCurrentDebit;
    // 外币期末借方余额
    private Double currencyEndingDebitBalance;
    // 外币期末贷方余额
    private Double currencyEndingCreditBalance;

    private Integer isAuxiliary;//是否辅助核算；0不是；1是


    public void setBeginningDebitBalance(Double beginningDebitBalance) {
        if (beginningDebitBalance == null) return;

        if (this.beginningDebitBalance == null) {
            this.beginningDebitBalance = beginningDebitBalance;
        } else {
            this.beginningDebitBalance += beginningDebitBalance;
        }
    }

    public void setCurrencyBeginningDebitBalance(Double currencyBeginningBalance) {
        if (currencyBeginningBalance == null) return;

        if (this.currencyBeginningDebitBalance == null) {
            this.currencyBeginningDebitBalance = currencyBeginningBalance;
        } else {
            this.currencyBeginningDebitBalance += currencyBeginningBalance;
        }
    }

    public void setBeginningCreditBalance(Double beginningCreditBalance) {
        if (beginningCreditBalance == null) return;

        if (this.beginningCreditBalance == null) {
            this.beginningCreditBalance = beginningCreditBalance;
        } else {
            this.beginningCreditBalance += beginningCreditBalance;
        }
    }

    public void setCurrentDebitAmount(Double currentDebitAmount) {
        if (currentDebitAmount == null) return;

        if (this.currentDebitAmount == null) {
            this.currentDebitAmount = currentDebitAmount;
        } else {
            this.currentDebitAmount += currentDebitAmount;
        }
    }

    public void setCurrencyCurrentDebit(Double currencyCurrentDebit) {
        if (currencyCurrentDebit == null) return;

        if (this.currencyCurrentDebit == null) {
            this.currencyCurrentDebit = currencyCurrentDebit;
        } else {
            this.currencyCurrentDebit += currencyCurrentDebit;
        }
    }

    public void setCurrencyDebitAmount(Double currencyDebitAmount) {
        if (currencyDebitAmount == null) return;

        if (this.currencyDebitAmount == null) {
            this.currencyDebitAmount = currencyDebitAmount;
        } else {
            this.currencyDebitAmount += currencyDebitAmount;
        }
    }

    public void setCurrencyCreditAmount(Double currencyCreditAmount) {
        if (currencyCreditAmount == null) return;

        if (this.currencyCreditAmount == null) {
            this.currencyCreditAmount = currencyCreditAmount;
        } else {
            this.currencyCreditAmount += currencyCreditAmount;
        }
    }

    public void setCurrentDebitAmountNum(Double currentDebitAmountNum) {
        if (currentDebitAmountNum == null) return;

        if (this.currentDebitAmountNum == null) {
            this.currentDebitAmountNum = currentDebitAmountNum;
        } else {
            this.currentDebitAmountNum += currentDebitAmountNum;
        }
    }

    public void setCurrentCreditAmount(Double currentCreditAmount) {
        if (currentCreditAmount == null) return;

        if (this.currentCreditAmount == null) {
            this.currentCreditAmount = currentCreditAmount;
        } else {
            this.currentCreditAmount += currentCreditAmount;
        }
    }

    public void setCurrencyCurrentCredit(Double currencyCurrentCredit) {
        if (currencyCurrentCredit == null) return;

        if (this.currencyCurrentCredit == null) {
            this.currencyCurrentCredit = currencyCurrentCredit;
        } else {
            this.currencyCurrentCredit += currencyCurrentCredit;
        }
    }

    public void setBeginningDebitAmountNum(Double beginningDebitAmountNum) {
        if (beginningDebitAmountNum == null) return;

        if (this.beginningDebitAmountNum == null) {
            this.beginningDebitAmountNum = beginningDebitAmountNum;
        } else {
            this.beginningDebitAmountNum += beginningDebitAmountNum;
        }
    }

    public void setBeginningCreditAmountNum(Double beginningCreditAmountNum) {
        if (beginningCreditAmountNum == null) return;

        if (this.beginningCreditAmountNum == null) {
            this.beginningCreditAmountNum = beginningCreditAmountNum;
        } else {
            this.beginningCreditAmountNum += beginningCreditAmountNum;
        }
    }


    public void setCurrentCreditAmountNum(Double currentCreditAmountNum) {
        if (currentCreditAmountNum == null) return;

        if (this.currentCreditAmountNum == null) {
            this.currentCreditAmountNum = currentCreditAmountNum;
        } else {
            this.currentCreditAmountNum += currentCreditAmountNum;
        }
    }

    public void setEndingDebitBalance(Double endingDebitBalance) {
        if (endingDebitBalance == null) return;

        if (this.endingDebitBalance == null) {
            this.endingDebitBalance = endingDebitBalance;
        } else {
            this.endingDebitBalance += endingDebitBalance;
        }
    }

    public void setCurrencyEndingDebitBalance(Double endingDebitBalance) {
        if (endingDebitBalance == null) return;

        if (this.currencyEndingDebitBalance == null) {
            this.currencyEndingDebitBalance = endingDebitBalance;
        } else {
            this.currencyEndingDebitBalance += endingDebitBalance;
        }
    }

    public void setEndingDebitBalance(Double endingDebitBalance,Integer y) {
        this.endingDebitBalance = endingDebitBalance;
    }

    public void setCurrencyEndingDebitBalance(Double currencyEndingDebitBalance,Integer y) {
        this.currencyEndingDebitBalance = currencyEndingDebitBalance;
    }

    public void setEndingDebitBalanceNum(Double endingDebitBalanceNum,Integer y) {
        this.endingDebitBalanceNum = endingDebitBalanceNum;
    }

    public void setEndingDebitBalanceNum(Double endingDebitBalanceNum) {
        if (endingDebitBalanceNum == null) return;

        if (this.endingDebitBalanceNum == null) {
            this.endingDebitBalanceNum = endingDebitBalanceNum;
        } else {
            this.endingDebitBalanceNum += endingDebitBalanceNum;
        }
    }

    public void setEndingCreditBalance(Double endingCreditBalance,Integer y) {
            this.endingCreditBalance = endingCreditBalance;
    }

    public void setEndingCreditBalanceNum(Double endingCreditBalanceNum,Integer y) {
        this.endingCreditBalanceNum = endingCreditBalanceNum;
    }

    public void setEndingCreditBalance(Double endingCreditBalance) {
        if (endingCreditBalance == null) return;

        if (this.endingCreditBalance == null) {
            this.endingCreditBalance = endingCreditBalance;
        } else {
            this.endingCreditBalance += endingCreditBalance;
        }
    }

    public void setCurrencyEndingCreditBalance(Double endingCreditBalance) {
        if (endingCreditBalance == null) return;

        if (this.currencyEndingCreditBalance == null) {
            this.currencyEndingCreditBalance = endingCreditBalance;
        } else {
            this.currencyEndingCreditBalance += endingCreditBalance;
        }
    }



    public void setEndingCreditBalanceNum(Double endingCreditBalanceNum) {
        if (endingCreditBalanceNum == null) return;

        if (this.endingCreditBalanceNum == null) {
            this.endingCreditBalanceNum = endingCreditBalanceNum;
        } else {
            this.endingCreditBalanceNum += endingCreditBalanceNum;
        }
    }

    public void setBeginningBalance(Double beginBalance) {
        switch (this.balanceDirection) {
            case "借":
                this.setBeginningDebitBalance(beginBalance);
                break;
            case "贷":
                this.setBeginningCreditBalance(beginBalance);
                break;
        }
    }

    public void setCurrencyBeginningBalance(Double currencyBeginningBalance) {
        switch (this.balanceDirection) {
            case "借":
                this.setCurrencyBeginningDebitBalance(currencyBeginningBalance);
                break;
            case "贷":
                this.setCurrencyBeginningCreditBalance(currencyBeginningBalance);
                break;
        }
    }

    public void setCurrencyBeginningCreditBalance(Double currencyBeginningBalance) {
        if (currencyBeginningBalance == null) return;

        if (this.currencyBeginningCreditBalance == null) {
            this.currencyBeginningCreditBalance = currencyBeginningBalance;
        } else {
            this.currencyBeginningCreditBalance += currencyBeginningBalance;
        }
    }

    public void setBeginningActiveBalance(double beginBalance) {
        if (beginBalance > 0) {
            switch (this.balanceDirection) {
                case "借":
                    this.setBeginningDebitBalance(beginBalance);
                    break;
                case "贷":
                    this.setBeginningCreditBalance(beginBalance);
                    break;
            }
        } else if (beginBalance < 0) {
            switch (this.balanceDirection) {
                case "借":
                    this.setBeginningCreditBalance(Math.abs(beginBalance));
                    break;
                case "贷":
                    this.setBeginningDebitBalance(Math.abs(beginBalance));
                    break;
            }
        }
    }

    public void setEndingActiveBalance(double endBalance) {
        if (endBalance > 0) {
            switch (this.balanceDirection) {
                case "借":
                    this.setEndingDebitBalance(endBalance);
                    break;
                case "贷":
                    this.setEndingCreditBalance(endBalance);
                    break;
            }
        } else if (endBalance < 0) {
            switch (this.balanceDirection) {
                case "借":
                    this.setEndingDebitBalance(endBalance);
                    break;
                case "贷":
                    this.setEndingCreditBalance(endBalance);
                    break;
            }
        }
    }

    //ywh改 核算项目余额合计需要用
    public void setEndingActiveBalance1(double endBalance) {
        if (endBalance > 0) {
            switch (this.balanceDirection) {
                case "借":
                    this.setEndingDebitBalance(endBalance);
                    break;
                case "贷":
                    this.setEndingCreditBalance(endBalance);
                    break;
            }
        } else if (endBalance < 0) {
            switch (this.balanceDirection) {
                case "借":
//                    this.setEndingDebitBalance(endBalance);
                    this.setBalanceDirection("贷");
                    this.setEndingCreditBalance(-endBalance);
                    break;
                case "贷":
//                    this.setEndingCreditBalance(endBalance);
                    this.setBalanceDirection("借");
                    this.setEndingDebitBalance(-endBalance);
                    break;
            }
        }
    }

    public void setCurrencyEndingActiveBalance(double endBalance) {
        if (endBalance > 0) {
            switch (this.balanceDirection) {
                case "借":
                    this.setCurrencyEndingDebitBalance(endBalance);
                    break;
                case "贷":
                    this.setCurrencyEndingCreditBalance(endBalance);
                    break;
            }
        } else if (endBalance < 0) {
            switch (this.balanceDirection) {
                case "借":
                    this.setCurrencyEndingDebitBalance(endBalance);
                    break;
                case "贷":
                    this.setCurrencyEndingCreditBalance(endBalance);
                    break;
            }
        }
    }

    public Double getBeginningBalance() {
        if (this.balanceDirection == null || (this.beginningCreditBalance == null && this.beginningDebitBalance == null)) {
            return null;
        }
        double val;
        if ("借".equals(this.balanceDirection)) {
            if (this.beginningDebitBalance != null) {
                val = this.beginningDebitBalance;
            } else {
                val = 0 - this.beginningCreditBalance;
            }
        } else {
            if (this.beginningDebitBalance != null) {
                val = 0 - this.beginningDebitBalance;
            } else {
                val = this.beginningCreditBalance;
            }
        }
        return val;
    }

    public Double getCurrencyBeginningBalance() {
        if (this.balanceDirection == null || (this.currencyBeginningCreditBalance == null && this.currencyBeginningDebitBalance == null)) {
            return null;
        }
        double val;
        if ("借".equals(this.balanceDirection)) {
            if (this.currencyBeginningDebitBalance != null) {
                val = this.currencyBeginningDebitBalance;
            } else {
                val = 0 - this.currencyBeginningCreditBalance;
            }
        } else {
            if (this.currencyBeginningDebitBalance != null) {
                val = 0 - this.currencyBeginningDebitBalance;
            } else {
                val = this.currencyBeginningCreditBalance;
            }
        }
        return val;
    }

    public Double getEndingBalance() {
        return DoubleValueUtil.getNotNullVal(this.endingDebitBalance, this.endingCreditBalance);
    }

    public Double getCurrencyEndingBalance() {
        return DoubleValueUtil.getNotNullVal(this.currencyEndingDebitBalance, this.currencyEndingCreditBalance);
    }
}
