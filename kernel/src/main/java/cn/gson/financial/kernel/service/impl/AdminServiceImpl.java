package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.controller.JsonResult;
import cn.gson.financial.kernel.exception.ServiceException;
import cn.gson.financial.kernel.model.entity.UserManage;
import cn.gson.financial.kernel.model.mapper.UserMapper;
import cn.gson.financial.kernel.service.AdminService;
import cn.gson.financial.kernel.utils.AESUtil;
import cn.gson.financial.kernel.utils.HttpClientUtils;
import com.alibaba.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/7/24 15:16
 * @Description：
 */
@Slf4j
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<UserManage> findUserManage(UserManage userManage) {
        //查询数据并返回
        List<UserManage> userManageList = userMapper.findUserManage(userManage);
        return userManageList;
    }

    @Override
    public List<UserManage> findUserSource(UserManage userManage) {
        return userMapper.findUserSource(userManage);
    }

    @Override
    public void updateInvoiceCheck() {
        String key ="";
        String key2 ="";
        try {
            String result = HttpClientUtils.httpGet("");
            List<String> list = new ArrayList<>();
            Object[] jsonArray = JSONArray.parseArray(result).toArray();
            for (Object s : jsonArray) {
                list.add(AESUtil.decrString(String.valueOf(s), key, key2));//解密
            }
            userMapper.updateInvoiceCheck(list);
        }catch (ServiceException se) {
            log.info("调用失败！", se);
        }
    }

    @Override
    public List<List<Object>> findUserManageDownLoad(String[] title, List<UserManage> userManageList) {
        List<List<Object>> obj = setTitleExcel(title);
        for (UserManage userManage : userManageList) {
            List<Object> list = new ArrayList<>();
            list.add(userManage.getOpenId());
            list.add(userManage.getMobile());
            list.add(userManage.getCreateDate());
            list.add(userManage.getBindPhoneTime());
            list.add(userManage.getAccountSetsNum());
            list.add(userManage.getLoginTime());
            list.add(userManage.getIsRegisterInvoiceCheck());
            obj.add(list);
        }
        return obj;
    }

    @Override
    public List<List<Object>> findUserSourceDownLoad(String[] title, List<UserManage> userManageList) {
        List<List<Object>> obj = setTitleExcel(title);
        for (UserManage userManage : userManageList) {
            List<Object> list = new ArrayList<>();
            list.add(userManage.getOpenId());
            list.add(userManage.getCreateDate());
            list.add(userManage.getChannel());
            obj.add(list);
        }
        return obj;
    }

    private List<List<Object>> setTitleExcel(String[] title){
        List<Object> list = new ArrayList<>();
        List<List<Object>> obj = new ArrayList<>();
        for (String s : title) {
            list.add(s);
        }
        obj.add(list);
        return obj;
    }
}
