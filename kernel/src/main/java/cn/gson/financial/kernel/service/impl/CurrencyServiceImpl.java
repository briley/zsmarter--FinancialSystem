package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.exception.ServiceException;
import cn.gson.financial.kernel.model.vo.CurrencyVo;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import cn.gson.financial.kernel.model.mapper.CurrencyMapper;
import cn.gson.financial.kernel.model.entity.Currency;
import cn.gson.financial.kernel.service.CurrencyService;

/**
 * 外币
 */
@Service
public class CurrencyServiceImpl extends ServiceImpl<CurrencyMapper, Currency> implements CurrencyService {

    @Autowired
    private CurrencyMapper currencyMapper;

    @Override
    public int batchInsert(List<Currency> list) {
        return baseMapper.batchInsert(list);
    }

    @Override
    public int saveCurrency(Currency entity) {
        if (currencyMapper.selectByName(entity)>0 || currencyMapper.selectByCode(entity)>0){
            return 1;
        }else {
            currencyMapper.insert(entity);
            return 0;
        }
    }

    @Override
    public int updateCurrency(Currency entity) {
        if (currencyMapper.selectByName(entity)>1 || currencyMapper.selectByCode(entity)>1){
            return 1;
        }else {
            currencyMapper.updateCurrency(entity);
            return 0;
        }
    }

    @Override
    public int deleteCurrency(Integer id) {
        if (currencyMapper.selectSubjectById(id)>0){
            return 1;
        }else {
            currencyMapper.deleteCurrency(id);
            return 0;
        }
    }

    @Override
    public List<CurrencyVo> currencyList(Integer accountSetsId) {
        List<Currency> list = currencyMapper.currencyList(accountSetsId);
        List<CurrencyVo> currencyVosList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            CurrencyVo vo = new CurrencyVo();
            vo.setAccountSetsId(list.get(i).getAccountSetsId());
            vo.setCode(list.get(i).getCode());
            vo.setExchangeRate(list.get(i).getExchangeRate());
            vo.setId(list.get(i).getId());
            vo.setLocalCurrency(list.get(i).getLocalCurrency());
            vo.setName(list.get(i).getName());
            if (currencyMapper.selectSubjectById(list.get(i).getId())>0){
                vo.setFlag(false);
            }else {
                vo.setFlag(true);
            }
            currencyVosList.add(vo);
        }
        return currencyVosList;
    }

    @Override
    public List<Currency> initCurrencyList(Integer accountSetsId, String type) {
        return currencyMapper.initCurrencyList(accountSetsId,type);
    }

    @Override
    public boolean save(Currency entity) {
        LambdaQueryWrapper<Currency> qw = Wrappers.lambdaQuery();
        qw.eq(Currency::getAccountSetsId, entity.getAccountSetsId());
        qw.eq(Currency::getCode, entity.getCode());
        if (this.count(qw) > 0) {
            throw new ServiceException("币别编码已经存在！");
        }
        return super.save(entity);
    }

    @Override
    public boolean remove(Wrapper<Currency> wrapper) {
        Currency currency = this.getOne(wrapper);
        if (currency.getLocalCurrency()) {
            throw new ServiceException("本位币不能删除！");
        }
        return super.remove(wrapper);
    }
}


