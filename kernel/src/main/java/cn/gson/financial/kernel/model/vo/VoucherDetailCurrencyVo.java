package cn.gson.financial.kernel.model.vo;

import lombok.Data;

import java.util.Date;


@Data
public class VoucherDetailCurrencyVo {

    private Date voucherDate;
    private Integer voucherId;
    private Integer detailsId;
    private String word;
    private Integer code;
    private String summary;
    private String subjectName;
    private Integer subjectId;
    private String subjectCode;
    private Double debitAmount;
    private Double creditAmount;
    private String balanceDirection;
    private Double balance;
    private Double num;
    private Double numBalance;
    private Double price;
    private String unit;
    private Double originalAmount;
    private Integer currencyId;
    private String currencyName;
    private String currencyCode;
    private Double exchangeRate;
    private Double currencyDebit;
    private Double currencyCredit;

    private Double cumulativeCreditNum;
    private Double cumulativeDebitNum;
    public boolean isNull() {
        return this.debitAmount == null && this.creditAmount == null;
    }
}
