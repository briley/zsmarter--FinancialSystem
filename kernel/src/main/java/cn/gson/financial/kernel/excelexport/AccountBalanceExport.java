package cn.gson.financial.kernel.excelexport;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/17 15:50
 * @Description：
 */
@Data
public class AccountBalanceExport {

    //科目余额导出
    private String subjectCode;//科目编码
    private String subjectName;//科目名称


    private String beginningDebitBalance;//期初余额借方
    private String beginningCreditBalance;//期初余额贷方

    private String currentDebitAmount;//本期发生额借方
    private String currentDebitAmountNum;//本期发生额贷方

    private String endingDebitBalance;//期末借方余额
    private String endingCreditBalance;//期末贷方余额
}
