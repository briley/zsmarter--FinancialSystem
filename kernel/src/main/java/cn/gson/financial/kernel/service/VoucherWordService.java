package cn.gson.financial.kernel.service;

import java.util.List;
import cn.gson.financial.kernel.model.entity.VoucherWord;
import com.baomidou.mybatisplus.extension.service.IService;


public interface VoucherWordService extends IService<VoucherWord> {


    int batchInsert(List<VoucherWord> list);

}

