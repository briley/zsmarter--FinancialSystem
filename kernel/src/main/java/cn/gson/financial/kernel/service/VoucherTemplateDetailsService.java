package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.VoucherTemplateDetails;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


public interface VoucherTemplateDetailsService extends IService<VoucherTemplateDetails> {


    int batchInsert(List<VoucherTemplateDetails> list);

}



