package cn.gson.financial.kernel.model.vo;

import lombok.Data;

@Data
public class UpdateUserPhoneVo {
    private String phone;
    private String openId;
}
