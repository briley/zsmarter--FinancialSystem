package cn.gson.financial.kernel.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-04-04  09:48
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class VoucherSelectVo {
    private String timeStart;
    private String timeEnd;
    private String voucherWord;
    private String voucherNo;
    private String voucherState;
    private String voucherSummary;
    private String voucherAccount;
    private String moneyStart;
    private String moneyEnd;
    private List<String> voucherIdList;
}
