package cn.gson.financial.kernel.exception;

public enum ResultCode {
    SUCCESS("1","成功"),
    // 参数错误1001-1999
    PARAM_IS_INVALID("1001","参数无效"),
    PARAM_IS_BLANK("1002","参数为空"),
    PARAM_TYPE_BAND_ERROR("1003","参数类型错误"),
    PARAM_NOT_COMPLETE("1004","参数缺失"),
    // 用户错误2001-2999
    USER_NOT_LOGGED_IN("2001","用户未登录"),
    USER_LOGIN_ERROR("2002","账户不存在或密码错误"),
    USER_ACCOUNT_FORBIDDEN("2003","账户已被禁用"),
    USER_NOT_EXIST("2004","用户不存在"),
    USER_HAS_EXISTED("2005","用户已存在"),
    USER_PASS_ERROR("2006","密码错误"),
    USER_AUTHENTICATION_ERROR("2007","认证失败"),
    USER_AUTHORIZATION_ERROR("2008","没有权限"),
    //USER_ILLEGALACCESS_ERROR("2009","非法访问"),
    USER_ILLEGALACCESS_ERROR("2009","网络已切换，请重新扫码登录"),
    USER_LIMITACCESS_ERROR("2010","接口访问次数上限"),
    USER_REPEATLOGIN_ERROR("2011","您的账号在异地登录，请退出后重新登录"),
    USER_SUBMIT_ERROR("2012","当日提交次数已上限，请于明天再次提交"),
    VISITOR_REGISTER_ERROR("2013","当日该IP游客注册已上限，请于明天再次注册"),
    INVOICE_IS_CHECK_OR_QUEUE("2014","该任务中的票据已经处于查验或者排队中,不用再次点击批量查验！"),
    CMS_DOCID_IS_NULL("2015","cms生成的docId为空,请联系客服进行修复！"),
    USER_SMS_CODE_ERROR("2016","验证码错误!"),
    USER_SESSION_EXPIRE("2017","会话过期或者服务重启，请重新登录！"),
    USER_LOGIN_ELSE_WHERE("2018","该账号已经在别处登录！"),
    // 服务器错误3001-3999
    SERVER_OPTIMISTIC_LOCK_ERROR("3001","操作冲突"),
    SERVER_INNER_ERROR("3002","服务器内部错误"),
    SERVER_UNKNOW_ERROR("3003","服务器未知错误"),
    SERVER_EMPTY_RESULT_DATA_ACCESS_ERROR("3004","没有找到对应的数据"),
    SERVER_INVOICE_NOT_EXISTENT("3005","此发票数据已经存在"),
    SERVER_CMS_EXCEPTION("3006","上传的文件中包含空文件或者CMS服务器异常,请联系客服"),
    FILE_BYTE_SIZE_IS_EMPTY("3007","上传的文件中包含空文件");

    private String status;
    private String message;

    ResultCode(String status, String message) {
        this.status = status;
        this.message = message;
    }
    public String status(){
        return this.status;
    }
    public String message(){
        return this.message;
    }

}

