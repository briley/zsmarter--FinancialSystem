package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.AccountingCategoryDetails;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface AccountingCategoryDetailsService extends IService<AccountingCategoryDetails> {


    int batchInsert(List<AccountingCategoryDetails> list);

    /**
     * 清空辅助数据
     *
     * @param accountingCategoryId 类别 id
     * @param accountSetsId        账套 id
     */
    boolean clear(Integer accountingCategoryId, Integer accountSetsId);

    /**
     * @param name
     * @param accountSetsId
     * @param value
     * @return
     */
    List<AccountingCategoryDetails> getByCodeSet(String name, Integer accountSetsId, Set<String> value);

    /**
     * 导入数据
     *
     * @param categoryId
     * @param detailsMap
     * @return
     */
    Map<String, Integer> importData(Integer categoryId, Map<String, AccountingCategoryDetails> detailsMap);

    Integer findData(Integer categoryId,Integer categoryDetailsId);
}

