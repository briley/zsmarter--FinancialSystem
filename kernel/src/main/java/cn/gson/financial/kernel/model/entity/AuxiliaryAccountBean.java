package cn.gson.financial.kernel.model.entity;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/23 17:38
 * @Description：
 */
@Data
public class AuxiliaryAccountBean {
    private String id;
    private String name;
    private String code;
}
