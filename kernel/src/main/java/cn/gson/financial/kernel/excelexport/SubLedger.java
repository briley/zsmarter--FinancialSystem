package cn.gson.financial.kernel.excelexport;

import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/17 11:55
 * @Description：
 */
@Data
public class SubLedger {
    //明细账导出
    private String voucherDate;//日期
    private String wordCode;//	凭证字号
    private String subjectCode;//科目编码
    private String subjectName;//科目名称
    private String summary;//	摘要
    private String debitAmount;//借方
    private String creditAmount;//贷方
    private String balanceDirection;//方向:借/贷
    private String balance;//余额
}
