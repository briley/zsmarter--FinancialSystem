package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.UserRoundupsBean;
import cn.gson.financial.kernel.model.vo.AccountRoleVo;
import cn.gson.financial.kernel.model.vo.AuditVo;
import cn.gson.financial.kernel.model.vo.VoucherIdVo;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface UserRoundupsService {

    List<String> selectRoundupByUserId(String userId);

    int insertRoundupByUserId(UserRoundupsBean bean);

    String addUserRole(String phone,String role,Integer accountSetsId,String code);

    String bridingPhone(String phone, String userId, String code);

    String updateUserPhone(String phone, String userId, String code);

    String updateUserPhoneHx(Integer wxUserId, String userId, String phone, String code, HttpSession session);

    int auditNew(AuditVo vo,Integer accountSetsId);

    int cancelAuditNew(VoucherIdVo vo,Integer accountSetsId);

    void deleteVoucher(VoucherIdVo vo);

    AccountRoleVo selectAccountRole(Integer accountSetsId);
}
