package cn.gson.financial.kernel.model.vo;

import lombok.Data;

/**
 * @BelongsProject: financial
 * @BelongsPackage: cn.gson.financial.kernel.model.vo
 * @Author: 龙泽霖
 * @CreateTime: 2023-04-28  10:27
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class VoucherCreateVo {
    private Integer id;
    private String member;
}
