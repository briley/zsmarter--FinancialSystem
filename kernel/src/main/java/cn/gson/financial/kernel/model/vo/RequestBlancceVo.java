package cn.gson.financial.kernel.model.vo;

import lombok.Data;
import java.util.List;

@Data
public class RequestBlancceVo {

    private Integer subjectId;

    private String auxiliaryTitle;

    private List<CategoryVo> categoryVoList;
}