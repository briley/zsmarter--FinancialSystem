package cn.gson.financial.kernel.common;


public final class DoubleValueUtil {

    /**
     * 获取非空值
     *
     * @param val
     * @return
     */
    public static Double getNotNullVal(Double val) {
        return getNotNullVal(val, 0d);
    }

    /**
     * 获取非空值
     *
     * @param val
     * @param defaultVal 默认值
     * @return
     */
    public static Double getNotNullVal(Double val, Double defaultVal) {
        if (val == null) {
            return defaultVal == null ? 0 : defaultVal;
        }
        return val;
    }
}
