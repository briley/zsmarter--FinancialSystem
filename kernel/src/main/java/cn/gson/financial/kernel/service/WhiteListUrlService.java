package cn.gson.financial.kernel.service;


import cn.gson.financial.kernel.model.entity.WhiteListUrlTbBean;

import java.util.List;

public interface WhiteListUrlService {

     WhiteListUrlTbBean findByUrl(String url);

     List<String> findByType(String type);

}
