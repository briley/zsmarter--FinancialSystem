package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.FileBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author：ywh
 * @Date：2023/4/13 15:09
 * @Description：
 */
@Mapper
@Repository
public interface FileMapper   {

    int insert(FileBean fileBean);

    int delFileList(@Param("fileIdList") List<String> fileIdList);

    @Select("select file_name from fxy_finacnial_file where account_sets_id = #{accountSetsId} and voucher_id = #{voucherId}")
    List<String> findFileName(@Param("accountSetsId") int accountSetsId,@Param("voucherId") int voucherId);

    @Select("select * from fxy_finacnial_file where account_sets_id = #{accountSetsId} and voucher_id = #{voucherId}")
    List<FileBean> findFileBean(@Param("accountSetsId") int accountSetsId,@Param("voucherId") int voucherId);

    List<FileBean> findFileNameList(@Param("accountSetsId") int accountSetsId, @Param("voucherIdList") List<Integer> voucherIdList);

    int updateVoucherDocNum(@Param("accountSetsId") int accountSetsId,@Param("voucherId") int voucherId);
}
