package cn.gson.financial.kernel.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;


@Data
@TableName(value = "fxy_financial_voucher_details_auxiliary")
public class VoucherDetailsAuxiliary implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 凭证明细 Id
     */
    @TableField(value = "voucher_details_id")
    private Integer voucherDetailsId;

    /**
     * 辅助类型 id
     */
    @TableField(value = "accounting_category_id")
    private Integer accountingCategoryId;

    /**
     * 辅助项值 Id
     */
    @TableField(value = "accounting_category_details_id")
    private Integer accountingCategoryDetailsId;

    @TableField(exist = false)
    private AccountingCategoryDetails accountingCategoryDetails;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_VOUCHER_DETAILS_ID = "voucher_details_id";

    public static final String COL_ACCOUNTING_CATEGORY_ID = "accounting_category_id";

    public static final String COL_ACCOUNTING_CATEGORY_DETAILS_ID = "accounting_category_details_id";
}