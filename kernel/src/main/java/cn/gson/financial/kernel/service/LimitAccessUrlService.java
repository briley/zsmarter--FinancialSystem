package cn.gson.financial.kernel.service;


import cn.gson.financial.kernel.model.entity.LimitAccessUrltbBean;

public interface LimitAccessUrlService {

    LimitAccessUrltbBean findByIdUrl(String idUrl);

    void cleanLimitAccessUrlTable();

    void addLimitAccessUrlBean(LimitAccessUrltbBean bean);

    void updateAccessNum(String idUrl, String num);


}
