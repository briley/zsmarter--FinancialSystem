package cn.gson.financial.kernel.model.mapper;



import cn.gson.financial.kernel.model.entity.LimitAccessUrltbBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 限量接口
 */

@Mapper
@Repository
public interface LimitAccessUrlMapper {


    LimitAccessUrltbBean findByIdUrl(@Param("idUrl") String idUrl);

    void cleanLimitAccessUrlTable();

    void addLimitAccessUrlBean(@Param("bean") LimitAccessUrltbBean bean);

    void updateLimitAccessUrlNumByIdUrl(@Param("idUrl") String idUrl, @Param("num") String num);

}
