package cn.gson.financial.kernel.service;

import cn.gson.financial.kernel.model.entity.ReportBottomTableBean;
import cn.gson.financial.kernel.model.entity.Voucher;
import cn.gson.financial.kernel.model.vo.IndexTargetVo;
import cn.gson.financial.kernel.model.vo.VoucherCreateVo;
import cn.gson.financial.kernel.model.vo.VoucherSelectVo;

import java.util.Date;
import java.util.List;

public interface AllSelectService {
    List<Voucher> voucherSelect(VoucherSelectVo vo , Integer id);

    List<IndexTargetVo> IndexTarget(Integer accountSetsId,String timeStart,String timeEnd);

    IndexTargetVo IndexTargetYear(String date,Integer accountSetsId);


    List<VoucherCreateVo> selectCreateMember(List<VoucherCreateVo> vo);

    List<String> selectWord(Integer accountSetsId);
}
