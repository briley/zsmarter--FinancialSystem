package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.SubjectMateriel;
import cn.gson.financial.kernel.model.vo.SubjectMaterielVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface SubjectMaterielMapper extends BaseMapper<SubjectMateriel> {

    List<SubjectMaterielVo> selectSubjectMaterielVo(@Param("accountSetsId") Integer accountSetsId);
}
