package cn.gson.financial.kernel.model.entity;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/4/13 14:54
 * @Description：
 */
@Data
public class FileBean {
    private int accountSetsId;//账套id
    private int voucherId;//凭证id
    private List<MultipartFile> fileList ;
    private String fileId;//附件文件id
    private String fileName;//附件文件名字
    private Date createTime;//
}
