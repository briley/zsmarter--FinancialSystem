package cn.gson.financial.kernel.dao;

import cn.gson.financial.kernel.model.entity.User;
import cn.gson.financial.kernel.model.entity.UserAccountLimit;
import cn.gson.financial.kernel.model.mapper.UserAccountLimitMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class UserAccountLimitDao {

    @Resource
    private UserAccountLimitMapper mapper;

    /**
     * 添加
     * @param userAccountLimit
     */
    public void save(UserAccountLimit userAccountLimit){
        this.mapper.insert(userAccountLimit);
    }


    /**
     * 跟用户的 openId 查询 当前用户的 账套数量限制
     * @param userOpenId
     * @return
     */
    public int selectAccountLimitNum(String userOpenId){
        LambdaQueryWrapper<UserAccountLimit> qw = new LambdaQueryWrapper<>();
        qw.eq(UserAccountLimit::getUserOpenId,userOpenId);
        return this.mapper.selectOne(qw).getAccountLimitNum();
    }

}
