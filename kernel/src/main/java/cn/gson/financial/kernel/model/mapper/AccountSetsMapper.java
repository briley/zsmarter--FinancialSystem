package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.AccountSets;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface AccountSetsMapper extends BaseMapper<AccountSets> {
    int batchInsert(@Param("list") List<AccountSets> list);

    List<AccountSets> selectMyAccountSets(@Param("uid") Integer uid);

    String selectSubjectFrist(Integer accountSetsId);

    String selectSubjectEnd(Integer accountSetsId);

    Integer selectCheck(@Param("id") Integer accountSetsId,@Param("year") Integer year,@Param("month")Integer month);
}