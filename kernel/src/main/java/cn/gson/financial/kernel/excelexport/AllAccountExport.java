package cn.gson.financial.kernel.excelexport;
import lombok.Data;

/**
 * @Author：ywh
 * @Date：2023/4/14 17:19
 * @Description：
 */
@Data
public class AllAccountExport  {

    //总账导出
    private String code;//科目编码
    private String name;//科目名称
    private String voucherDate;//期间
    private String summary;//摘要
    private String debitAmount;//借方金额
    private String creditAmount;//贷方金额
    private String balanceDirection;//方向
    private String balance;//余额

}
