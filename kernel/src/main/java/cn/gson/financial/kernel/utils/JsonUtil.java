package cn.gson.financial.kernel.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author：ywh
 * @Date：2023/4/7 9:21
 * @Description：
 */
public class JsonUtil {
    public static void main(String[] args) {

        String param = "    private Date voucherDate;\n" +
                "    private Integer voucherId;\n" +
                "    private Integer detailsId;\n" +
                "    private String word;\n" +
                "    private Integer code;\n" +
                "    private String summary;\n" +
                "    private String subjectName;\n" +
                "    private String voucherWordCode;\n" +
                "    private Integer subjectId;\n" +
                "    private String subjectCode;\n" +
                "    private String detailSubjectCode;\n" +
                "    private Double debitAmount;\n" +
                "    private Double creditAmount;\n" +
                "    private String balanceDirection;\n" +
                "    private Double balance;\n" +
                "    private Double num;\n" +
                "    private Double numBalance;\n" +
                "    private Double price;\n" +
                "    private String unit;\n" +
                "    private String auxiliaryTitle;\n" +
                "    private Date minVoucherDate;\n" +
                "    private Double currencyDebit;\n" +
                "    private Double currencyCredit;\n" +
                "    private Double currencyBalance;\n" +
                "    private Double cumulativeCredit;\n" +
                "    private Double cumulativeDebit;";
//        System.out.println(beanToJson(param));
        List<String> list = beanToGetSet(param, "voucherDetailVo.get");
        list.forEach(System.out::println);





    }

    public static List<String> beanToGetSet(String param,String method){
        param = param .replace(" ","").replace("MultipartFile","")
                .replace("public","").replace("protected","")
                .replace("default","").replace("private","")
                .replace("static","").replace("final","")
                .replace("byte","").replace("short","").replace("int","").replace("long","")
                .replace("Byte","").replace("Short","").replace("Integer","").replace("Long","")
                .replace("Double","")
                .replace("String","").replace("boolean","").replace("Boolean","").replace("Date","");
        String[]  split = param.split("\n");

        for (String s : split) {
            System.out.println(s);
        }
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < split.length; i++) {
            int index = split[i].indexOf("//");
            if (index != -1){
                String substring = split[i].substring(0,index-1);

                char chars[] = substring.toCharArray();
                if (chars[0] >= 97) {
                    String result = method + (chars[0] + "").toUpperCase()+ substring.substring(1) +"();";
                    list.add(result);
                }

            }else {
                String substring = split[i].replace(";","");
                char chars[] = substring.toCharArray();
                System.out.println(chars.length);
                if (chars[0] >= 97) {
                    String result = method+ (chars[0] + "").toUpperCase()+ substring.substring(1) + "();";
                    list.add(result);
                }
            }
        }
        return list;
    }

    public static List<String> beanToJson(String param){
        param = param .replace(" ","").replace("MultipartFile","")
                .replace("public","").replace("protected","")
                .replace("default","").replace("private","")
                .replace("static","").replace("final","")
                .replace("byte","").replace("short","").replace("int","").replace("long","")
                .replace("Byte","").replace("Short","").replace("Integer","").replace("Long","")
                .replace("String","").replace("boolean","").replace("Boolean","");
        String[]  split = param.split("\n");

        for (int i = 0; i < split.length; i++) {
            int index = split[i].indexOf("//");
            if (index != -1){
                String substring = split[i].substring(0,index-1);
                split[i] = "\"" + substring + "\":\"\",";
            }else {
                split[i] = "\"" + split[i].replace(";","") + "\":\"\",";
            }
        }
        List<String> list = Arrays.asList(split);
        List<String> finList = new ArrayList<>();
        finList.addAll(list);
        finList.remove("\"\":\"\",");
        return finList;
    }
}