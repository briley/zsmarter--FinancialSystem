package cn.gson.financial.kernel.model.mapper;

import cn.gson.financial.kernel.model.entity.VoucherDetails;
import cn.gson.financial.kernel.model.vo.AccountBookNumVo;
import cn.gson.financial.kernel.model.vo.CategoryVo;
import cn.gson.financial.kernel.model.vo.ChronologicalAccountVo;
import cn.gson.financial.kernel.model.vo.VoucherDetailVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Mapper
public interface VoucherDetailsMapper extends BaseMapper<VoucherDetails> {
    int batchInsert(@Param("list") List<VoucherDetails> list);

    VoucherDetails selectCarryForwardMoney(@Param("accountSetsId") Integer accountSetsId, @Param("years") Integer years, @Param("month") Integer month, @Param("code") String code);

    VoucherDetails selectCarryForwardMoneyByFourParm(@Param("accountSetsId") Integer accountSetsId, @Param("years") Integer years, @Param("month") Integer month);


    VoucherDetails selectFinalCheckData(@Param("accountSetsId") Integer accountSetsId, @Param("year") Integer year, @Param("month") Integer month);

    /**
     * 报表检查 资产-负债=权益
     *
     * @param accountSetsId
     * @param voucherDate
     * @return
     */
    List<VoucherDetails> assetStatistics(@Param("accountSetsId") Integer accountSetsId, @Param("voucherDate") Date voucherDate);

    List<VoucherDetails> assetStatisticsByParm(@Param("accountSetsId") Integer accountSetsId, @Param("voucherDate") Date voucherDate);

    List<VoucherDetails> assetInitByParmInit(@Param("accountSetsId") Integer accountSetsId);

    /**
     * 最常用备注
     *
     * @param accountSetsId
     * @return
     */
    List<String> selectTopSummary(@Param("accountSetsId") Integer accountSetsId);

    /**
     * 科目余额计算汇总数据
     *
     * @param accountSetsId
     * @param subjectId
     * @param categoryId
     * @param categoryDetailsId
     * @return
     */
    List<VoucherDetailVo> selectBalanceData(@Param("accountSetsId") Integer accountSetsId, @Param("subjectId") Integer subjectId, @Param("categoryId") Integer categoryId, @Param("categoryDetailsId") Integer categoryDetailsId);

    List<VoucherDetailVo> selectBalanceData1(@Param("accountSetsId") Integer accountSetsId, @Param("subjectId") Integer subjectId,@Param("auxiliaryTitle") String auxiliaryTitle);

    /**
     * 期初检查
     *
     * @param accountSetsId
     * @return
     */
    Map<String, Double> selectListInitialCheckData(@Param("accountSetsId") Integer accountSetsId);

    List<VoucherDetails> selectBalanceList(@Param("accountSetsId") Integer accountSetsId, @Param("type") String type,@Param("currencyId") Integer currencyId);

    Double selectBassetsAndLiabilities(@Param("accountSetsId") Integer accountSetsId, @Param("type") String type);

    List<VoucherDetails> selectAuxiliaryList(@Param("accountSetsId") Integer accountSetsId, @Param("type") String type,@Param("currencyId") Integer currencyId);

    List<VoucherDetails> selectAggregateAmount(@Param("accountSetsId") Integer accountSetsId, @Param("codeList") Set<String> codeList, @Param("year") int year, @Param("month") int month);

    //序时账 动态查询

    /**
     *
     * @param accsetsId 账套
     * @param sTime  开始时间
     * @param eTime 结束时间
     * @param sWordCode 开始的凭证字号
     * @param eWordCode 结束的凭证字号
     * @param word  凭证字
     * @param summary 摘要
     * @param createMemberName 制单人
     * @param sSubjectCode 起始科目
     * @param eSubjectCode 结束科目
     * @param sMoney 起始金额
     * @param eMoney 结束金额
     * @param sort 排序 1-2
     * @param wordCodeList 凭证字号 集合
     * @param subCodeList  科目Code 集合
     * @return
     */
    List<ChronologicalAccountVo> selectChronologicalAccount(
          @Param("aId")  Integer accsetsId,
          @Param("sTime")  String sTime,
          @Param("eTime")  String eTime,
          @Param("sWordCode")  String sWordCode,
          @Param("eWordCode")  String eWordCode,
          @Param("word")  String word,
          @Param("summary")  String summary,
          @Param("createMemberName") String createMemberName,
          @Param("sSubjectCode")  String sSubjectCode,
          @Param("eSubjectCode")  String eSubjectCode,
          @Param("sMoney")String sMoney,
          @Param("eMoney")String eMoney,
          @Param("sort")  Integer sort,
          @Param("wordCodeList") List<String> wordCodeList,
          @Param("subCodeList") List<String> subCodeList
    );

    List<ChronologicalAccountVo> selectChronologicalAccountBaseCurrency(
            @Param("aId")  Integer accsetsId,
            @Param("sTime")  String sTime,
            @Param("eTime")  String eTime,
            @Param("sWordCode")  String sWordCode,
            @Param("eWordCode")  String eWordCode,
            @Param("word")  String word,
            @Param("summary")  String summary,
            @Param("createMemberName") String createMemberName,
            @Param("sSubjectCode")  String sSubjectCode,
            @Param("eSubjectCode")  String eSubjectCode,
            @Param("sMoney")String sMoney,
            @Param("eMoney")String eMoney,
            @Param("sort")  Integer sort,
            @Param("wordCodeList") List<String> wordCodeList,
            @Param("subCodeList") List<String> subCodeList

            );
    List<ChronologicalAccountVo> selectChronologicalAccountCurrency(
            @Param("aId")  Integer accsetsId,
            @Param("sTime")  String sTime,
            @Param("eTime")  String eTime,
            @Param("sWordCode")  String sWordCode,
            @Param("eWordCode")  String eWordCode,
            @Param("word")  String word,
            @Param("summary")  String summary,
            @Param("createMemberName") String createMemberName,
            @Param("sSubjectCode")  String sSubjectCode,
            @Param("eSubjectCode")  String eSubjectCode,
            @Param("sMoney")String sMoney,
            @Param("eMoney")String eMoney,
            @Param("sort")  Integer sort,
            @Param("wordCodeList") List<String> wordCodeList,
            @Param("subCodeList") List<String> subCodeList,
            @Param("currencyId")  Integer currencyId

    );

    /**
     * 根据账套ID获取 所有的科目期初数据
     */

    Map<String, Double> initialCheckData(@Param("accountSetsId") Integer accountSetsId);
    List<Map> findBassetsAndLiabilities(@Param("accountSetsId") Integer accountSetsId);

    int updateQiChu(@Param("voucherDetails") VoucherDetails voucherDetails);

    int countSubjectAuxi(@Param("accId")Integer accountSetsId,@Param("subjectId") Integer subjectId);

    int removeAuxiliary(@Param("subId") Integer subjectId,@Param("name")String name);

    List<VoucherDetails> findAuxiliary(@Param("voucherDetailVoList") List<VoucherDetailVo> voucherDetailVoList,@Param("auxiliaryTitle") String auxiliaryTitle);

    List<VoucherDetails> countAuxiliary(@Param("subId")Integer subId,@Param("name")String name);
    //修改期初数据
    int  updateVoucherDetails(@Param("bean")VoucherDetails voucherDetails);

    List<VoucherDetails> selectQichu(@Param("accountSetsId") Integer accountSetsId,@Param("subjectId") Integer subjectId, @Param("categoryVoList") List<CategoryVo> categoryVoList);

    List<Integer> checkIsInsertQiChu(@Param("accountSetsId") Integer accountSetsId,@Param("subjectId") Integer subjectId,@Param("categoryVoList") List<CategoryVo> categoryVoList);

    int delQIChu(@Param("accountSetsId") Integer accountSetsId,@Param("subjectId") Integer subjectId,@Param("subjectCode") String subjectCode);

    List<AccountBookNumVo> generalLedgerByParamNum(@Param("accountSetsId") Integer accountSetsId,
                                                   @Param("startTime") String startTime,@Param("endTime") String endTime,
                                                   @Param("startSubject") String startSubject,@Param("endSubject") String endSubject,
                                                   @Param("startLevel") Integer startLevel,@Param("endLevel") Integer endLevel,
                                                   @Param("currencyId") Integer currencyId);

    Double selectCarryForward(@Param("accountSetsId") Integer accountSetsId, @Param("years") String timeStart,@Param("month") String timeEnd);

    List<AccountBookNumVo> countBalance(@Param("accountSetsId") Integer accountSetsId,@Param("lastMonthEndTimeString")String lastMonthEndTimeString,
                                        @Param("yearMonth") String yearMonth,@Param("subjectSet") Set<Integer> subjectSet);

    List<AccountBookNumVo> generalLedgerByParamNum1(@Param("accountSetsId") Integer accountSetsId,
                                                   @Param("startTime") String startTime,@Param("endTime") String endTime,
                                                   @Param("startSubject") String startSubject,@Param("endSubject") String endSubject,
                                                   @Param("startLevel") Integer startLevel,@Param("endLevel") Integer endLevel,
                                                   @Param("currencyId") Integer currencyId,
                                                   @Param("showNumPrice") boolean showNumPrice);

    List<AccountBookNumVo> countBalance1(@Param("accountSetsId") Integer accountSetsId,@Param("lastMonthEndTimeString")String lastMonthEndTimeString,
                                        @Param("endTime")String endTime,@Param("yearMonth") String yearMonth,@Param("subjectSet") Set<Integer> subjectSet);

}