package cn.gson.financial.kernel.service.impl;

import cn.gson.financial.kernel.model.entity.VoucherTemplateDetails;
import cn.gson.financial.kernel.model.mapper.VoucherTemplateDetailsMapper;
import cn.gson.financial.kernel.service.VoucherTemplateDetailsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class VoucherTemplateDetailsServiceImpl extends ServiceImpl<VoucherTemplateDetailsMapper, VoucherTemplateDetails> implements VoucherTemplateDetailsService {

    @Override
    public int batchInsert(List<VoucherTemplateDetails> list) {
        return baseMapper.batchInsert(list);
    }
}



